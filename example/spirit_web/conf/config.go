package conf

import (
	"gitee.com/smallow/spirit/web"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

type SystemConfig struct {
	web.SpiritConfig   `json:"spirit_config" mapstructure:"spirit_config"`
	ReadTimeOut        int    `json:"read_timeout" mapstructure:"read_timeout"`
	WriteTimeOut       int    `json:"write_timeout" mapstructure:"write_timeout"`
	StaticRoot         string `json:"static_root" mapstructure:"static_root"`
	StaticPrefix       string `json:"static_prefix" mapstructure:"static_prefix"`
	TemplatePageEnable bool   `json:"template_page_enable" mapstructure:"template_page_enable"`
	TemplateDir        string `json:"template_dir" mapstructure:"template_dir"`
}

/**
* 示例自定义配置文件-数组类型配置
 */
type SystemService struct {
	Code string `json:"code" mapstructure:"code"`
	Name string `json:"name" mapstructure:"name"`
	Url  string `json:"url" mapstructure:"url"`
	Ip   string `json:"ip" mapstructure:"ip"`
}

type serviceConfiguration struct {
	Services []*SystemService `json:"system_services" mapstructure:"system_services"`
}

var (
	systemConfig = &SystemConfig{}
	db           *gorm.DB
	services     = &serviceConfiguration{}
)

func init() {
	_, err := web.Viper("./config.toml", systemConfig)
	if err != nil {
		log.Panic("【Spirit Web】init system config error:", err)
	}
	//TODO mysql init
	if systemConfig.Mysql.DataSourceName == "" {
		log.Panic("【Spirit Web】mysql init config error:%v", "datasource may be empty")
		//return
	}
	if db, err := gorm.Open(mysql.New(mysql.Config{DSN: systemConfig.Mysql.DataSourceName, DriverName: systemConfig.Mysql.DriverName}), getGormConfig()); err == nil {
		sqlDB, _ := db.DB()
		sqlDB.SetMaxIdleConns(systemConfig.Mysql.MaxIdleConn)
		sqlDB.SetMaxOpenConns(systemConfig.Mysql.MaxOpenConn)
	}

	//TODO init self configuration ex. service.toml   it also can be got from the etcd server or other way
	_, err = web.Viper("./service.toml", &services)
	if err != nil {
		log.Println("system_services 初始化 error:", err.Error())
		return
	} else {
		log.Println("服务调用配置初始化成功!")
	}
}

func GetSystemConfig() *SystemConfig {
	return systemConfig
}

func GetDB() *gorm.DB {
	return db
}

func getGormConfig() *gorm.Config {
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer（日志输出的目标，前缀和日志包含的内容——译者注）
		logger.Config{
			SlowThreshold: time.Second,  // 慢 SQL 阈值
			LogLevel:      logger.Error, // 日志级别
			Colorful:      false,        // 禁用彩色打印
		},
	)
	return &gorm.Config{
		Logger:                                   newLogger,
		DisableForeignKeyConstraintWhenMigrating: true,
	}
}
