package main

import (
	"gitee.com/smallow/spirit/example/spirit_web/conf"
	"gitee.com/smallow/spirit/example/spirit_web/router"
	"gitee.com/smallow/spirit/web"
)

func main() {
	r := router.InitRouter()
	web.StartServer(conf.GetSystemConfig().Web.ServerPort, conf.GetSystemConfig().ReadTimeOut, conf.GetSystemConfig().WriteTimeOut, r)
}
