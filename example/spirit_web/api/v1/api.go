package v1

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func RouterRegister(router *gin.Engine) {
	group := router.Group("/api/v1")
	group.Use()
	{
		group.GET("/get", get)
		group.POST("/post", post)
	}
}

func post(context *gin.Context) {
	context.JSON(http.StatusOK, "post method response")
}

func get(context *gin.Context) {
	context.JSON(http.StatusOK, "get method response")
}
