package router

import (
	"gitee.com/smallow/spirit/example/spirit_web/conf"
	"gitee.com/smallow/spirit/web"
	"github.com/gin-gonic/gin"
)

func InitRouter(middlewares ...gin.HandlerFunc) *gin.Engine {
	gin.SetMode(gin.DebugMode)
	router := gin.Default()
	router.Use(middlewares...)
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "spider ok",
		})
	})
	/*router.GET("/ws",websocket.Handler(func(connection *websocket.Connection) {

	}))*/
	apiRouterRegister(router)
	if conf.GetSystemConfig().TemplatePageEnable {
		pageRouterRegister(router)
		web.InitViews(router, conf.GetSystemConfig().StaticPrefix, conf.GetSystemConfig().StaticRoot, conf.GetSystemConfig().TemplateDir)
	}
	return router
}
