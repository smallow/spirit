package router

import (
	"gitee.com/smallow/spirit/example/spirit_web/api/v1"
	"github.com/gin-gonic/gin"
)

func apiRouterRegister(router *gin.Engine) {
	v1.RouterRegister(router)
}
