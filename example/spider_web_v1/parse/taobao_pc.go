package parse

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitee.com/smallow/spirit/example/spider_web_v1/client"
	"gitee.com/smallow/spirit/msgbus"
	"log"
	"net/http"
	"regexp"
	"strings"
)

var (
	reg         = regexp.MustCompile(`g_page_config\s*=\s*(\{.+\})`)
	reg2        = regexp.MustCompile(`jsonp\d+\(\n*(.+)\n*\)`)
	tbDetail    = "https://item.taobao.com/item.htm?id=%s"
	tmDetail    = "https://detail.tmall.com/item.htm?id=%s"
	nullCookies = make([]*http.Cookie, 0)
)

/**
商品搜索第一页返回结果业务解析
*/
func ForSearchPage(taskID string, downloadResp []byte) (bool, []map[string]interface{}) {
	matchedData := reg.FindSubmatch(downloadResp)
	if len(matchedData) == 2 {
		data := make(map[string]interface{}, 0)
		err := json.Unmarshal([]byte(matchedData[1]), &data)
		if err != nil {
			log.Printf("taskID:[%s]数据解析失败", taskID)
			return false, nil
		}
		return parseData(taskID, data)
	}
	return false, nil
}

/**
商品搜索分页返回结果业务解析
*/
func ForPaginationPage(taskID string, downloadResp []byte) (bool, []map[string]interface{}) {
	content := string(downloadResp)
	if strings.Contains(content, "jsonp") {
		content = strings.Replace(content, " ", "", -1)
		content = strings.Replace(content, "\n", "", -1)
		matchedData := reg2.FindSubmatch([]byte(content))
		if matchedData != nil && len(matchedData) == 2 {
			data := make(map[string]interface{}, 0)
			err := json.Unmarshal([]byte(matchedData[1]), &data)
			if err != nil {
				log.Printf("taskID:[%s]数据解析失败", taskID)
				return false, nil
			}
			return parseData(taskID, data)
		}

	}
	return false, nil
}

func parseData(taskID string, data map[string]interface{}) (bool, []map[string]interface{}) {
	saveDatas := make([]map[string]interface{}, 0)
	var (
		shopID       string
		goodsID      string
		shopName     string
		price        string
		sales        string
		itemLocation string //地域
		//title        string
		rawTitle  string
		detailUrl string
		//shopLink     string
		pType string //淘宝|天猫
	)
	mods := data["mods"].(map[string]interface{})
	itemslist := mods["itemlist"].(map[string]interface{})
	if itemslist["data"] != nil {
		_data := itemslist["data"].(map[string]interface{})
		auctions := _data["auctions"].([]interface{})
		if len(auctions) > 0 {
			log.Printf(">>>>>>>>>>>>>>>>>>>>>>>>> taskID:[%s]任务抓取数据解析成功", taskID)
			for _, val := range auctions {
				result := val.(map[string]interface{})
				//title = result["title"].(string)
				rawTitle = result["raw_title"].(string)
				price = result["view_price"].(string)
				sales = result["view_sales"].(string)
				shopID = result["user_id"].(string)
				shopName = result["nick"].(string)
				itemLocation = result["item_loc"].(string)
				goodsID = result["nid"].(string)
				shopcard := result["shopcard"].(map[string]interface{})
				isTmall := shopcard["isTmall"].(bool)
				//shopLink = result["shopLink"].(string)
				if isTmall {
					detailUrl = fmt.Sprintf(tmDetail, goodsID)
					pType = "tmall"
				} else {
					detailUrl = fmt.Sprintf(tbDetail, goodsID)
					pType = "taobao"
				}
				tmp := map[string]interface{}{
					"task_id":  taskID,
					"goods_id": goodsID,
					//"title":      title,
					"raw_title":  rawTitle,
					"price":      price,
					"sales":      sales,
					"shop_name":  shopName,
					"shop_id":    shopID,
					"detail_url": detailUrl,
					"item_loc":   itemLocation,
					"p_type":     pType,
				}
				saveDatas = append(saveDatas, tmp)
			}
			return true, saveDatas
		}
	}
	return false, saveDatas
}
func requestDetail(searchKey, cookies, url string) {
	log.Println("下载详情页:", url)
	msgid := msgbus.UUID(8)
	ret, err := client.GetTBClient().Call("", msgid, msgbus.SERVICE_DOWNLOAD, msgbus.SENDTO_TYPE_RAND_RECIVER, map[string]interface{}{
		"url":    url,
		"method": "get",
		"param":  map[string]interface{}{},
		"head": map[string]interface{}{
			"accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
			"accept-language":           "zh-CN,zh;q=0.9,en;q=0.8",
			"cache-control":             " no-cache",
			"pragma":                    " no-cache",
			"refer":                     "https://s.taobao.com",
			"user-agent":                " Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.62 Safari/537.36",
			"cookie":                    cookies,
			"sec-ch-ua":                 "",
			"sec-ch-ua-platform":        "document",
			"sec-fetch-mode":            "navigate",
			"sec-fetch-site":            "same-site",
			"upgrade-insecure-requests": "1",
			"accept-encoding":           "",
		},
		"cookies": nullCookies,
	}, 20)

	if err != nil {
		log.Println("下载出错:", err)
		return
	}
	respData := map[string]interface{}{}
	err = json.Unmarshal(ret, &respData)
	if err != nil {
		log.Println("解析下载数据出错:", err)
		return
	}
	content := respData["content"].(string)
	bs, err := base64.StdEncoding.DecodeString(content)
	log.Println("详情页数据:", string(bs))
}
