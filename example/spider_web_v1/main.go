package main

import (
	"gitee.com/smallow/spirit/example/spider_web_v1/router"
	"gitee.com/smallow/spirit/web"
	"gitee.com/smallow/spirit/websocket"
	"github.com/gin-gonic/gin"
	"log"
)

func main() {
	//client.TBClientInit()
	gin.SetMode(gin.DebugMode)
	r := router.InitRouter()
	r.GET("/ws", func(context *gin.Context) {
		h := websocket.Handler(func(connection *websocket.Connection) {
			for {
				if bs, err := connection.ReadMessage(); err != nil {
					//log.Println("读数据出错:", err.Error())
					goto ERR
				} else {
					log.Printf("收到ClientID:%s 的message:%s", connection.Id(), string(bs))
				}
			}
		ERR:
			//TODO:关闭连接
			connection.Close()
		})
		h.ServeHTTP(context.Writer, context.Request)
	})
	//go job.StartTaoBaoPriceTopNJob()
	web.StartServer(":8080", 300, 200, r)
}
