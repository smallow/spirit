package job

import (
	"gitee.com/smallow/spirit/example/spider_web_v1/task"
	"time"
)



var (
	tbTopNPriceJobPool chan *task.TaoBaoPriceTopNJob = make(chan *task.TaoBaoPriceTopNJob, 50000)
	interval           *time.Ticker             = time.NewTicker(10 * time.Second)
)

func StartTaoBaoPriceTopNJob() {
	for {
		<-interval.C
		job := <-tbTopNPriceJobPool
		task.StartDownloadDetail(job)
	}
}
