package conf

import (
	sgrom "gitee.com/smallow/spirit/lib/gorm"
	"gitee.com/smallow/spirit/web"
	"gorm.io/gorm"
	"log"
)

var (
	SysConfig = &Config{}
	MysqlDB        *gorm.DB
)

type Config struct {
	Base struct {
		MsgBusServer string `json:"msg_bus_server" mapstructure:"msg_bus_server"`
	} `mapstructure:"base"`

	Mysql struct {
		DriverName     string `json:"driver_name" mapstructure:"driver_name"`
		DataSourceName string `json:"data_source_name" mapstructure:"data_source_name"`
		MaxOpenConn    int    `json:"max_open_conn" mapstructure:"max_open_conn"`
		MaxIdleConn    int    `json:"max_idle_conn" mapstructure:"max_idle_conn"`
	} `mapstructure:"mysql"`
}

func init() {
	_, err := web.Viper("./config.toml", SysConfig)
	if err != nil {
		log.Panic("init base config error:", err)
	}
	if SysConfig.Mysql.DataSourceName == "" {
		log.Printf("mysql DB init config error:%v", "datasource may be empty")
		return
	}
	MysqlDB = sgrom.NewGorm(SysConfig.Mysql.DataSourceName, SysConfig.Mysql.DriverName, SysConfig.Mysql.MaxOpenConn, SysConfig.Mysql.MaxIdleConn, nil)
	if MysqlDB == nil {
		log.Println("mysql DB pool created error")
		return
	}
	log.Printf(" DB init successful [%s]", SysConfig.Mysql.DataSourceName)
}
