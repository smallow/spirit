package api

import (
	"encoding/json"
	"fmt"
	"gitee.com/smallow/spirit/example/spider_web_v1/conf"
	"gitee.com/smallow/spirit/example/spider_web_v1/model"
	"gitee.com/smallow/spirit/example/spider_web_v1/task"
	"gitee.com/smallow/spirit/utils"
	"gitee.com/smallow/spirit/websocket"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

func RouterRegister(router *gin.Engine) {
	group := router.Group("/api/spider")
	group.Use()
	{
		group.POST("/first", first)
		group.POST("/paginationSearch", paginationSearch)
		group.POST("/saveData", saveData)
		group.GET("/taskOverviews", taskOverviews)
		group.GET("/queryByTaskID", queryByTaskID)
	}
}

func queryByTaskID(context *gin.Context) {
	data := make([]*model.TaoBaoIndexDownloadData, 0)
	taskID := context.Request.FormValue("taskID")
	_type := context.Request.FormValue("type") // 数据类型 1-下载数据  2-入库数据
	tableName := "taobao_pc_index_data"
	if _type == "1" {
		tableName = "taobao_pc_index_download_data"
	}
	conf.MysqlDB.Table(tableName).Where("task_id=?", taskID).Find(&data)
	result := map[string]interface{}{}
	result["code"] = 0
	result["data"] = data
	result["msg"] = "success"
	context.JSON(200, result)
}

/**
查看任务执行概览情况列表
*/
func taskOverviews(context *gin.Context) {
	result := map[string]interface{}{}
	record := make([]map[string]interface{}, 0)
	data := make([]map[string]interface{}, 0)
	page := context.Request.FormValue("page")
	pageSize := context.Request.FormValue("pageSize")
	var _page, _pageSize int
	var total, totalPage int64
	if page != "" {
		conf.MysqlDB.Table("taobao_pc_task").Count(&total)
		if total > 0 {
			_page, _ = strconv.Atoi(page)
			_pageSize, _ = strconv.Atoi(pageSize)
			totalPage = (total + int64(_pageSize) - 1) / int64(_pageSize)
			start := (_page - 1) * _pageSize
			conf.MysqlDB.Table("taobao_pc_task").Select("id", "task_id", "search_key", "success", "total", "save_num", "date", "create_time", "platform", "is_save").Limit(_pageSize).Offset(start).Order("id desc").Find(&data)
			for _, val := range data {
				m := map[string]interface{}{
					"search_key": val["search_key"].(string),
					"task_id":    val["task_id"].(string),
					"date":       val["date"].(string),
					"total":      val["total"].(int64),
					"platform":   val["platform"].(string),
				}
				if val["save_num"] != nil {
					m["save_num"] = val["save_num"].(int64)
				} else {
					m["save_num"] = 0
				}

				if val["create_time"] != nil {
					t := val["create_time"].(time.Time)
					m["create_time"] = utils.FormatDate(&t, utils.Date_Full_Layout)
				}
				if val["success"] != nil {
					a1 := val["success"].(int64)
					switch a1 {
					case 0:
						m["success"] = "成功"
					case 1:
						m["success"] = "失败"
					}
				}
				record = append(record, m)
			}
			result["records"] = record
		}
		result["total"] = total
		result["totalPage"] = totalPage
		result["page"] = _page
	}

	context.JSON(http.StatusOK, result)
}

/**
淘宝PC搜索 第一页数据
*/
func first(context *gin.Context) {
	result := make(map[string]interface{}, 0)
	searchKey := context.Request.FormValue("searchKey")
	cookies := context.Request.FormValue("cookies")
	data, _, err := task.StartDownloadTbPCJob(fmt.Sprintf(task.WrapRequestUrl(0), url.QueryEscape(searchKey)), searchKey, cookies, true)
	if err != nil {
		result["code"] = 201
		result["msg"] = err.Error()
	} else {
		result["code"] = 0
		result["data"] = data
		//result["cacheID"] = taskID
		result["msg"] = "success"
	}
	context.JSON(200, result)
}

/**
淘宝搜索 翻页数据
page:页码
*/
func paginationSearch(context *gin.Context) {
	result := make(map[string]interface{}, 0)
	page := context.PostForm("page")
	var (
		_page int
		err   error
	)
	if page != "" {
		_page, err = strconv.Atoi(page)
	}
	if err != nil {
		result["code"] = 201
		result["msg"] = err.Error()
		context.JSON(200, result)
		return
	}
	searchKey := context.Request.FormValue("searchKey")
	cookies := context.Request.FormValue("cookies")
	data, _, err := task.StartDownloadTbPCJob(fmt.Sprintf(task.WrapRequestUrl(_page), url.QueryEscape(searchKey)), searchKey, cookies, true)
	if err != nil {
		result["code"] = 201
		result["msg"] = err.Error()
	} else {
		result["code"] = 0
		result["data"] = data
		//result["cacheID"] = taskID
		result["msg"] = "success"
	}
	context.JSON(200, result)
}

/**
选择、筛选数据保存入查询库
*/
func saveData(context *gin.Context) {
	result := make(map[string]interface{}, 0)
	data := make([]*model.TaoBaoIndexDownloadData, 0)
	if err := context.ShouldBind(&data); err != nil {
		context.JSON(200, "参数异常")
		return
	}
	var taskID string
	if len(data) > 0 {
		taskID = data[0].TaskID
	}
	err := conf.MysqlDB.Transaction(func(tx *gorm.DB) error {
		err := tx.Table("taobao_pc_index_data").Create(data).Error
		if err != nil {
			tx.Rollback()
			return err
		}
		err = tx.Table("taobao_pc_task").Where("task_id=?", taskID).Updates(map[string]interface{}{
			"save_num": len(data)}).Error
		if err != nil {
			tx.Rollback()
			return err
		}
		return nil
	})
	if err != nil {
		log.Println("保存数据出错:", err)
		result["code"] = 500
		result["msg"] = err.Error()
	} else {
		result["code"] = 0
		result["msg"] = "success"
		sp := websocket.OnlineConnection()
		sp.Lock.Lock()
		defer sp.Lock.Unlock()
		for _, client := range sp.Map {
			conn := client.(*websocket.Connection)
			data := map[string]interface{}{"search_key": "", "task_id": "", "success": 0}
			message := websocket.Message{Act: websocket.ActMessage, Data: data}
			bs, _ := json.Marshal(message)
			conn.WriteMessage(bs)
		}
	}

	context.JSON(200, result)
}
