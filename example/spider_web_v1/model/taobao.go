package model

type SpiderTask struct {
	ID        int64  `json:"id" form:"id"`
	SearchKey string `json:"search_key" form:"search_key"`
	TaskID    string `json:"task_id" form:"task_id"`
	Cookies   string `json:"cookies" form:"cookies"`
	Success   bool   `json:"success" from:"success"`
	//TODO List
}

type TaoBaoIndexDownloadData struct {
	ID        int64  `json:"id" gorm:"" form:"id" gorm:"primaryKey,autoIncrement"`
	GoodsID   string `json:"goods_id" form:"goods_id"`
	TaskID    string `json:"task_id" form:"task_id"`
	Title     string `json:"title" form:"title"`
	RawTitle  string `json:"raw_title" form:"raw_title"`
	Price     string `json:"price" form:"price"`
	Sales     string `json:"sales" form:"sales"`
	ShopName  string `json:"shop_name" form:"shop_name"`
	ShopID    string `json:"shop_id" form:"shop_id"`
	PicUrl    string `json:"pic_url" form:""`              //缩略图url地址
	DetailUrl string `json:"detail_url" form:"detail_url"` //商品详情页地址
	ItemLoc   string `json:"item_loc" form:"item_loc"`     //地域
	ShopLink  string `json:"shop_link" form:"shop_link"`   //店铺地址
	PType     string `json:"p_type" form:"p_type"`         //tmal-天猫,taobao-淘宝 店铺类型
}

func (td *TaoBaoIndexDownloadData) TableName() string {
	return "taobao_pc_index_data"
}
