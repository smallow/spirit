package router

import "github.com/gin-gonic/gin"

func pageRouterRegister(router *gin.Engine) {
	router.GET("/", index)
}

func index(context *gin.Context) {
	context.HTML(200, "index.html", nil)
}
