package router

import (
	"gitee.com/smallow/spirit/example/spider_web_v1/api"
	"github.com/gin-gonic/gin"
)

func apiRouterRegister(router *gin.Engine) {
	api.RouterRegister(router)
}
