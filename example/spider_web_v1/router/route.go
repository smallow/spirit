package router

import (
	"gitee.com/smallow/spirit/web"
	"github.com/gin-gonic/gin"
	//"gitee.com/smallow/spirit/websocket"
)

func InitRouter(middlewares ...gin.HandlerFunc) *gin.Engine {
	router := gin.Default()
	router.Use(middlewares...)
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "spider ok",
		})
	})
	/*router.GET("/ws",websocket.Handler(func(connection *websocket.Connection) {

	}))*/
	apiRouterRegister(router)
	pageRouterRegister(router)
	web.InitViews(router, "/res", "./static/res", "./static/templates")
	return router
}
