function ajaxLoading(msg) {
    $("<div class=\"datagrid-mask\"></div>").css({
        display: "block",
        width: "100%",
        height: $(window).height() + 1000
    }).appendTo("body");
    $(".datagrid-mask").attr("z-index", 99999);
    $("<div class=\"datagrid-mask-msg\" style='font-size: 12px;'></div>").html(msg).appendTo("body").css({
        display: "block",
        left: ($(document.body).outerWidth(true) - 190) / 2,
        top: ($(window).height() - 45) / 2
    });
}

function ajaxLoadEnd() {
    $(".datagrid-mask").remove();
    $(".datagrid-mask-msg").remove();
}


function startLoading() {
    $("#spider_mask").show();
    $("#loading").html("");
    return setInterval(function () {
        let point = $("<span>.</span>")
        $("#loading").append(point)
    }, 1000)  //每隔一秒钟打印出111
}

function myAjax($, url, method, param, success, fail) {
    console.log("请求:" + url);
    $.ajax({
        url: url,
        //contentType: "application/json;charset=UTF-8",//请求类型
        dataType: "json",//服务器返回的数据类型
        headers: {
            "token": getLocalToken("token"),
        },
        data: param,
        type: method,
        success: function (resp, status, xhr) {
            console.log("请求:" + url + " 返回值code:" + resp.code);
            layer.closeAll('loading');
            if (resp.code === 0) {
                let newToken = xhr.getResponseHeader("new-token");
                if (newToken != null && newToken !== "") {
                    console.log("token更新:", newToken);
                    localStorage.setItem("token", newToken);
                }
                success(resp);
            } else if (resp.code === 1001) {
                console.log("token超时,重新登录");
                exit();
                //refreshToken(getLocalToken("token"), url, param, success)
            } else if (resp.code === 1002) {
                exit();
            } else if (resp.code === 1003) {
                layer.msg("无请求权限", {icon: 5, anim: 6});
            }
        },
        error: function (e) {
            layer.closeAll('loading');
            console.log("url:[" + url + "] 请求出错:" + e.status + "----[" + e.responseText + "]");
        }
    });
}

function refreshToken(oldToken, refreshToken, originUrl, originParam, originRequestCallback) {

}


function getActionFromUrl(url) {
    return urlKeyVal(url, "action");
}

function urlKeyVal(url, key) {
    let reg = new RegExp("(^|\\?|&)" + key + "=([^&]*)(\\s|&|$)", "i");
    if (reg.test(url)) return unescape(RegExp.$2.replace(/\+/g, " "));
    return "";
}

function exit() {
    removeLocalToken();
    window.location.href = "/"
}


function getLocalToken(key) {
    let token = localStorage.getItem(key);
    if (token != null && token != "")
        return token;
    return "";
}

function removeLocalToken() {
    localStorage.clear();
}

function insTbUpdatedSuccess(resp, back) {
    if (resp.code === 0) {
        layer.msg("操作成功", {icon: 1, time: 1000}, function () {
            if (back) {
                back();
            }
        });
    } else {
        layer.msg(resp.msg, {icon: 2});
    }
}

