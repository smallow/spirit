package task

import (
	"log"
)

type TaoBaoPriceTopNJob struct {
	Area      string `json:"area"`       //区域
	GoodsID   string `json:"goods_id"`   //商品id
	GoodsName string `json:"goods_name"` //商品名称
	ShopID    string `json:"shop_id"`    //店铺id
	ShopName  string `json:"shop_name"`  //店铺名称
	DetailUrl string `json:"detail_url"` //商品详情页面url
	Cookies   string `json:"cookies"`
}

var (
	tbDetailHeader = map[string]interface{}{
		"accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"accept-language":           "zh-CN,zh;q=0.9,en;q=0.8",
		"cache-control":             " no-cache",
		"pragma":                    " no-cache",
		"refer":                     "https://s.taobao.com",
		"user-agent":                " Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.62 Safari/537.36",
		"sec-ch-ua":                 "",
		"sec-ch-ua-platform":        "document",
		"sec-fetch-mode":            "navigate",
		"sec-fetch-site":            "same-site",
		"upgrade-insecure-requests": "1",
		"accept-encoding":           "",
	}
)

func StartDownloadDetail(j *TaoBaoPriceTopNJob) {
	log.Printf("开始下载店铺[%s] 商品id:[%s] ", j.ShopName, j.GoodsID)
	tbDetailHeader["cookie"] = j.Cookies
	bs, err, taskID := download(j.DetailUrl, j.Cookies, tbDetailHeader)
	if err != nil {
		//go SaveOverviewInfo(searchKey, cookies, taskID, "taobao_pc", 1, 0)
		log.Printf("详情页下载任务:[%s],shopname:[%s],goodid:[%s]失败:[%v]", taskID, j.ShopName, j.GoodsID, err)
		return
	}
	log.Println("详情页数据:", string(bs))
}
