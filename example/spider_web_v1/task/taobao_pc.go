package task

import (
	"encoding/json"
	"fmt"
	"gitee.com/smallow/spirit/example/spider_web_v1/conf"
	"gitee.com/smallow/spirit/example/spider_web_v1/parse"
	"gitee.com/smallow/spirit/websocket"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"log"
	"sync"
	"time"
)

var (
	//主页关键字搜索url 默认排序(综合)
	//mainSearchUrl         = "https://s.taobao.com/search?q=%s&suggest=0_2&commend=all&ssid=s5-e&search_type=item&sourceId=tb.index&spm=a21bo.jianhua.201856-taobao-item.2&ie=utf8&initiative_id=tbindexz_20170306&_input_charset=utf-8&wq=&suggest_query=&source=suggest"
	//sortSearchUrlForPrice    = "https://s.taobao.com/search?data-key=sort&data-value=price-asc&ajax=true&_ksTS=1655110839616_724&callback=jsonp725&q=%s&commend=all&ssid=s5-e&search_type=item&sourceId=tb.index&spm=a21bo.jianhua.201856-taobao-item.2&ie=utf8&initiative_id=tbindexz_20170306"
	sortSearchUrlForSaleDesc = "https://s.taobao.com/search?data-key=sort&data-value=sale-desc&ajax=true&_ksTS=1655086620535_724&callback=jsonp725&q=%s&commend=all&ssid=s5-e&search_type=item&sourceId=tb.index&spm=a21bo.jianhua.201856-taobao-item.2&ie=utf8&initiative_id=tbindexz_20170306"
	CacheMap                 = sync.Map{}
	//第二、三、四、五页url (商品搜索)
	paginationUrls = []string{
		"https://s.taobao.com/search?data-key=s&data-value=44&ajax=true&_ksTS=1655110898388_891&callback=jsonp892&q=%s&commend=all&ssid=s5-e&search_type=item&sourceId=tb.index&spm=a21bo.jianhua.201856-taobao-item.2&ie=utf8&initiative_id=tbindexz_20170306&sort=sale-desc&bcoffset=0&p4ppushleft=%2C44",
		"https://s.taobao.com/search?data-key=s&data-value=88&ajax=true&_ksTS=1655110941951_1112&callback=jsonp1113&q=%s&commend=all&ssid=s5-e&search_type=item&sourceId=tb.index&spm=a21bo.jianhua.201856-taobao-item.2&ie=utf8&initiative_id=tbindexz_20170306&sort=sale-desc&bcoffset=0&p4ppushleft=%2C44&s=44",
		"https://s.taobao.com/search?data-key=s&data-value=132&ajax=true&_ksTS=1655111070774_1340&callback=jsonp1341&q=%s&commend=all&ssid=s5-e&search_type=item&sourceId=tb.index&spm=a21bo.jianhua.201856-taobao-item.2&ie=utf8&initiative_id=tbindexz_20170306&sort=sale-desc&bcoffset=0&p4ppushleft=%2C44&s=88",
		"https://s.taobao.com/search?data-key=s&data-value=176&ajax=true&_ksTS=1655111700714_1607&callback=jsonp1608&q=%s&commend=all&ssid=s5-e&search_type=item&sourceId=tb.index&spm=a21bo.jianhua.201856-taobao-item.2&ie=utf8&initiative_id=tbindexz_20170306&sort=sale-desc&bcoffset=0&p4ppushleft=%2C44&s=132",
	}

	tbPCMainSearchHeader = map[string]interface{}{
		"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
		"cache-control":   " no-cache",
		"pragma":          " no-cache",
		"user-agent":      " Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
		"referer":         "https://s.taobao.com/search?ie=utf8&initiative_id=staobaoz_20220609&stats_click=search_radio_all%3A1&js=1&imgfile=&q=%E5%A5%B3%E8%A3%85&suggest=0_1&_input_charset=utf-8&wq=nvz&suggest_query=nvz&source=suggest&bcoffset=1&ntoffset=1&p4ppushleft=2%2C48&s=44",
	}
)

func StartDownloadTbPCJob(requestUrl, searchKey, cookies string, pagination bool) ([]map[string]interface{}, string, error) {
	//log.Println("requestUrl:", requestUrl)
	tbPCMainSearchHeader["cookie"] = cookies
	bs, err, taskID := download(requestUrl, cookies, tbPCMainSearchHeader)
	if err != nil {
		go SaveOverviewInfo(searchKey, cookies, taskID, "taobao_pc", 1, 0)
		return nil, taskID, err
	}
	var (
		b    bool
		data []map[string]interface{}
	)
	if !pagination {
		b, data = parse.ForSearchPage(taskID, bs)
	} else {
		b, data = parse.ForPaginationPage(taskID, bs)
	}

	if b {
		/*CacheMap.Store(taskID, map[string]interface{}{
			"searchKey": searchKey,
			"cookies":   cookies,
			"data":      data,
		})*/
		go SaveDownloadData(taskID, data)                                          //保存下载数据
		go SaveOverviewInfo(searchKey, cookies, taskID, "taobao_pc", 0, len(data)) //任务执行成功
		return data, taskID, nil
	} else {
		go SaveOverviewInfo(searchKey, cookies, taskID, "taobao_pc", 1, 0) //任务执行失败
	}
	return nil, taskID, errors.New("抓取数据匹配失败")
}

/**
保存下载数据到冷库(表)
*/
func SaveDownloadData(taskID string, data []map[string]interface{}) {
	err := conf.MysqlDB.Transaction(func(tx *gorm.DB) error {
		err := tx.Table("taobao_pc_index_download_data").Create(data).Error
		if err != nil {
			tx.Rollback()
			return err
		}
		return nil
	})
	if err != nil {
		log.Printf("taskID:[%s] SaveDownloadData err:[%v]", taskID, err)
	}
}

func SaveOverviewInfo(searchKey, cookies, taskID, platform string, success, total int) {
	now := time.Now()
	conf.MysqlDB.Table("taobao_pc_task").Create(map[string]interface{}{
		"search_key": searchKey,
		"cookies":    cookies,
		"task_id":    taskID,
		"success":    success,
		"total":      total,
		"date":       fmt.Sprintf("%d-%02d-%02d", now.Year(), now.Month(), now.Day()),
		"platform":   platform,
	})

	//成功向前台发送刷新socket信息
	sp := websocket.OnlineConnection()
	sp.Lock.Lock()
	defer sp.Lock.Unlock()
	for _, client := range sp.Map {
		conn := client.(*websocket.Connection)
		data := map[string]interface{}{"search_key": searchKey, "task_id": taskID, "success": success}
		message := websocket.Message{Act: websocket.ActMessage, Data: data}
		bs, _ := json.Marshal(message)
		conn.WriteMessage(bs)
	}

}

func WrapRequestUrl(page int) string {
	if page == 0 {
		return sortSearchUrlForSaleDesc
	}
	if page > 1 {
		return paginationUrls[page-2]
	}
	return ""
}
