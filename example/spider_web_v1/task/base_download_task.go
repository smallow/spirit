package task

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/smallow/spirit/example/spider_web_v1/client"
	"gitee.com/smallow/spirit/msgbus"
	"log"
)

func download(url, cookies string, header map[string]interface{}) ([]byte, error, string) {
	msgid := msgbus.UUID(8)
	log.Printf(">>>>>>>>>>>>>>>>>>>>>>>>> taskID:[%s] 开始抓取数据", msgid)
	ret, err := client.GetTBClient().Call("", msgid, msgbus.SERVICE_DOWNLOAD, msgbus.SENDTO_TYPE_RAND_RECIVER, map[string]interface{}{
		"url":    url,
		"method": "get",
		"param":  map[string]interface{}{},
		"head": header,
		"cookies": cookies,
	}, 60)
	if err != nil {
		log.Printf("taskID:[%s]抓取出错:[%v]", msgid, err)
		return nil, errors.New(fmt.Sprintf("taskID:[%s]抓取出错:[%v]", msgid, err)), msgid
	}
	respData := map[string]interface{}{}
	err = json.Unmarshal(ret, &respData)
	if err != nil {
		log.Printf("taskID:[%s]解析下载数据出错:[%v]", msgid, err)
		return nil, errors.New(fmt.Sprintf("taskID:[%s]解析下载数据出错:[%v]", msgid, err)), msgid
	}
	content := respData["content"].(string)
	bs, err := base64.StdEncoding.DecodeString(content)
	return bs, err, msgid
}
