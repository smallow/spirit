package client

import (
	"gitee.com/smallow/spirit/example/spider_web_v1/conf"
	"gitee.com/smallow/spirit/msgbus"
	"log"
)

var (
	client *msgbus.Client
)

func TBClientInit() {
	client1, err := msgbus.StartClient(nil, conf.SysConfig.Base.MsgBusServer, "数据采集平台淘宝网查询客户端", []int{msgbus.SERVICE_SPIDER_TAOBAO})
	if err != nil {
		log.Println("淘宝网客户端连接消息调度总线服务失败>>>>>>>>>>>>>>>>>>>>>>>")
		return
	}
	client = client1
	log.Println("淘宝网客户端消息调度总线服务连接成功>>>>>>>>>>>>>>>>>>>>>>>")
}

func GetTBClient() *msgbus.Client {
	return client
}
