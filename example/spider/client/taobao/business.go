package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitee.com/smallow/spirit/msgbus"
	"gorm.io/gorm"
	"log"
	"regexp"
	"time"
)

var (
	reg      = regexp.MustCompile(`g_page_config\s*=\s*(\{.+\})`)
	tbDetail = "https://item.taobao.com/item.htm?id=%s"
	tmDetail = "https://detail.tmall.com/item.htm?id=%s"
)

/**
商品搜索第一页返回结果业务解析
*/
func parseForSearchPage(downloadResp string, taskID string) bool {
	saveDatas := make([]map[string]interface{}, 0)
	matchedData := reg.FindStringSubmatch(downloadResp)
	var (
		shopID       string
		goodsID      string
		shopName     string
		price        string
		sales        string
		itemLocation string //地域
		title        string
		rawTitle     string
		detailUrl    string
		//shopLink     string
	)
	if len(matchedData) == 2 {
		data := make(map[string]interface{}, 0)
		err := json.Unmarshal([]byte(matchedData[1]), &data)
		if err != nil {
			log.Println("数据解析失败")
			return false
		}
		mods := data["mods"].(map[string]interface{})
		itemslist := mods["itemlist"].(map[string]interface{})
		_data := itemslist["data"].(map[string]interface{})
		auctions := _data["auctions"].([]interface{})
		if len(auctions) > 0 {
			log.Println("*****************列表数据获取成功*****************")
			for _, val := range auctions {
				result := val.(map[string]interface{})
				title = result["title"].(string)
				rawTitle = result["raw_title"].(string)
				price = result["view_price"].(string)
				sales = result["view_sales"].(string)
				shopID = result["user_id"].(string)
				shopName = result["nick"].(string)
				itemLocation = result["item_loc"].(string)
				goodsID = result["nid"].(string)
				shopcard := result["shopcard"].(map[string]interface{})
				isTmall := shopcard["isTmall"].(bool)
				//shopLink = result["shopLink"].(string)
				if isTmall {
					detailUrl = fmt.Sprintf(tmDetail, goodsID)
				} else {
					detailUrl = fmt.Sprintf(tbDetail, goodsID)
				}
				//fmt.Printf("序号[%d] 标题:[%s],价格:[%s],销量:[%s],店铺:[%s],店铺id:[%s] ,商品id:[%s],地域:[%s],rawTitle:[%s],detailUrl:[%s] \n", index+1, title, price, sales, shopName, shopID, goodsID, itemLocation, rawTitle, detailUrl)
				tmp := map[string]interface{}{
					"task_id":    taskID,
					"goods_id":   goodsID,
					"title":      title,
					"raw_title":  rawTitle,
					"price":      price,
					"sales":      sales,
					"shop_name":  shopName,
					"shop_id":    shopID,
					"detail_url": detailUrl,
					"item_loc":   itemLocation,
				}
				saveDatas = append(saveDatas, tmp)
				if needDetailRequest {
					requestDetail(detailUrl)
				}
			}
			if err := saveData(taskID, saveDatas); err == nil {
				return true
			}
		}
	} else {
		now := time.Now()
		db.Table("index_search_overview").Create(map[string]interface{}{
			"task_id":    taskID,
			"search_key": searchKey,
			"cookies":    cookieStr,
			"success":    1,
			"total":      0,
			"date":       fmt.Sprintf("%d-%02d-%02d", now.Year(), now.Month(), now.Day()),
			"platform":   "taobao",
		})
		log.Printf("[%s] 任务执行下载匹配失败",taskID)
	}
	return false
}

func saveData(taskID string, data []map[string]interface{}) error {
	err := db.Transaction(func(tx *gorm.DB) error {
		err := tx.Table("index_search_data").Create(data).Error
		if err != nil {
			tx.Rollback()
			return err
		}
		now := time.Now()
		err = tx.Table("index_search_overview").Create(map[string]interface{}{
			"task_id":    taskID,
			"search_key": searchKey,
			"cookies":    cookieStr,
			"success":    0,
			"total":      len(data),
			"date":       fmt.Sprintf("%d-%02d-%02d", now.Year(), now.Month(), now.Day()),
			"platform":   "taobao",
		}).Error
		if err != nil {
			tx.Rollback()
			return err
		}
		return nil
	})
	if err == nil {
		log.Printf("taskID:[%s]保存数据[%d]条成功", taskID, len(data))
	} else {
		log.Printf("taskID:[%s]保存数据失败:[%v]", taskID, data)
	}

	return err
}

func requestDetail(url string) {
	log.Println("下载详情页:", url)
	msgid := msgbus.UUID(8)
	ret, err := client.Call("", msgid, msgbus.SERVICE_DOWNLOAD, msgbus.SENDTO_TYPE_RAND_RECIVER, map[string]interface{}{
		"url":    url,
		"method": "get",
		"param":  map[string]interface{}{},
		"head": map[string]interface{}{
			"accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
			"accept-language":           "zh-CN,zh;q=0.9,en;q=0.8",
			"cache-control":             " no-cache",
			"pragma":                    " no-cache",
			"refer":                     "https://s.taobao.com",
			"user-agent":                " Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.62 Safari/537.36",
			"cookie":                    cookieStr,
			"sec-ch-ua":                 "",
			"sec-ch-ua-platform":        "document",
			"sec-fetch-mode":            "navigate",
			"sec-fetch-site":            "same-site",
			"upgrade-insecure-requests": "1",
			"accept-encoding":           "",
		},
		"cookies": nullCookies,
	}, 20)

	if err != nil {
		log.Println("下载出错:", err)
		return
	}
	respData := map[string]interface{}{}
	err = json.Unmarshal(ret, &respData)
	if err != nil {
		log.Println("解析下载数据出错:", err)
		return
	}
	content := respData["content"].(string)
	bs, err := base64.StdEncoding.DecodeString(content)
	log.Println("详情页数据:", string(bs))
}
