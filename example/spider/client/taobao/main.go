package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	sgorm "gitee.com/smallow/spirit/lib/gorm"
	"gitee.com/smallow/spirit/msgbus"
	"gorm.io/gorm"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var client *msgbus.Client
var (
	defaultHeader = map[string]interface{}{
		"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
		"cache-control":   " no-cache",
		"pragma":          " no-cache",
		"user-agent":      " Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
	}
	//第一页url(商品搜索)
	mainSearchUrl = "https://s.taobao.com/search?q=%s&suggest=0_2&commend=all&ssid=s5-e&search_type=item&sourceId=tb.index&spm=a21bo.jianhua.201856-taobao-item.2&ie=utf8&initiative_id=tbindexz_20170306&_input_charset=utf-8&wq=&suggest_query=&source=suggest"
	//第二、三、四、五页url (商品搜索)
	paginationUrls = []string{
		"https://s.taobao.com/search?data-key=s&data-value=44&ajax=true&_ksTS=1653898916859_750&callback=jsonp751&q=%s&suggest=history_1&commend=all&ssid=s5-e&search_type=item&sourceId=tb.index&spm=a21bo.jianhua.201856-taobao-item.2&ie=utf8&initiative_id=tbindexz_20170306&_input_charset=utf-8&wq=&suggest_query=&source=suggest&bcoffset=1&ntoffset=7&p4ppushleft=2%2C48",
		"https://s.taobao.com/search?data-key=s&data-value=88&ajax=true&_ksTS=1652773924882_1004&callback=jsonp1005&q=%s&imgfile=&js=1&stats_click=search_radio_all%3A1&initiative_id=staobaoz_20220517&ie=utf8&bcoffset=-2&ntoffset=4&p4ppushleft=2%2C48&s=44",
		"https://s.taobao.com/search?data-key=s&data-value=132&ajax=true&_ksTS=1652774095641_1178&callback=jsonp1179&q=%s&imgfile=&js=1&stats_click=search_radio_all%3A1&initiative_id=staobaoz_20220517&ie=utf8&bcoffset=-5&ntoffset=1&p4ppushleft=2%2C48&s=88",
		"https://s.taobao.com/search?data-key=s&data-value=176&ajax=true&_ksTS=1652774191027_1413&callback=jsonp1414&q=%s&imgfile=&js=1&stats_click=search_radio_all%3A1&initiative_id=staobaoz_20220517&ie=utf8&bcoffset=-8&ntoffset=-2&p4ppushleft=2%2C48&s=132",
	}
	//半自动cookie字符串 下载手动修改
	cookieStr         = "xlly_s=1; _samesite_flag_=true; cookie2=1cec76a2d3565d67df3a9c5edea89c58; t=f48e30a11e2a40456b1efabec802306b; sgcookie=E100GPjW3itIS%2BnQwitACvyol7L3u2BrDqF7q%2Fs1IhSr9RFxX3l3PVpIjdO1CHxmCcDS%2BWPnqm4XOCAdH6C0iMroWbCMWXO5eukUaEv7lPExvkzhFLfBipvkJmdIpts%2BOv57; enc=80Je4CxKdpYC06JJ%2BMifRXLC6mtA6LWNiC5t%2BFt%2FcjnZ7KcHpL4T5lAnA%2BvpiAJ%2FggQ8WktiOLM1fSxtpuD4Jg%3D%3D; thw=cn; uc1=cookie14=UoexNgT9LPm3lA%3D%3D; cna=niseGyoXKVUCAXWg2RiRFUIc; v=0; mt=ci%3D-1_1; _tb_token_=f5e3d35b88ab1; _m_h5_tk=3c2e23410e5d57c47457085430905d04_1654162646699; _m_h5_tk_enc=0c158ff8c5c15e5a6cc577f08d9b1b79; JSESSIONID=A625755A7CB42B670345E6F172B51AF0; tfstk=ceYRBPOUNxDk1rCv_3nc8tLtvFccZUmRH71YJe9Dg4aQK64diaYMWViqVtWNejC..; l=eBLCHflPLpsMNCALBO5Zourza77OFIRb81PzaNbMiInca1-f9FO-uNChBeCBWdtjgt5jpetriLCmodHM8xa_Wxwpp4wpRs5mpI968e1..; isg=BOPj1w4JhVLFnElrzFIinr1ScieN2Hcaz7vLbhVAkcK5VAJ2nanjaLbGTiTac88S; x5sec=7b227365617263686170703b32223a22333162353238663738323635393261623766336166343066396339373939653043507a48345a5147454f58676b34484d2f73537a74414561437a4d794d6a41354e5463354e4473784d4b6546677037382f2f2f2f2f77453d227d"
	mainSearchCookies = make([]*http.Cookie, 0) //自动化爬取数据时候用到的动态cookie目前没有用到
	nullCookies       = make([]*http.Cookie, 0)
	f                 = false //是否持续爬数据
	interval          = 24    //爬取间隔,单位-小时
	alMsgServer       = "39.107.203.162:7070"
	localMsgServer    = "127.0.0.1:7070"
	needDetailRequest = false //是否爬取详情页
	searchKey         = "生姜"
)
var (
	db             *gorm.DB
	driverName     = "mysql"
	dataSourceName = "root:shenlan123@tcp(192.168.1.169:3306)/spider?charset=utf8mb4&parseTime=true&loc=Local"
	maxOpenConn    = 20
	maxIdleConn    = 10
)

/**
淘宝网首页搜索爬虫客户端
*/
func processEvent(p *msgbus.Packet) {
	event := int(p.Event)
	switch event {
	case msgbus.SERVICE_SPIDER_TAOBAO: //同步调用，原数据返回
		//client.WriteObj(p.From, p.Msgid, util.EVENT_RECIVE_CALLBACK, util.SENDTO_TYPE_P2P, p.GetBusinessData())
		/*param := make(map[string]interface{})
		json.Unmarshal(p.GetBusinessData(), &param)
		search(&param)*/
	}
}

func main2() {
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < 5; i++ {
		log.Println(rand.Intn(len(paginationUrls)))
		//time.Sleep(2 * time.Second)
	}
}

func init() {
	db = sgorm.NewGorm(dataSourceName, driverName, maxOpenConn, maxIdleConn, nil)
	if db == nil {
		log.Println("mysql DB pool created error")
		return
	}
	log.Printf("Seland Asset DB init successful [%s]", dataSourceName)
}

func main() {
	client1, err := msgbus.StartClient(nil, alMsgServer, "淘宝网查询客户端", []int{msgbus.SERVICE_SPIDER_TAOBAO})
	if err != nil {
		log.Println("err")
		return
	}
	client = client1
	search(searchKey, mainSearchCookies)
	//requestDetail("")

	/*ticker := time.NewTicker(15 * time.Second)
	for {
		select {
		case <-ticker.C:
			search("东北大米", mainSearchCookies)
		}
	}*/
	/*cnaCookie := cnaCookie()
	time.Sleep(1000)
	loginCookies := loginCookie()
	mainSearchCookies = append(mainSearchCookies, cnaCookie)
	mainSearchCookies = append(mainSearchCookies, loginCookies...)
	mainSearchCookies = append(mainSearchCookies, &http.Cookie{Name: "l", Value: "eBa9EGn7Lo7tQ8ujBOfaourza779GIR4SuPzaNbMiOCP_Q5J5FQCW6fhkX8vCnGVHs_Wf37GxW68BmYeCyCqJxpsw3k_J_DmndC.."})
	mainSearchCookies = append(mainSearchCookies, &http.Cookie{Name: "enc", Value: "TxwhIiPFgOcVSgQD8QKcFy009D1XZpv%2B%2FATFBO%2FPMLDVdU9mT9spBkusDYyoofIG6crAt5Vv9JIorDRowYa%2F4w%3D%3D"})
	search("东北大米", mainSearchCookies)*/
}

/**
商品列表搜索第一页 (页面跳转)
*/
func search(str string, cookies []*http.Cookie) {
	msgid := msgbus.UUID(8)
	searchUrl := fmt.Sprintf(mainSearchUrl, url.QueryEscape(str))
	ret, err := client.Call("", msgid, msgbus.SERVICE_DOWNLOAD, msgbus.SENDTO_TYPE_RAND_RECIVER, map[string]interface{}{
		"url":    searchUrl,
		"method": "get",
		"param":  map[string]interface{}{},
		"head": map[string]interface{}{
			"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
			"accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
			"cache-control":   " no-cache",
			"pragma":          " no-cache",
			"user-agent":      " Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
			"cookie":          cookieStr,
		},
		"cookies": cookies,
	}, 60)

	if err != nil {
		log.Println("下载出错:", err)
		return
	}
	respData := map[string]interface{}{}
	err = json.Unmarshal(ret, &respData)
	if err != nil {
		log.Println("解析下载数据出错:", err)
		return
	}
	content := respData["content"].(string)
	bs, err := base64.StdEncoding.DecodeString(content)
	//log.Println("下载到数据:",string(bs))
	if err != nil {
		log.Println("下载内容解密失败:", err)
		return
	}
	b := parseForSearchPage(string(bs), msgid)
	if b {
		if f {
			//首页数据获取到之后 模拟爬取第2-5页分页信息
			//lens := len(paginationUrls)
			rand.Seed(time.Now().UnixNano())
			for i, val := range paginationUrls {
				//i := rand.Intn(lens)
				if b := searchForPagination(i, fmt.Sprintf(val, url.QueryEscape(str)), cookieStr, cookies); b {
					time.Sleep(2 * time.Second)
				} else {
					log.Println("模拟分页下载出错,休息10秒")
					time.Sleep(10 * time.Second)
				}
			}
		}
	} else {
		log.Println("首页数据下载异常,请更换cookieStr 稍后重试!")
	}
}

/**
商品搜索翻页(ajax)
*/
func searchForPagination(pageNum int, url, cookiesStr string, cookies []*http.Cookie) bool {
	log.Printf("开始模拟下载第[%d]页数据..", pageNum)
	msgid := msgbus.UUID(8)
	//log.Println(cookies)
	//请求第二页
	ret, err := client.Call("", msgid, msgbus.SERVICE_DOWNLOAD, msgbus.SENDTO_TYPE_RAND_RECIVER, map[string]interface{}{
		"url":    url,
		"method": "get",
		"param":  map[string]interface{}{},
		"head": map[string]interface{}{
			"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
			"accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
			"ache-control":    " no-cache",
			"pragma":          " no-cache",
			"user-agent":      " Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
			"cookie":          cookiesStr,
		},
		"cookies": cookies,
	}, 60)

	if err != nil {
		log.Printf("searchForPagination [%d]页下载出错:[%v]", pageNum, err)
		return false
	}
	respData := map[string]interface{}{}
	err = json.Unmarshal(ret, &respData)
	if err != nil {
		log.Printf("searchForPagination [%d]页 解析下载数据出错:[%v]", pageNum, err)
		return false
	}

	if content, ok := respData["content"]; ok {
		c, _ := base64.StdEncoding.DecodeString(content.(string))
		if strings.Contains(string(c), "jsonp") {
			log.Printf("searchForPagination [%d]页 下载数据成功 ", pageNum)
			return true
		}
	}
	return false
}

func parseResponse(bs []byte) (code, content string, cookies []*http.Cookie) {
	respData := map[string]interface{}{}
	if bs != nil && len(bs) > 0 {
		err := json.Unmarshal(bs, &respData)
		if err != nil {
			return "", "", nil
		}
		contentByte, err := base64.StdEncoding.DecodeString(respData["content"].(string))
		cookies := make([]*http.Cookie, 0)
		if respData["cookie"] != nil {
			tmp := respData["cookie"].([]interface{})
			for _, i := range tmp {
				item := i.(map[string]interface{})
				cookies = append(cookies, &http.Cookie{Value: item["Value"].(string),
					Name: item["Name"].(string)})
			}
		}
		return respData["code"].(string), string(contentByte), cookies
	}
	return "", "", nil
}

/**
第一步访问https://www.taobao.com 拿到部分cookie
*/
func cnaCookie() *http.Cookie {
	log.Println("cnaCookie download start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	msgID := msgbus.UUID(8)
	defaultHeader["accept"] = "*/*"
	defaultHeader["referer"] = "https://www.taobao.com"
	defaultHeader["host"] = "log.mmstat.com"
	ret, err := client.Call("", msgID, msgbus.SERVICE_DOWNLOAD, msgbus.SENDTO_TYPE_RAND_RECIVER, map[string]interface{}{
		"url":     "https://log.mmstat.com/eg.js",
		"method":  "get",
		"param":   map[string]interface{}{},
		"head":    defaultHeader,
		"cookies": nullCookies,
	}, 60)
	if err != nil {
		log.Println("cnaCookie  fail:", err)
		return nil
	}
	_, _, cookies := parseResponse(ret)
	/*log.Printf("step1 resp code:[%s]\n content:[%s] \n cookies:[%v]", code, content, cookies)*/
	log.Println("cnaCookie download end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
	if cookies != nil {
		return &http.Cookie{Name: cookies[0].Name, Value: cookies[0].Value}
	}
	return nil
}

/**
获取登陆页面部分cookie
*/
func loginCookie() []*http.Cookie {
	msgID := msgbus.UUID(8)
	defaultHeader["referer"] = "https://www.taobao.com"
	defaultHeader["host"] = "www.taobao.com"
	ret, err := client.Call("", msgID, msgbus.SERVICE_DOWNLOAD, msgbus.SENDTO_TYPE_RAND_RECIVER, map[string]interface{}{
		"url":     "https://login.taobao.com",
		"method":  "get",
		"param":   map[string]interface{}{},
		"head":    defaultHeader,
		"cookies": nullCookies,
	}, 60)
	if err != nil {
		log.Println("loginCookie  fail:", err)
		return nil
	}
	_, _, cookies := parseResponse(ret)
	//log.Println("loginCookie:", cookies)
	return cookies
}

/**
测试下载demo
*/
func searchTest() {
	msgid := msgbus.UUID(8)
	cookie1 := &http.Cookie{
		Name:   "ds",
		Value:  "req.AddCookie",
		Path:   "/",
		Domain: "localhost",
	}
	cookie2 := &http.Cookie{
		Name:   "_token",
		Value:  "sdfsdfsdfljsdf888333",
		Path:   "/",
		Domain: "localhost",
	}
	cookies := make([]*http.Cookie, 0)
	cookies = append(cookies, cookie1, cookie2)
	ret, err := client.Call("", msgid, msgbus.SERVICE_DOWNLOAD, msgbus.SENDTO_TYPE_RAND_RECIVER, map[string]interface{}{
		"url":      "http://localhost:7001/seland/manage/user/test1?name=whd&age=18",
		"method":   "post",
		"encoding": "utf-8",
		"useProxy": false,
		"isHttps":  false,
		"param": map[string]interface{}{
			"name": "王会东",
			"age":  10,
		},
		"head": map[string]interface{}{
			"Content-Type": "application/x-www-form-urlencoded",
			"accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
			"user-agent":   "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.88 Safari/537.36",
		},
		"cookies": cookies,
	}, 60)
	if err != nil {
		log.Println("err:", err)
	} else {
		log.Println("收到下载结果.......:", string(ret))

	}

}
