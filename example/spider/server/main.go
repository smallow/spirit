package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"gitee.com/smallow/spirit/msgbus"
	"github.com/pkg/errors"
	"log"
	"math/rand"
	"net"
	"sync"
	"time"
)

type Client struct {
	conn            net.Conn
	timestamp       int64
	name            string //
	canhandlerevent []int  //
	ip              string
	id              string
}

var lock sync.Mutex

//所有的请求
var allclient map[string]*Client = make(map[string]*Client)

//服务与提供者对应表
var allservice map[int][]string = make(map[int][]string)

func GC() {
	now := time.Now().Unix()
	now_bs := msgbus.Int2Byte(int32(now))
	for k, v := range allclient {
		if now-v.timestamp > gcinterval*3 {
			//3次GC未回应心跳
			v.conn.Close()
			removeClient(k)
			continue
		} else if now-v.timestamp > gcinterval {
			_, err := v.conn.Write(msgbus.Enpacket("", k, "", msgbus.EVENT_REQUEST_HEARTBEAT,
				msgbus.SENDTO_TYPE_P2P, now_bs))
			if err != nil { //发心跳包出错
				v.conn.Close()
				removeClient(k)
			}
		}

	}
	time.AfterFunc(time.Duration(gcinterval)*time.Second, GC)
}

//删除服务节点
func removeClient(myid string) {
	lock.Lock()
	defer lock.Unlock()
	defer msgbus.Catch()
	if v, ok := allclient[myid]; ok {
		v.conn.Close()
	}
	delete(allclient, myid)
	for k, v := range allservice {
		for j, smid := range v {
			if smid == myid {
				allservice[k] = append(v[:j], v[j+1:]...)
				break
			}
		}
	}

	fmt.Println()
	fmt.Println("删除节点", myid, "全部服务", allservice)
}

//更新心跳
func updateheartbeat(from string) {
	lock.Lock()
	defer lock.Unlock()
	defer msgbus.Catch()
	if v, ok := allclient[from]; ok {
		v.timestamp = time.Now().Unix()
	}
}

//发布服务
func publishservice(myid string, s []int) {
	if myid == "00000000" { //无效请求验证
		return
	}
	lock.Lock()
	defer lock.Unlock()
	defer msgbus.Catch()
	if allclient[myid] != nil {
		allclient[myid].canhandlerevent = s
		//删除该ID以前发布的服务
		for k, v := range allservice {
			for j, smid := range v {
				if smid == myid {
					allservice[k] = append(v[:j], v[j+1:]...)
					break
				}
			}
		}
		//追加新发布的服务
		for _, v := range s {
			if v == msgbus.EVENT_RECIVE_CALLBACK { //回调的不再作为服务注册
				continue
			}
			allservice[v] = append(allservice[v], myid)
		}
	}
	fmt.Println()
	fmt.Println("全部服务", allservice)
}

//查看所有服务提供者
func viewallservice(p *msgbus.Packet) {
	defer msgbus.Catch()
	ret := []interface{}{}
	lock.Lock()
	for k, v := range allclient {
		ret = append(ret, map[string]interface{}{
			"handle": fmt.Sprintf("%v", v.canhandlerevent),
			"name":   v.name,
			"myid":   k,
			"ip":     v.ip,
		})
	}
	lock.Unlock()
	bs, _ := json.Marshal(ret)
	allclient[p.From].conn.Write(msgbus.Enpacket("", p.From, p.Msgid, msgbus.EVENT_RECIVE_CALLBACK, msgbus.SENDTO_TYPE_P2P, bs))
}

//更新客户端名称
func updatemyname(myid, name string) {
	fmt.Println("updatename", myid, name)
	lock.Lock()
	defer lock.Unlock()
	if v, ok := allclient[myid]; ok {
		v.name = name
	}
}

//通过名称查找客户端
func findClientByName(name string) (*Client, error) {
	for _, c := range allclient {
		if c.name == name {
			return c, nil
		}
	}
	return nil, errors.New("cant't find the client")
}

//处理客户端发过来的消息
func processmsg(msg *msgbus.Packet) {
	defer msgbus.Catch()
	from := msg.From
	event := int(msg.Event)
	//直接更新发送人心跳
	updateheartbeat(from)
	switch event {
	//TODO 只写需要特殊处理的时间，其他都走default
	case msgbus.EVENT_RETURN_HEARTBEAT: //心跳回应包处理
		fmt.Print(".")
	case msgbus.EVENT_PUBLISH_MYSERVICES: //客户端发布了自己的服务
		data := msg.GetBusinessData()
		services := []int{}
		for i := 0; i < len(data)/4; i++ {
			start := i * 4
			service := int(msgbus.Byte2Int(data[start : start+4]))
			services = append(services, service)
		}
		publishservice(from, services)
	case msgbus.EVENT_VIEWALL_SERVICE: //
		go viewallservice(msg)
	case msgbus.EVENT_REMOVE_CLIENT: //服务端强制删除节点
		removeClient(string(msg.GetBusinessData()))
	case msgbus.EVENT_BYE: //客户端主动要求断开
		removeClient(from)
	case msgbus.EVENT_SYSTEM_COMMAND: //转发系统消息
		if v, ok := allclient[msg.To]; ok {
			_, err := v.conn.Write(msg.Raw)
			if err == nil {
				updateheartbeat(from)
			}
		}
	case msgbus.EVENT_UPDATE_MYNAME:
		updatemyname(from, string(msg.GetBusinessData()))
	case msgbus.EVENT_GET_ALLDOWNLOADER:
		if v, ok := allservice[msgbus.SERVICE_DOWNLOAD]; ok {
			if v1, ok1 := allclient[msg.From]; ok1 {
				v1.conn.Write(msgbus.Enpacket("", msg.From, msg.Msgid, msgbus.EVENT_RECIVE_CALLBACK, msgbus.SENDTO_TYPE_P2P, v))
			}
		}
	default: //处理业务事件
		//识别发送类型
		sttype := int(msg.SentToType)
		log.Println("sendtotype:", sttype)
		if sttype == msgbus.SENDTO_TYPE_ALL { //发送给所有节点
			for service_machine_id, v := range allclient { //发所有，不支持的不处理
				if service_machine_id == from { //广播不用发给自己
					continue
				}
				_, err := v.conn.Write(msg.Raw)
				if err == nil {
					updateheartbeat(service_machine_id)
				}
			}
		} else if sttype == msgbus.SENDTO_TYPE_P2P { //点对点发消息，不用注册服务
			if v2, ok2 := allclient[msg.To]; ok2 {
				_, err := v2.conn.Write(msg.Raw)
				if err == nil {
					updateheartbeat(msg.To)
				}
			}
		} else if sttype == msgbus.SENDTO_TYPE_P2P_BYNAME { //通过名称点对点发消息
			if v2, err2 := findClientByName(msg.To); err2 == nil {
				_, err := v2.conn.Write(msg.Raw)
				if err == nil {
					updateheartbeat(v2.id)
				}
			}
		}
		if v, ok := allservice[event]; ok {
			switch sttype {
			case msgbus.SENDTO_TYPE_RAND_RECIVER: //随机选择一个节点提供服务,允许出错尝试3次
				for i := 0; i < 3; i++ {
					if len(v) < 1 {
						break
					}
					service_machine_id := v[rand.Intn(len(v))]
					log.Printf("随机处理,选择处理id:[%s]", service_machine_id)
					lock.Lock()
					client, ok := allclient[service_machine_id]
					lock.Unlock()
					if ok {
						_, err := client.conn.Write(msg.Raw)
						if err == nil {
							updateheartbeat(service_machine_id)
							break
						} else {
							removeClient(service_machine_id)
						}
					}

				}
			case msgbus.SENDTO_TYPE_ALL_RECIVER:
				if v, ok := allservice[event]; ok {
					for _, service_machine_id := range v {
						if service_machine_id == from { //广播不用发给自己
							continue
						}
						if v2, ok2 := allclient[service_machine_id]; ok2 {
							_, err := v2.conn.Write(msg.Raw)
							if err == nil {
								updateheartbeat(service_machine_id)
							}
						}
					}
				}
			}

		}

	}
}

var port string
var gcinterval int64

func main() {
	flag.StringVar(&port, "p", "7070", "开放端口")
	flag.Int64Var(&gcinterval, "g", 10, "GC间隔时间")
	flag.Parse()
	//心跳检测
	go GC()
	//启动服务
	msgbus.StartServer(func(data *msgbus.Packet) {
		//接受消息处理
		processmsg(data)
	}, func(c net.Conn) { //连接后返回UUID
		uuid := msgbus.UUID(8)
		//防止重复
		for {
			if allclient[uuid] == nil {
				break
			} else {
				uuid = msgbus.UUID(8)
			}
		}
		c.Write(msgbus.Enpacket("", "", "", msgbus.EVENT_RETURN_MACHINE_ID, msgbus.SENDTO_TYPE_P2P, []byte(uuid)))
		allclient[uuid] = &Client{conn: c,
			timestamp: time.Now().Unix(),
			name:      c.RemoteAddr().String(),
			ip:        c.RemoteAddr().String(),
			id:        uuid,
		}
	}, ":"+port)

}
