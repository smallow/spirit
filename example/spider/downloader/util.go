package main

import (
	"bytes"
	"encoding/binary"
)

//
func uint32tobyte(src uint32) []byte {
	buf := bytes.NewBuffer([]byte{})
	binary.Write(buf, binary.BigEndian, src)
	return buf.Bytes()
}

//
func uint64tobyte(src uint64) []byte {
	buf := bytes.NewBuffer([]byte{})
	binary.Write(buf, binary.BigEndian, src)
	return buf.Bytes()
}

//
func bytetouint32(src []byte) uint32 {
	var ret uint32
	binary.Read(bytes.NewReader(src), binary.BigEndian, &ret)
	return ret
}

//
func bytetouint64(src []byte) uint64 {
	var ret uint64
	binary.Read(bytes.NewReader(src), binary.BigEndian, &ret)
	return ret
}
