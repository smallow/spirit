package main

import (
	"gitee.com/smallow/spirit/msgbus"
	"log"

	"net"
)

var udpclient msgbus.UdpClient
var serveraddr *net.UDPAddr

//
func startUdpListen() {
	defer msgbus.Catch()
	/*if conf.ServerUdpAddr == "" || conf.LocalUdpAddr == "" { //没配置的就不管了
		return
	}*/
	serveraddr, _ = net.ResolveUDPAddr("udp4"," conf.ServerUdpAddr")
	udpclient = msgbus.UdpClient{Local: localUdpAddr, BufSize: 128}
	udpclient.Listen(func(act byte, data []byte, ra *net.UDPAddr) {
		switch act {
		case msgbus.OP_GET_DOWNLOADERCODE: //返回我的机器码
			udpclient.WriteUdp([]byte(client.GetMyclient()), msgbus.OP_JOIN, serveraddr)
		case msgbus.OP_WILLCHANGEIP: //将要切换IP了,先把我的所有服务下掉
			client.WriteObj("", "", msgbus.EVENT_PUBLISH_MYSERVICES, msgbus.SENDTO_TYPE_P2P, []byte{})
		}
	})
}

//
func joinUdpGroup() {
	defer msgbus.Catch()
	myid := client.GetMyclient()
	log.Println("加入:", myid)
	udpclient.WriteUdp([]byte(myid), msgbus.OP_JOIN, serveraddr)
}
