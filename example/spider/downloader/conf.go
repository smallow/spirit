package main

var conf struct {
	MsgBusServer  string `json:"msg_bus_server"`
	TimeOut       int    `json:"time_out"`
	Sleep         int    `json:"sleep"`
	RetryTimes    int    `json:"retry_times"`
	RetrySleep    int    `json:"retry_sleep"`
	Gui           bool   `json:"gui"`
	Name          string `json:"name"`
	LocalUdpAddr  string `json:"local_udp_addr"`
	ServerUdpAddr string `json:"server_udp_addr"`
	IsFile        bool   `json:"is_file"`
}
