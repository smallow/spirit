package main

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"net/http"

	"gitee.com/smallow/spirit/msgbus"
	"gitee.com/smallow/spirit/spider"
	"gitee.com/smallow/spirit/utils"

	"net/url"
	"os"
	"runtime"
	"sync/atomic"
	"time"
)

//下载器的监控事件定义
const (
	EVENT_GET_DOWNLOAD_STATE = 17001 //
)

var (
	client             *msgbus.Client
	kb                 uint64
	totalReq, ok, fail uint32
	httpState          map[int]*uint32
	dateStr            string
	myName                                  = "下载器"
	timeout                                 = 60000
	sleep                                   = 100
	retryTimes                              = 3
	retrySleep                              = 500
	se                 *utils.SimpleEncrypt = &utils.SimpleEncrypt{Key: "@#X#$!swer*)(234x"}
	localUdpAddr  = "12346"
	serverUdpAddr = "127.0.0.1:12345"
	gui           = true
	channel       chan bool
)

func GC() {
	defer msgbus.Catch()
	now := time.Now()
	nowdatestr := now.Format("20060102")
	if nowdatestr != dateStr {
		httpState = make(map[int]*uint32)
		atomic.StoreUint32(&totalReq, 0)
		atomic.StoreUint32(&fail, 0)
		atomic.StoreUint32(&ok, 0)
		atomic.StoreUint64(&kb, 0)
		write2file()
		dateStr = nowdatestr
	}
	write2file()
	time.AfterFunc(10*time.Minute, GC)
}
func write2file() {
	defer msgbus.Catch()
	date := time.Now().Format("20060102")
	fi, _ := os.OpenFile("res/data.dat", os.O_CREATE|os.O_TRUNC|os.O_SYNC|os.O_RDWR, 0x666)
	defer fi.Close()
	bs := []byte{}
	bs = append([]byte(date))
	bs = append(bs, uint32tobyte(atomic.LoadUint32(&totalReq))...)
	bs = append(bs, uint32tobyte(atomic.LoadUint32(&ok))...)
	bs = append(bs, uint32tobyte(atomic.LoadUint32(&fail))...)
	bs = append(bs, uint64tobyte(atomic.LoadUint64(&kb))...)
	se.Encode(bs)
	fi.Write(bs)
}
func heartPrint() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	/*log.Printf("申请内存:%dM,分配内存:%dM,未使用内存:%dM,回收内存:%dM\n", m.HeapSys/(1024*1024), m.HeapAlloc/(1024*1024),
		m.HeapIdle/(1024*1024), m.HeapReleased/(1024*1024))
	fmt.Printf("申请内存:%dM,分配内存:%dM,未使用内存:%dM,回收内存:%dM\n", m.HeapSys/(1024*1024), m.HeapAlloc/(1024*1024),
		m.HeapIdle/(1024*1024), m.HeapReleased/(1024*1024))*/
	time.AfterFunc(10*time.Second, heartPrint)
}

/*func initGUI() {
	defer msgbus.Catch()
	mw, err := walk.NewMainWindow()
	if err != nil {
		log.Println("NewMainWindow:", err)
		return
	}
	icon, _ := walk.NewIconFromFile("res/icon.ico")
	view_icon, _ := walk.NewBitmapFromFile("res/view_icon.png")
	exit_icon, _ := walk.NewBitmapFromFile("res/exit_icon.png")
	ni, _ := walk.NewNotifyIcon()
	defer ni.Dispose()
	ni.SetIcon(icon)
	ni.SetToolTip("Seland资源下载器")
	ni.MouseDown().Attach(func(x, y int, button walk.MouseButton) {
		if button != walk.LeftButton {
			return
		}
		if err := ni.ShowCustom(
			"Seland下载助推器",
			"感谢你的支持，仅代表Seland团队向你致以真挚的感谢。"); err != nil {
			log.Fatal(err.Error())
		}
	})

	exitAction := walk.NewAction()
	exitAction.SetText("E&退出")
	exitAction.SetImage(exit_icon)
	exitAction.Triggered().Attach(func() {
		walk.App().Exit(0)
		os.Exit(0)
	})
	viewAction := walk.NewAction()
	viewAction.SetImage(view_icon)
	viewAction.SetText("V&查看运行状态") //显示今天下载量，出错量
	viewAction.Triggered().Attach(func() {
		msg := fmt.Sprintf("总下载次数:%d\n成功次数:%d\n失败次数:%d\n总流量占用:%.2fM\n", atomic.LoadUint32(&totalReq),
			atomic.LoadUint32(&ok),
			atomic.LoadUint32(&fail),
			float64(atomic.LoadUint64(&kb))/1024/1024)
		walk.MsgBox(mw, "下载器状态", msg, walk.MsgBoxIconInformation)
	})
	ni.ContextMenu().Actions().Add(viewAction)
	ni.ContextMenu().Actions().Add(exitAction)
	ni.SetVisible(true)
	mw.Run()
}*/

func processEvent(p *msgbus.Packet) {
	defer msgbus.Catch()
	channel <- true
	event := int(p.Event)
	switch event {
	case msgbus.SERVICE_DOWNLOAD:
		ret := make(map[string]interface{}, 0)
		log.Println("开始请求下载数据......")
		param := map[string]interface{}{}
		err := json.Unmarshal(p.GetBusinessData(), &param)
		if err != nil {
			log.Println("请求参数解析错误:", err)
			ret["code"] = "500"
			ret["content"] = err.Error()
			ret["cookie"] = []byte{}
		} else {
			downloadRequest := parseDownloadMsg(param)
			bs, cookie, err := spider.Download(downloadRequest)
			//log.Println("下载到数据长度:", len(bs))
			/*log.Println(bs[0],bs[1],bs[2],bs[3],bs[4])
			log.Println(bs[len(bs)-5],bs[len(bs)-4],bs[len(bs)-3],bs[len(bs)-2],bs[len(bs)-1])*/
			//client.WriteObj(p.From, p.Msgid, msgbus.EVENT_RECIVE_CALLBACK, msgbus.SENDTO_TYPE_P2P, bs)
			if err != nil {
				ret["code"] = "400"
				ret["content"] = ""
				ret["cookie"] = nil
				log.Println("下载失败:", err.Error())
			} else {
				ret["code"] = "200"
				//ret["content"] = bs
				ret["content"] = base64.StdEncoding.EncodeToString(bs)
				ret["content"] = bs
			}
			ret["cookie"] = cookie
			/*if cookie == nil {
				ret["cookie"] = []byte{}
			} else {
				ret["cookie"] = cookie
			}*/
		}
		//下载完成写回消息总线
		client.WriteObj(p.From, p.Msgid, msgbus.EVENT_RECIVE_CALLBACK, msgbus.SENDTO_TYPE_P2P, ret)
		<-channel
	}
}
func main() {
	utils.ReadConfig("res/config.json", &conf)
	myName = conf.Name
	timeout = conf.TimeOut
	retryTimes = conf.RetryTimes
	sleep = conf.Sleep
	retrySleep = conf.RetrySleep
	gui = conf.Gui
	channel = make(chan bool, 5)
	go GC() //每日凌晨清空统计数据
	go heartPrint()

	client, _ = msgbus.StartClient(processEvent, conf.MsgBusServer, myName, []int{msgbus.SERVICE_GETPROXY, msgbus.SERVICE_DOWNLOAD, EVENT_GET_DOWNLOAD_STATE})
	client.OnConnectSuccess = joinUdpGroup
	startUdpListen()
	/*if gui {
		initGUI()
	}*/
	/*if conf.ProxyPort != "" {
		fmt.Println("ProxyPort", conf.ProxyPort)
		server := goproxy.NewServer(":" + conf.ProxyPort)
		server.Start()
	} else {
		lock := make(chan bool)
		<-lock
	}*/

	lock := make(chan bool)
	<-lock
}

func parseDownloadMsg(param map[string]interface{}) *spider.HttpDownloadRequest {
	httpRequest := &spider.HttpDownloadRequest{
		Encoding: "utf8",
		UseProxy: false,
		IsHttps:  false,
	}
	httpRequest.Url = param["url"].(string)
	httpRequest.Method = param["method"].(string)
	if param["head"] != nil {
		httpRequest.Header = param["head"].(map[string]interface{})
	}
	if param["param"] != nil {
		httpRequest.Param = param["param"].(map[string]interface{})
	}
	var cookies []*http.Cookie // //cookies
	if tmp, ok := param["cookies"].([]interface{}); ok {
		cookies = spider.ParseHttpCookie(tmp)
		httpRequest.Cookies = cookies
	}
	if param["encoding"] != nil {
		httpRequest.Encoding = param["encoding"].(string)
	}
	if param["useProxy"] != nil {
		httpRequest.UseProxy = param["useProxy"].(bool)
	}
	if param["isHttps"] != nil {
		httpRequest.IsHttps = param["isHttps"].(bool)
	}
	return httpRequest
}

func escape(str string) string {
	return url.QueryEscape(str)
}
