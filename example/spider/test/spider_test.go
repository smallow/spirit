package test

import (
	"encoding/json"
	"gitee.com/smallow/spirit/socket/siface"
	"gitee.com/smallow/spirit/socket/snet"
	"gitee.com/smallow/spirit/spider"
	"io"
	"log"
	"net"
	"net/http"
	"testing"
)

//form表单参数  需要指定HttpHeader 的Content-Type:application/x-www-form-urlencoded
func Test_spider(t *testing.T) {
	url := "http://localhost:7001/seland/manage/user/test1?name=whd&age=18"
	param := map[string]interface{}{
		"name": "王会东",
		"age":  10,
	}
	cookie1 := &http.Cookie{
		Name:   "ds",
		Value:  "req.AddCookie",
		Path:   "/",
		Domain: "localhost",
	}
	cookie2 := &http.Cookie{
		Name:   "_token",
		Value:  "sdfsdfsdfljsdf888333",
		Path:   "/",
		Domain: "localhost",
	}
	cookies := make([]*http.Cookie, 0)
	cookies = append(cookies, cookie1, cookie2)
	head := map[string]interface{}{
		"Content-Type": "application/x-www-form-urlencoded",
		"accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"user-agent":   "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.88 Safari/537.36",
	}
	respBytes, respCookies, err := spider.Download2(url, "post", param, head, cookies, "utf8", false, false)
	if err != nil {
		log.Println("Download error:", err)
		return
	}
	log.Println("response info str:", string(respBytes))
	log.Println("response cookies :", respCookies)

}

//snet服务端
func Test_snetServer(t *testing.T) {
	s := snet.NewServer()
	s.SetOnConnStart(func(connection siface.IConnection) {
		log.Println("=====================>connection ID=", connection.GetConnID(), " prepare to starting")
		log.Println("Set Conn Name,Spirit MsgBus ......")
		connection.SetProperty("Name", "王惠东")
		connection.SetProperty("Home", "http://smallow.top")
		connection.SendMsg(202, []byte("DoConnection BEGIN"))
	})
	s.SetOnConnStop(func(connection siface.IConnection) {
		log.Println("=====================>connection ID=", connection.GetConnID(), " prepare to stop")
		connection.SendMsg(203, []byte("DoConnection STOP"))
		name, _ := connection.GetProperty("Name")
		log.Println("Conn Name:", name)
		home, _ := connection.GetProperty("Home")
		log.Println("Conn Home:", home)
	})
	s.AddRouter(1, &spider.DownloadRouter{Name: "下载器1"})
	s.Serve()
}

//snet客户端
func Test_snetClient(t *testing.T) {
	conn, _ := net.Dial("tcp", "127.0.0.1:7070")
	url := "http://localhost:7001/seland/manage/user/test1?name=whd&age=18"
	param := map[string]interface{}{
		"name": "王会东",
		"age":  10,
	}
	cookie1 := &http.Cookie{
		Name:   "ds",
		Value:  "req.AddCookie",
		Path:   "/",
		Domain: "localhost",
	}
	cookie2 := &http.Cookie{
		Name:   "_token",
		Value:  "sdfsdfsdfljsdf888333",
		Path:   "/",
		Domain: "localhost",
	}
	cookies := make([]*http.Cookie, 0)
	cookies = append(cookies, cookie1, cookie2)
	head := map[string]interface{}{
		"Content-Type": "application/x-www-form-urlencoded",
		"accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"user-agent":   "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.88 Safari/537.36",
	}

	request := &spider.HttpDownloadRequest{
		Url:     url,
		Param:   param,
		Header:  head,
		Cookies: cookies,
		Method:  "post",
	}
	bs, _ := json.Marshal(request)
	dp := &snet.Package{}
	msg1 := &snet.Message{ID: 1, DataLen: uint32(len(bs)), Data: bs}
	senData1, _ := dp.Pack(msg1)
	conn.Write(senData1)

	/*dp := &snet.Package{}
	msg1 := &snet.Message{ID: 1, DataLen: 6, Data: []byte("SPIRIT")}
	senData1, _ := dp.Pack(msg1)
	conn.Write(senData1)*/
	go func() {
		log.Println("client1 start reading response from server conn ......")
		for {
			headData := make([]byte, dp.GetHeadLen())
			_, err := io.ReadFull(conn, headData)
			if err != nil {
				log.Printf("client 1 read head error:[%v]", err)
				break
			}
			//拆包得到msgID和msgDataLen的message对象,没有data
			msg, err := dp.Unpack(headData)
			//log.Println("client1 unpack msgID:", msg.GetMessageID(), " msgLen:", msg.GetMsgLen())
			var data []byte
			msgID := msg.GetMessageID()
			if msg.GetMsgLen() > 0 {
				data = make([]byte, msg.GetMsgLen())
				//log.Println("client1 start read data:", data)
				_, err = io.ReadFull(conn, data)
				if err != nil {
					log.Printf("client 1 msgID=[%d] read body data error:[%v]", msgID, err)
					break
				}
				//log.Println("client1 read data:", data)
			}
			msg.SetData(data)
			log.Printf("---------------->client 1 msgID=[%d] received msg data:[%s]>----------------------", msgID, string(msg.GetData()))
		}

	}()

	select {}
}
