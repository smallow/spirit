package main

import (
	"fmt"
	"gitee.com/smallow/spirit/msgbus"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os/exec"
	"regexp"
	"time"
)

const (
	MSG_TPL = "%s(D:%.4fM U:%.4fM)"
)

//
type Config struct {
	Name             string  `json:"name"`
	User             string  `json:"user"`
	Pwd              string  `json:"pwd"`
	Interval         int     `json:"interval"`
	LocalUdpAddr     string  `json:"localudpaddr"`
	ServerUdpAddr    string  `json:"serverudpaddr"`
	ServerAddr       string  `json:"serveraddr"`
	VpsName          string  `json:"vpsname"`
	CheckAddr        string  `json:"checkaddr"`
	DownSpeedLimit   float64 `json:"downspeed"`
	UploadSpeedLimit float64 `json:"uploadspeed"`
	CheckSpeedAddr   string  `json:"checkspeedaddr"`
}

//
type DownloaderItem struct {
	Addr *net.UDPAddr
	Code string
}

var sysconfig Config
var myipreg *regexp.Regexp
var cache map[string]DownloaderItem = make(map[string]DownloaderItem)
var udpclient msgbus.UdpClient
var serveraddr *net.UDPAddr
var myoldip string

//
var client *msgbus.Client


func checkSpeed(addr string) (float64, float64) {
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		return 0, 0
	}
	defer conn.Close()
	t1 := time.Now()
	buf := make([]byte, 1024)
	for i := 0; i < 50; i++ {
		io.ReadFull(conn, buf)
	}
	dd := time.Since(t1)
	t1 = time.Now()
	for i := 0; i < 50; i++ {
		conn.Write(buf)
	}
	for {
		_, err := conn.Write(buf)
		if err != nil {
			break
		}
		time.Sleep(10 * time.Millisecond)
	}
	ud := time.Since(t1)
	dp, up := float64(0.05)/dd.Seconds(), float64(0.05)/ud.Seconds()
	msg := fmt.Sprintf(MSG_TPL, sysconfig.VpsName, float64(0.05)/dd.Seconds(), float64(0.05)/ud.Seconds())
	log.Println("网速测试,", msg)
	client.WriteObj("", "", msgbus.EVENT_UPDATE_MYNAME, msgbus.SENDTO_TYPE_P2P, []byte(msg))
	return dp, up
}

func pushNewDownloaders() {
	for i := 0; i < 10; i++ {
		if len(cache) < 1 {
			time.Sleep(20 * time.Second)
			continue
		} else {
			break
		}
	}
	if len(cache) < 1 {
		return
	}
	bs := []byte{}
	for _, v := range cache {
		bs = append(bs, []byte(v.Code)...)
	}
	ds, us := checkSpeed(sysconfig.CheckSpeedAddr)
	if sysconfig.DownSpeedLimit > 0 && ds < sysconfig.DownSpeedLimit {
		log.Println("网速太差，特殊爬虫不适用该节点")
		return
	}
	if sysconfig.UploadSpeedLimit > 0 && us < sysconfig.UploadSpeedLimit {
		log.Println("网速太差，特殊爬虫不适用该节点")
		return
	}
	//推数据到爬虫监控端
	log.Println("新的机器码:", string(bs))
	client.WriteObj("", "", msgbus.SERVICE_DOWNLOAD_APPEND_NODE, msgbus.SENDTO_TYPE_ALL_RECIVER, bs)
}



//取公网IP
func getMyIp() string {
	resp, err := http.Get(sysconfig.CheckAddr)
	if err != nil {
		return ""
	}
	defer resp.Body.Close()
	bs, _ := ioutil.ReadAll(resp.Body)
	ip := myipreg.FindString(string(bs))
	log.Println("IP--", ip, string(bs))
	return ip
}


//切换ip,保证与旧IP不重复
func changeIP() {
	for i := 0; i < 10; i++ {
		exec.Command("rasdial", sysconfig.Name, "/disconnect").Run()
		exec.Command("rasdial", sysconfig.Name, sysconfig.User, sysconfig.Pwd).Run()
		//清空
		for k, _ := range cache {
			delete(cache, k)
		}
		newip := getMyIp()
		if newip == "" {
			log.Println("取不到公网IP，可能拨号失败了")
			time.Sleep(1 * time.Minute)
		} else if myoldip != newip {
			log.Printf("你的公网IP从%s换成%s \n", myoldip, newip)
			myoldip = newip
			break
		} else {
			log.Println("取到的IP重复了")
		}
	}

}

func fireChangeIP() {
	log.Println("准备切换IP")
	//发布要换IP了
	bs := []byte{}
	for k, v := range cache {
		udpclient.WriteUdp([]byte{}, msgbus.OP_WILLCHANGEIP, v.Addr)
		bs = append(bs, []byte(v.Code)...)
		delete(cache, k)
	}
	//
	client.WriteObj("", "", msgbus.SERVICE_DOWNLOAD_DELETE_NODE, msgbus.SENDTO_TYPE_ALL_RECIVER, bs)
	//udpclient.WriteUdp(bs, mu.OP_DELETE_DOWNLOADERCODES, serveraddr)
	//等待30秒
	time.Sleep(30 * time.Second)
	//切换ip
	changeIP()
	pushNewDownloaders()
	//
	time.AfterFunc(time.Duration(sysconfig.Interval)*time.Minute, fireChangeIP)
}

func main() {
	time.AfterFunc(2*time.Minute, fireChangeIP) //5分钟后开始切换IP
	client, _ = msgbus.StartClient(func(p *msgbus.Packet) {}, sysconfig.ServerAddr, sysconfig.VpsName,
		[]int{})

	udpclient = msgbus.UdpClient{Local: sysconfig.LocalUdpAddr, BufSize: 128}
	udpclient.Listen(processUdpMsg)
	log.Println("Udp服务监听:", sysconfig.LocalUdpAddr)
	lock := make(chan bool)
	<-lock
}

func processUdpMsg(act byte, data []byte, ra *net.UDPAddr) {
	switch act {
	case msgbus.OP_JOIN:
		cache[ra.String()] = DownloaderItem{
			Addr: ra,
			Code: string(data),
		}
		log.Println(ra.String(), "加入")
	}
}
