package main

import (
	"gitee.com/smallow/spirit/websocket"
	"log"
	"net/http"
)

func main() {
	var (
		err error
		bs  []byte
	)

	http.Handle("/ws", websocket.Handler(func(connection *websocket.Connection) {
		for {
			if bs, err = connection.ReadMessage(); err != nil {
				goto ERR
			}
			log.Printf("收到ClientID:%s 的message:%s", connection.Id(), string(bs))
		}
	ERR:
		//TODO:关闭连接
		connection.Close()
	}))
	log.Printf("spirit websocket server starting successful,listening at :8999")
	http.ListenAndServe(":8999", nil)
}
