package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
)

var (
	nodes = []string{
		"http://127.0.0.1:2001",
		"http://127.0.0.1:2002",
		"http://127.0.0.1:2003",
		"http://127.0.0.1:2004",
	}
	currentIndex = 0
)

const (
	LbRandom int = iota
	LbRoundRobin
	LbWeightRoundRobin
	LbConsistentHash
)

func main() {
	var a = 1
	a1(a)
	fmt.Println(a)
	a2(&a)
	fmt.Println(a)
}

func a1(a int)  {
	a = 3
}
func a2(a *int)  {
	*a = 3
}

func main1() {
	proxy := NewMultipleHostsReverseProxy(parseUrlFromNodes(nodes), LbRandom)
	http.ListenAndServe(":2000", proxy)
}

func parseUrlFromNodes(strings []string) []*url.URL {
	var urls []*url.URL
	for _, node := range strings {
		url1, err1 := url.Parse(node)
		if err1 != nil {
			log.Println(err1)
		}
		urls = append(urls, url1)
	}
	return urls
}

func NewMultipleHostsReverseProxy(targets []*url.URL, balanceType int) *httputil.ReverseProxy {
	//请求协调者
	director := func(req *http.Request) {
		//url_rewrite
		//127.0.0.1:2002/dir/abc ==> 127.0.0.1:2003/base/abc ??
		//127.0.0.1:2002/dir/abc ==> 127.0.0.1:2002/abc
		//127.0.0.1:2002/abc ==> 127.0.0.1:2003/base/abc
		/*re, _ := regexp.Compile("^/dir(.*)")
		req.URL.Path = re.ReplaceAllString(req.URL.Path, "$1")*/
		var target *url.URL
		switch balanceType {
		case LbRandom:
			//随机负载均衡
			targetIndex := rand.Intn(len(targets))
			target = targets[targetIndex]
		case LbRoundRobin:
			//轮询负载均衡 算法:当前索引+1 然后对len(nodes)取余 得到下一个索引

		}
		if target == nil {
			log.Println("未找到反向代理下游节点,负载失败")
			return
		}
		targetQuery := target.RawQuery
		req.URL.Scheme = target.Scheme
		req.URL.Host = target.Host
		//todo 当对域名(非内网)反向代理时需要设置此项。当作后端反向代理时不需要
		req.Host = target.Host

		// url地址重写：重写前：/aa 重写后：/base/aa
		req.URL.Path = singleJoiningSlash(target.Path, req.URL.Path)
		if targetQuery == "" || req.URL.RawQuery == "" {
			req.URL.RawQuery = targetQuery + req.URL.RawQuery
		} else {
			req.URL.RawQuery = targetQuery + "&" + req.URL.RawQuery
		}
		if _, ok := req.Header["User-Agent"]; !ok {
			req.Header.Set("User-Agent", "user-agent")
		}
	}
	return &httputil.ReverseProxy{
		Director: director}
}

func singleJoiningSlash(a, b string) string {
	aslash := strings.HasSuffix(a, "/")
	bslash := strings.HasPrefix(b, "/")
	switch {
	case aslash && bslash:
		return a + b[1:]
	case !aslash && !bslash:
		return a + "/" + b
	}
	return a + b
}
