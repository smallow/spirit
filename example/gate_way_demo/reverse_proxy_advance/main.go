package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
)

/**
反向代理-返回内容修改
*/

var (
	proxyUrl      = "http://127.0.0.1:2003" //反向代理到的后端服务地址(先写死固定)
	port          = ":2000"
	modifyContent = "我是修改过的内容:\n"
)

func main() {
	proxy, err := url.Parse(proxyUrl)
	if err != nil {
		log.Panic(err)
	}
	reverseProxy := httputil.NewSingleHostReverseProxy(proxy)
	reverseProxy.ModifyResponse = func(response *http.Response) error {
		/*if response.StatusCode != 200 {

		}*/
		oldPayload, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return err
		}
		/**
		修改返回内容时候除了要修改body ,重要的Content-Length属性必须设置
		*/
		newPayload := []byte(modifyContent + string(oldPayload))
		response.Body = ioutil.NopCloser(bytes.NewBuffer(newPayload))
		response.ContentLength = int64(len(newPayload))
		//
		response.Header.Set("Content-Length", fmt.Sprint(len(newPayload)))
		return nil
	}
	reverseProxy.ErrorHandler = func(res http.ResponseWriter, req *http.Request, err error) {
		res.Write([]byte(err.Error()))
	}
	http.ListenAndServe(port, reverseProxy)
}
