package main

import (
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
)

/**
第一反向代理
*/

var (
	proxyUrl = "http://127.0.0.1:2000"
	port     = ":1999"
)

func main() {
	//httputil.NewSingleHostReverseProxy的话 不能在第一代理上做更多的自定义设置
	//NewSingleHostReverseProxy方法只设置了 ReverseProxy的Director参数的部分信息,所以需要修改Director参数的设置
	proxy, err := url.Parse(proxyUrl)
	if err != nil {
		log.Panic(err)
	}
	reverseProxy := NewSingleHostReverseProxy(proxy)
	http.ListenAndServe(port, reverseProxy)
}

func NewSingleHostReverseProxy(target *url.URL) *httputil.ReverseProxy {
	targetQuery := target.RawQuery
	director := func(req *http.Request) {
		req.URL.Scheme = target.Scheme
		req.URL.Host = target.Host
		req.URL.Path = singleJoiningSlash(target.Path, req.URL.Path)
		if targetQuery == "" || req.URL.RawQuery == "" {
			req.URL.RawQuery = targetQuery + req.URL.RawQuery
		} else {
			req.URL.RawQuery = targetQuery + "&" + req.URL.RawQuery
		}
		if _, ok := req.Header["User-Agent"]; !ok {
			// explicitly disable User-Agent so it's not set to default value
			req.Header.Set("User-Agent", "")
		}
		/**
		修改这一个地方:在第一代理这里设置x-real-ip,下游的以保证在后端server使用X-real-ip获取的客户端地址的不可伪造
		*/
		req.Header.Set("X-Real-Ip", req.RemoteAddr)
		//---------------------------------------------
	}
	return &httputil.ReverseProxy{Director: director}
}

func singleJoiningSlash(a, b string) string {
	aslash := strings.HasSuffix(a, "/")
	bslash := strings.HasPrefix(b, "/")
	switch {
	case aslash && bslash:
		return a + b[1:]
	case !aslash && !bslash:
		return a + "/" + b
	}
	return a + b
}
