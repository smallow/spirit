package main

import (
	"fmt"
	"io"
	"net/http"
)

type SpiritProxy struct{}

func (p *SpiritProxy) ServeHTTP(rw http.ResponseWriter, request *http.Request) {
	fmt.Printf("Received request %s %s %s \n", request.Method, request.Host, request.RemoteAddr)
	transport := http.DefaultTransport
	// step 1，浅拷贝对象，然后就再新增属性数据
	//outReq := new(http.Request)
	//*outReq = *request
	/*if clientIP, _, err := net.SplitHostPort(request.RemoteAddr); err == nil {
		if prior, ok := outReq.Header["X-Forwarded-For"]; ok {
			clientIP = strings.Join(prior, ", ") + ", " + clientIP
		}
		outReq.Header.Set("X-Forwarded-For", clientIP)
	}*/
	// step 2, 请求下游
	res, err := transport.RoundTrip(request)
	if err != nil {
		rw.WriteHeader(http.StatusBadGateway)
		return
	}

	// step 3, 把下游请求内容返回给上游
	for key, value := range res.Header {
		for _, v := range value {
			rw.Header().Add(key, v)
		}
	}
	rw.WriteHeader(res.StatusCode)
	io.Copy(rw, res.Body)
	res.Body.Close()
	/*defer res.Body.Close()
	bufio.NewReader(res.Body).WriteTo(rw)*/
}

func main() {
	fmt.Println("正向代理示例Demo")
	http.Handle("/", &SpiritProxy{})
	http.ListenAndServe(":8080", nil)
}
