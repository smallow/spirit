package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
)

/**
两种方式实现反向代理
*/

var (
	proxyUrl = "http://127.0.0.1:2003" //反向代理到的后端服务地址(先写死固定)
	port     = ":2000"
)

/**
方式1: go原生代码字节流复制转发
      代理两个重要得属性即可URL.Host,URL.Scheme
*/
func main1() {
	proxy, err := url.Parse(proxyUrl)
	if err != nil {
		log.Panic(err)
	}
	http.HandleFunc("/", func(writer http.ResponseWriter, req *http.Request) {
		//代理两个重要得属性即可
		req.URL.Host = proxy.Host
		req.URL.Scheme = proxy.Scheme
		//转发给下游代理
		defaultTransport := http.DefaultTransport
		resp, err := defaultTransport.RoundTrip(req)
		if err != nil {
			log.Println("middleware proxy transport error:", err)
		}
		for key, val := range resp.Header {
			for _, v := range val {
				writer.Header().Add(key, v)
			}
		}
		defer resp.Body.Close()
		bufio.NewReader(resp.Body).WriteTo(writer)
	})
	http.ListenAndServe(port, nil)
}

/**
方式2: go http工具包提供的ReverseProxy
*/

func main() {
	proxy, err := url.Parse(proxyUrl)
	if err != nil {
		log.Panic(err)
	}
	reverseProxy := httputil.NewSingleHostReverseProxy(proxy)
	/**
	设置了reverseProxy在listenAndServe里的话 下边的handleFunc都不生效
	*/
	http.HandleFunc("/abc", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprintln(writer, "2000端口应用/abc的响应")
	})
	//-----------
	http.ListenAndServe(port, reverseProxy)
}
