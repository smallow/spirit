package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type SpiritHttpServer struct {
	Addr string
}

func (s *SpiritHttpServer) Run() {
	defaultMux := http.NewServeMux()
	defaultMux.HandleFunc("/", func(writer http.ResponseWriter, req *http.Request) {
		remoteAddr := fmt.Sprintf("RemoteAddr:%s\n", req.RemoteAddr)
		xForwardFor := fmt.Sprintf("X-Forwarded-For:[%v]\n", req.Header.Get("X-Forwarded-For"))
		xRealIP := fmt.Sprintf("X-Real-Ip:[%v]", req.Header.Get("X-Real-Ip"))
		fmt.Fprintln(writer,"请求url:",req.URL.String())
		fmt.Fprintf(writer, "***************\n [%s]响应\n%s%s%s \n header:[%v] \n***************", s.Addr, remoteAddr, xForwardFor, xRealIP, req.Header)
	})
	server := &http.Server{
		Addr:         s.Addr,
		WriteTimeout: time.Second * 3,
		Handler:      defaultMux,
	}
	go func() {
		log.Fatal(server.ListenAndServe())
	}()
}

func main() {
	rs1 := &SpiritHttpServer{Addr: ":2001"}
	rs1.Run()
	rs2 := &SpiritHttpServer{Addr: ":2002"}
	rs2.Run()
	rs3 := &SpiritHttpServer{Addr: ":2003"}
	rs3.Run()
	rs4 := &SpiritHttpServer{Addr: ":2004"}
	rs4.Run()
	//监听关闭信号
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
}
