package main

import (
	"context"
	"go.etcd.io/etcd/clientv3"
	"log"
	"time"
)

/**
服务注册及健康检查
*/

/**
创建租约服务
*/
type ServiceRegister struct {
	cli           *clientv3.Client
	leaseID       clientv3.LeaseID
	keepAliveChan <-chan *clientv3.LeaseKeepAliveResponse //租约keepalive相应的chan
	key           string
	val           string
}

func NewServiceRegister(endpoints []string, key, val string, lease int64) (*ServiceRegister, error) {
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: 5 * time.Second,
	})
	if err != nil {
		log.Fatal(err)
	}
	ser := &ServiceRegister{
		cli: cli,
		key: key,
		val: val,
	}
	//申请租约设置时间keepalive
	if err := ser.putKeyWithLease(lease); err != nil {
		log.Fatal(err)
	}
	return ser, nil
}

/**
设置服务租约
*/
func (s *ServiceRegister) putKeyWithLease(lease int64) error {
	//设置租约时间
	resp, err := s.cli.Grant(context.Background(), lease)
	if err != nil {
		log.Println("putKeyWithLease err:", err)
		return err
	}
	//注册服务并绑定租约
	_, err = s.cli.Put(context.Background(), s.key, s.val, clientv3.WithLease(resp.ID))
	if err != nil {
		log.Println("注册服务并绑定租约 err:", err)
		return err
	}
	//设置续租 定期发送需求请求
	leaseRespChan, err := s.cli.KeepAlive(context.Background(), resp.ID)
	if err != nil {
		log.Println("KeepAlive err:", err)
		return err
	}
	s.leaseID = resp.ID
	s.keepAliveChan = leaseRespChan
	log.Printf("注册服务key:[%s] val:[%s] 成功!", s.key, s.val)
	return nil
}

/**
监听续租情况
*/
func (s *ServiceRegister) ListenLeaseRespChan() {
	for leaseKeepResp := range s.keepAliveChan {
		log.Printf("租约ID[%v]续约成功", leaseKeepResp)
	}
	log.Println("关闭续租")
}

// Close 注销服务
func (s *ServiceRegister) Close() error {
	//撤销租约
	if _, err := s.cli.Revoke(context.Background(), s.leaseID); err != nil {
		return err
	}
	log.Println("主动撤销租约")
	return s.cli.Close()
}

func main() {
	var endpoints = []string{"192.168.20.244:12379"}
	ser, err := NewServiceRegister(endpoints, "/user", "localhost:8080", 5)
	if err != nil {
		log.Fatalln(err)
	}
	//监听续租相应chan
	go ser.ListenLeaseRespChan()
	select {
	// case <-time.After(20 * time.Second):
	// 	ser.Close()
	}
}
