module ms

require (
	gitee.com/smallow/srpc v0.0.143
	github.com/coreos/etcd v3.3.25+incompatible
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/google/uuid v1.2.0 // indirect
	go.etcd.io/etcd v3.3.25+incompatible
	google.golang.org/grpc v1.36.0
	sigs.k8s.io/yaml v1.2.0 // indirect

)

replace gitee.com/smallow/srpc => ../../srpc_dev1.0

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0

go 1.13
