package rpc_test

import (
	"context"
	"fmt"
	"gitee.com/smallow/srpc/infrastructure/proto/pb"
	"google.golang.org/grpc"
	"log"
	"testing"
)

var (
	conn                 *grpc.ClientConn
	userServiceClient    pb.UserServiceClient
	companyServiceClient pb.CompanyServiceClient
)


func init() {
	//1、连接服务端
	c, err := grpc.Dial(":7070", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
		return
	}
	conn = c
	//2、实例化一个客户端
	userServiceClient = pb.NewUserServiceClient(conn)
	companyServiceClient = pb.NewCompanyServiceClient(conn)
	//defer conn.Close()
}

func Test_infrastructureCreateUser(t *testing.T) {
	//3、组装请求参数
	body := pb.CreateUserReq{Name: "李四", Phone: "18800868089", Memo: "带有公司ID和部门ID的人员测试", CompanyID: 5, CompanyName: "河南华星", DepartmentID: 2, DepartmentName: "部门2"}
	//4、调用接口
	resp, err := userServiceClient.CreateUser(context.Background(), &body)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(resp)
}

func Test_relateUserToCompanyAndDep(t *testing.T) {
	resp, err := userServiceClient.RelateUserToCompanyAndDep(context.Background(),
		&pb.RelateUserToCompanyAndDepReq{UserID: 22, UserName: "张三", CompanyID: 5, CompanyName: "河南华星", DepartmentID: 1, DepartmentName: "部门1"})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(resp)
}

func Test_infrastructureCreateCompany(t *testing.T) {
	resp, err := companyServiceClient.CreateCompany(context.Background(),
		&pb.CreateCompanyReq{
			Name:        "郑州兴城计算机",
			CompanyType: pb.CompanyType_CoLTD,
			ShortName:   "郑州兴城",
			LegalPerson: "李星",
			UnifyCode:   "4220093fNdd28",
			Address:     "金水东路128号",
		})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(resp)
}

func Test_infrastructureCreateDepartment(t *testing.T) {
	departments := []*pb.CreateDep{
		&pb.CreateDep{Name: "部门1", ShortName: "部门1", CompanyID: 6, CompanyName: "郑州兴城计算机", ParentID: 6, ParentName: "郑州兴城计算机"},
		&pb.CreateDep{Name: "部门2", ShortName: "部门2", CompanyID: 6, CompanyName: "郑州兴城计算机", ParentID: 6, ParentName: "郑州兴城计算机"},
		&pb.CreateDep{Name: "部门1-1", ShortName: "部门1-1", CompanyID: 5, CompanyName: "河南智星网络科技有限公司", ParentID: 1, ParentName: "部门1"},
	}
	resp, err := companyServiceClient.CreateDepartment(context.Background(), &pb.CreateDepReq{Departments: departments})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(resp)
}

func Test_infrastructureFindCompanyDepartment(t *testing.T) {
	resp, err := companyServiceClient.FindCompanyDepartments(context.Background(), &pb.FindCompanyDepartmentReq{CompanyID: 5})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(resp)
}

func Test_infrastructureFindDepartmentUser(t *testing.T) {
	resp, err := userServiceClient.FindDepartmentUser(context.Background(), &pb.FindDepartmentUserReq{
		CompanyID:    5,
		DepartmentID: 1,
		Page:         1,
		PageSize:     10,
	})

	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(resp)
}
func Test_InsertDB(t *testing.T) {

}
