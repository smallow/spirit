package websocket

const (
	ActRequestClientId   = iota //请求用户ID
	ActResponseClientId         //回应用户ID
	ActRequestHeartBeat         //请求心跳
	ActResponseHeartBeat        //回应心跳
	ActMessage                  //消息
	RefreshClients              //刷新客户端列表
	ActPointToPoint             //点对点通信
)
