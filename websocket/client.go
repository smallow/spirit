package websocket

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"github.com/satori/go.uuid"
	"log"
	"net/url"
	"sync"
	"time"
)

const (
	clientMsgChanCapacity = 500
)

/*客户端连接状态控制标志位*/
var (
	status    = make(chan bool, 1)
	netStatus = false
)

type ClientView struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type Client struct {
	ip          string
	port        string
	wsHandleUrl string
	id          string
	name        string
	conn        *websocket.Conn
	closeChan   chan byte
	isClosed    bool
	lock        sync.Mutex
	timestamp   int64
	//dataHandler func(*PacketData)
	dataHandler func(*Message)
}

func NewClient(ip, port, wsHandleUrl string, clientName string, datahandler func(p *Message)) *Client {
	c := &Client{name: clientName}
	var (
		wsConn *websocket.Conn
		err    error
	)
	for {
		u := url.URL{Scheme: "ws", Host: ip + ":" + port, Path: wsHandleUrl}
		log.Printf("connecting websocket server 【%s】", u.String())
		if wsConn, _, err = websocket.DefaultDialer.Dial(u.String(), nil); err != nil {
			log.Println("connecting websocket err:", err)
		} else {
			uid := uuid.NewV4()
			clientId := uid.String()
			c.id = clientId
			c.conn = wsConn
			c.closeChan = make(chan byte, 1)
			c.timestamp = time.Now().Unix()
			c.ip = ip
			c.port = port
			c.wsHandleUrl = wsHandleUrl
			c.dataHandler = datahandler
			log.Printf("websocket clientID:[%s] connected  successful......", clientId)
			//msgDump := make(chan *PacketData, clientMsgChanCapacity)
			msgDump := make(chan *Message, clientMsgChanCapacity)
			go c.read(msgDump)
			go c.handler(msgDump, datahandler)
			//go c.heartCheck()
			break
		}
		time.Sleep(5 * time.Second)
	}
	return c
}
func (c *Client) Write(act int, data []byte) error {
	//err := c.conn.WriteMessage(websocket.BinaryMessage, EnPacket(act, data))
	log.Println("客户端写入:", string(data))
	err := c.conn.WriteMessage(websocket.TextMessage, data)
	return err
}
func (c *Client) read(ch chan<- *Message) {
	var (
		data            []byte
		err             error
		maxRetryReadNum int
	)

	for {
		select {
		case <-c.closeChan:
			log.Printf("客户端连接:[%s]【read线程】退出......", c.id)
			return
		default:
			if _, data, err = c.conn.ReadMessage(); err != nil {
				log.Println("clientID:", c.id, "read message error:", err)
				if maxRetryReadNum > 2 {
					goto ERR
				}
				maxRetryReadNum++
				time.Sleep(500 * time.Millisecond)
				continue
			} else {
				//b := bytes.Equal(defaultHead, data[:4]);
				message := &Message{}
				if err = json.Unmarshal(data, message); err == nil {
					//dataLen := byte2int(data[4:8]) //前4个字节是固定协议头 接着4个字节是数据(act+实际的数据)长度
					//act, ret := Parse(data[8 : 8+dataLen])
					act := message.Act
					switch act {
					case ActRequestClientId:
						log.Println("收到服务端的actRequestClientId要求,返回clientId:", c.id)
						responseMsg := &Message{Act: ActResponseClientId, Data: map[string]interface{}{
							"clientID":   c.id,
							"clientName": c.name,
						}}
						bs, _ := json.Marshal(responseMsg)
						c.conn.WriteMessage(websocket.TextMessage, bs)
					case ActRequestHeartBeat:
						//c.conn.WriteMessage(websocket.BinaryMessage, EnPacket(ActResponseHeartBeat, ret))
						//c.timestamp = int64(byte2int(ret))
					default:
						//ch <- NewPacketData(act, ret)
						ch <- message
					}
				}
			}
		}
	}
ERR:
	c.Close()
	log.Printf("客户端连接:[%s]【read线程】退出......", c.id)
}
func (c *Client) Close() {
	if c.conn == nil {
		return
	}
	//线程安全,可以重入的close(可多次执行)
	c.conn.Close()
	//通道的close只能执行一次 需要枷锁
	c.lock.Lock()
	if !c.isClosed {
		close(c.closeChan)
		c.isClosed = true
		log.Printf("客户端连接:[%s] 关闭成功,5秒重启连接......", c.id)
	}
	c.lock.Unlock()
	go c.reconnect()
}

func (c *Client) reconnect() {
	time.Sleep(5 * time.Second)
	var (
		wsConn *websocket.Conn
		err    error
	)
	u := url.URL{Scheme: "ws", Host: c.ip + ":" + c.port, Path: c.wsHandleUrl}
	log.Printf("reconnecting websocket server 【%s】", u.String())
	if wsConn, _, err = websocket.DefaultDialer.Dial(u.String(), nil); err != nil {
		log.Println("reconnecting websocket err:", err)
	} else {
		c.conn.Close()
		c.conn = wsConn
		uid := uuid.NewV4()
		clientId := uid.String()
		c.id = clientId
		c.closeChan = make(chan byte, 1)
		c.timestamp = time.Now().Unix()
		c.isClosed = false
		log.Printf("websocket clientID:[%s] reconnected  successful......", clientId)
		//msgDump := make(chan *PacketData, clientMsgChanCapacity)
		msgDump := make(chan *Message, clientMsgChanCapacity)
		go c.read(msgDump)
		go c.handler(msgDump, c.dataHandler)
		go c.heartCheck()
		return
	}
	time.AfterFunc(2*time.Second, c.reconnect)

}
func (c *Client) handler(ch <-chan *Message, fn func(p *Message)) {
	for {
		select {
		case p := <-ch:
			go fn(p)
		case <-c.closeChan:
			log.Printf("客户端连接:[%s]【hander线程】退出", c.id)
			return
		}
	}
}

func (c *Client) heartCheck() {
	ticker := time.NewTicker(5 * time.Second)
	for {
		select {
		case <-c.closeChan:
			log.Printf("客户端连接:[%s]【heartCheck线程】退出", c.id)
			return
		case <-ticker.C:
			now := time.Now().Unix()
			cnow := c.timestamp
			//log.Println(now, cnow)
			if cnow+15 < now {
				log.Printf("客户端连接:[%s]【heartCheck线程】退出", c.id)
				c.Close()
				time.Sleep(500 * time.Millisecond)
				return
			}
		}
	}
}
