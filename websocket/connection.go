package websocket

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"sync"
	"time"
)

type Connection struct {
	wsConn     *websocket.Conn
	inChan     chan []byte
	outChan    chan []byte
	closeChan  chan byte
	isClosed   bool
	lock       sync.Mutex
	isServer   bool
	header     http.Header
	clientID   string
	clientName string
	timestamp  int64
}

func InitConnection(wsConn *websocket.Conn, isServer bool, header http.Header) (conn *Connection, err error) {
	if wsConn == nil {
		return
	}
	conn = &Connection{
		wsConn:    wsConn,
		inChan:    make(chan []byte, 1000),
		outChan:   make(chan []byte, 1000),
		closeChan: make(chan byte, 1),
		isServer:  isServer,
		header:    header,
	}
	go conn.readLoop()
	go conn.writeLoop()
	return
}

func (conn *Connection) ReadMessage() (data []byte, err error) {
	select {
	case data = <-conn.inChan:
	case <-conn.closeChan:
		err = errors.New(fmt.Sprintf("read connection is closed"))
	}
	return
}
func (conn *Connection) WriteMessage(data []byte) (err error) {
	select {
	case conn.outChan <- data:
	case <-conn.closeChan:
		err = errors.New(fmt.Sprintf("write connection is closed"))
		log.Println(err)
	}
	return
}
func (conn *Connection) Id() string {
	return conn.clientID
}
func (conn *Connection) Name() string {
	return conn.clientName
}
func (conn *Connection) Close() {
	if conn == nil || conn.wsConn == nil {
		return
	}
	//线程安全,可以重入的close(可多次执行)
	conn.wsConn.Close()
	//通道的close只能执行一次 需要枷锁
	conn.lock.Lock()
	if !conn.isClosed {
		close(conn.closeChan)
		conn.isClosed = true
		if conn.isServer {
			log.Printf("clientID:[%s] connection【关闭】成功!", conn.clientID)
			wsMap.Delete(conn.clientID)
		}
	}
	conn.lock.Unlock()
}
func (conn *Connection) IsClosed() bool {
	return conn.isClosed
}

func (conn *Connection) CloseChan() chan byte {
	return conn.closeChan
}
func (conn *Connection) readLoop() {
	var (
		data       []byte
		err        error
		clientID   string
		clientName string
	)
	for {
		if _, data, err = conn.wsConn.ReadMessage(); err != nil {
			log.Printf("wsConn read message error:[%v]", err)
			goto ERR
		}
		//b := bytes.Equal(defaultHead, data[:4]); b
		message := &Message{}
		err = json.Unmarshal(data, &message)
		if err == nil {
			/*dataLen := byte2int(data[4:8]) //前4个字节是固定协议头 接着4个字节是数据(act+实际的数据)长度
			act, ret := Parse(data[8 : 8+dataLen])*/
			act := message.Act
			switch act {
			case ActResponseClientId:
				//d := string(ret)
				//fmt.Println("收到客户端响应clientInfo:", d)
				//clientID = strings.Split(d, ",")[0]
				//clientName = strings.Split(d, ",")[1]
				if cv := message.Data; cv != nil {
					if clientViewMap, ok := cv.(map[string]interface{}); ok {
						clientID = clientViewMap["id"].(string)
						clientName = clientViewMap["name"].(string)
					}
				}
				log.Printf("收到客户端 actResponseClientID[%s],clientName:[%s] join进wsMap", clientID, clientName)
				conn.clientID = clientID
				conn.clientName = clientName
				conn.timestamp = time.Now().Unix()
				join(clientID, conn)
			case ActResponseHeartBeat:
			//processHeartbeat(clientID, ret, conn)
			case RefreshClients:
				clients := make([]*ClientView, 0)
				for _, val := range wsMap.Map {
					_val := val.(*Connection)
					clients = append(clients, &ClientView{ID: _val.clientID, Name: _val.clientName})
				}
				message := Message{Act: RefreshClients, Data: clients}
				bs, _ := json.Marshal(message)
				err = conn.wsConn.WriteMessage(websocket.TextMessage, bs)
				if err != nil {
					log.Println("refresh clients err:", err)
				}
			case ActPointToPoint:
				if clientID != "" {
					if pointInfo := message.Data; pointInfo != nil {
						if ptpInfo, ok := pointInfo.(PointToPointInfo); ok {
							to := ptpInfo.TO
							log.Printf("receive 点对点消息通信,from:[%s] to [%s]......", clientID, to)
							if toConn := wsMap.Get(to); toConn != nil {
								content := ptpInfo.Content
								if _toConn, ok := toConn.(*Connection); ok {
									message := Message{Act: ActMessage, Data: content}
									bs, _ := json.Marshal(message)
									err = _toConn.WriteMessage(bs)
									if err != nil {
										log.Printf("PointToPoint write message error {%v},from:[%s],to:[%s],content:[%v]", err, clientID, to, content)
									}
								}
							}
						}
					}
				} else {
					log.Printf("点对点消息通信 clientID为空 连接异常!")
				}
			default:
				select {
				case conn.inChan <- data:
				case <-conn.closeChan:
					//closeChan关闭的时候回进入到这个case
					goto ERR
				}
			}
		} else {
			log.Printf("wsConn read message [%s], json unmarshal format error:[%v]", string(data), err)
		}

	}
ERR:
	conn.Close()
}
func (conn *Connection) writeLoop() {
	var (
		data []byte
		err  error
	)
	for {
		select {
		case data = <-conn.outChan:
		case <-conn.closeChan:
			goto ERR
		}
		//log.Println("写回客户端信息:",string(data[4:]))
		if err = conn.wsConn.WriteMessage(websocket.TextMessage, data); err != nil {
			log.Printf("wsConn write message error:[%v]", err)
			goto ERR
		}
	}

ERR:
	conn.Close()
}

/**
被动超时断开连接
*/
func processHeartbeat(clientId string, data []byte, conn *Connection) {
	//log.Printf("收到客户端[%s]响应心跳:[%v]", clientID, byte2int(data))
	now := time.Now().Unix()
	cnow := int64(byte2int(data))
	if cnow+60 < now { //超时15秒开始清理
		wsMap.Delete(clientId)
		conn.Close()
		log.Println("clientID:", clientId, "超时，服务器被动断开连接")
	} else {
		conn.timestamp = now
	}
}
