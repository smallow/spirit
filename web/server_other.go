// +build !windows

package web

import (
	"github.com/fvbock/endless"
	"github.com/gin-gonic/gin"
	"time"
)

func initServer(address string, readTimeOut, writeTimeOut int, router *gin.Engine) server {
	s := endless.NewServer(address, router)
	s.ReadHeaderTimeout = time.Duration(readTimeOut) * time.Second
	s.WriteTimeout = time.Duration(writeTimeOut) * time.Second
	s.MaxHeaderBytes = 1 << 20
	return s
}
