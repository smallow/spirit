package web

import (
	"bytes"
	"fmt"
	"github.com/fsnotify/fsnotify"
	"io/ioutil"
	"log"
	"os"
	"strings"
)
import "github.com/spf13/viper"

type SpiritConfig struct {
	Web struct {
		ServerPort string `json:"server_port" mapstructure:"server_port"`
	} `mapstructure:"web"`

	Mysql struct {
		DriverName     string `json:"driver_name" mapstructure:"driver_name"`
		DataSourceName string `json:"data_source_name" mapstructure:"data_source_name"`
		MaxOpenConn    int    `json:"max_open_conn" mapstructure:"max_open_conn"`
		MaxIdleConn    int    `json:"max_idle_conn" mapstructure:"max_idle_conn"`
	} `mapstructure:"mysql"`
}

func Viper(configPath string, config interface{}) (*viper.Viper, error) {
	v := viper.New()
	v.SetConfigFile(configPath)
	err := v.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	v.WatchConfig()
	v.OnConfigChange(func(e fsnotify.Event) {
		log.Printf("config file [%s] changed:", e.Name)
		if err := v.Unmarshal(&config); err != nil {
			log.Printf("config file [%s] changed error:[%v]", e.Name, err)
		}
	})
	if err := v.Unmarshal(&config); err != nil {
		log.Printf("config file Unmarshal error:[%v]", err)
		return nil, err
	}
	return v, nil
}

func LoadAllTomlConf() (map[string]*viper.Viper, error) {
	viperConfMap := make(map[string]*viper.Viper, 0)
	f, err := os.Open("./conf")
	if err != nil {
		return nil, err
	}
	fileList, err := f.Readdir(1024)
	if err != nil {
		return nil, err
	}
	for _, f0 := range fileList {
		if !f0.IsDir() {
			bts, err := ioutil.ReadFile("./conf/" + f0.Name())
			if err != nil {
				return nil, err
			}
			v := viper.New()
			v.SetConfigType("toml")
			v.ReadConfig(bytes.NewBuffer(bts))
			pathArr := strings.Split(f0.Name(), ".")
			viperConfMap[pathArr[0]] = v
		}
	}
	return viperConfMap, nil
}
