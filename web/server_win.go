// +build windows

package web

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func initServer(address string, readTimeOut, writeTimeOut int, router *gin.Engine) server {
	return &http.Server{
		Addr:           address,
		Handler:        router,
		ReadTimeout:    time.Duration(readTimeOut) * time.Second,
		WriteTimeout:   time.Duration(writeTimeOut) * time.Second,
		MaxHeaderBytes: 1 << 40,
	}
}
