package web

import (
	"github.com/gin-gonic/gin"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type server interface {
	ListenAndServe() error
}

func StartServer(address string, readTimeout, writeTimeout int, r *gin.Engine) {
	if r != nil {
		//通过公共方法获取配置文件属性 不用定义结构体的方法
		s := initServer(address, readTimeout, writeTimeout, r)
		if err := s.ListenAndServe(); err != nil {
			log.Panicf("【spirit web module】 服务启动失败 %v", err)
		}
	}
	log.Panicln("error:gin router is null!")
}

func InitViews(router *gin.Engine, staticPrefix, staticRoot, templateDir string) {
	router.Static(staticPrefix, staticRoot)
	//router.StaticFile("/favicon", config.SysConfig.App.Favicon)
	if templateDir != "" {
		templateFiles := loadAllTemplates(templateDir)
		if len(templateFiles) > 0 {
			router.LoadHTMLFiles(templateFiles...)
		}
		router.Delims("{{", "}}")
	}
}

func loadAllTemplates(dir string) []string {
	templateFiles := make([]string, 0)
	filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if strings.HasSuffix(info.Name(), ".html") {
			//log.Println("加载模板文件", path)
			if _, err := os.Stat(path); err != nil {
				log.Println(path, err.Error())
			}
			templateFiles = append(templateFiles, path)
		}
		return nil
	})
	return templateFiles
}
