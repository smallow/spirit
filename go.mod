module gitee.com/smallow/spirit

go 1.13

require (
	github.com/akavel/rsrc v0.10.2 // indirect
	//github.com/Pallinder/go-randomdata v1.2.0
	github.com/dchest/captcha v0.0.0-20200903113550-03f5f0333e1f
	github.com/fsnotify/fsnotify v1.4.7
	github.com/fvbock/endless v0.0.0-20170109170031-447134032cb6
	github.com/garyburd/redigo v1.6.2
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/klauspost/compress v1.10.7 // indirect
	github.com/lxn/walk v0.0.0-20210112085537-c389da54e794
	github.com/lxn/win v0.0.0-20210218163916-a377121e959e // indirect
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/olivere/elastic/v7 v7.0.22
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/viper v1.7.1
	go.mongodb.org/mongo-driver v1.5.0
	go.uber.org/zap v1.16.0
	golang.org/x/net v0.0.0-20220513224357-95641704303c // indirect
	golang.org/x/sys v0.0.0-20220513210249-45d2b4557a2a // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.3
)
