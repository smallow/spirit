package elastic

import (
	"encoding/json"
	"fmt"
	"log"
	"runtime"
	"strings"
)

//obj(string,M)转M,查询用到
func objToMap(obj interface{}) *map[string]interface{} {
	data := make(map[string]interface{})
	if s, ok := obj.(string); ok {
		json.Unmarshal([]byte(strings.Replace(s, "'", "\"", -1)), &data)
	} else if s1, ok1 := obj.(map[string]interface{}); ok1 {
		data = s1
	} else if s1, ok1 := obj.(*map[string]interface{}); ok1 {
		return s1
	} else {
		data = nil
	}
	return &data
}

//对象数组转成string数组
func objArrToStringArr(old []interface{}) []string {
	if old != nil {
		new := make([]string, len(old))
		for i, v := range old {
			new[i], _ = v.(string)
		}
		return new
	} else {
		return nil
	}
}

//出错拦截
func catch() {
	if r := recover(); r != nil {
		log.Println(r)
		for skip := 0; ; skip++ {
			_, file, line, ok := runtime.Caller(skip)
			if !ok {
				break
			}
			go log.Printf("%v,%v\n", file, line)
		}
	}
}

func bsonIdToSId(_id interface{}) string {
	v := fmt.Sprintf("%v", _id)
	v = strings.TrimLeft(v, `ObjectID("`)
	v = strings.TrimRight(v, `")`)
	return v
}
