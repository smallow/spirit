package gorm

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

/**
这个gromConfig支持自定义config 也可以用默认gorm的config,主要就是日志这一块
*/
func NewGorm(dsn, driverName string, maxOpenConn, maxIdleConn int, gormConfig *gorm.Config) *gorm.DB {
	if gormConfig == nil {
		gormConfig = getGormConfig()
	}
	if db, err := gorm.Open(mysql.New(mysql.Config{DSN: dsn, DriverName: driverName}), gormConfig); err == nil {
		sqlDB, _ := db.DB()
		sqlDB.SetMaxIdleConns(maxIdleConn)
		sqlDB.SetMaxOpenConns(maxOpenConn)
		return db
	} else {
		log.Printf("GormMysql open Error,DSN:[%s],err:[%v]", dsn, err)
	}
	return nil
}

func getGormConfig() *gorm.Config {
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer（日志输出的目标，前缀和日志包含的内容——译者注）
		logger.Config{
			SlowThreshold: time.Second,   // 慢 SQL 阈值
			LogLevel:      logger.Error, // 日志级别
			Colorful:      false,         // 禁用彩色打印
		},
	)
	return &gorm.Config{
		Logger:                                   newLogger,
		DisableForeignKeyConstraintWhenMigrating: true,
	}
}
