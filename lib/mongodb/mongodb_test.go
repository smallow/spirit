package mongodb

import (
	"log"
	"testing"

	"go.mongodb.org/mongo-driver/bson"
)

func Test_add(t *testing.T) {
	m := &MongodbSim{
		MongodbAddr: "192.168.3.128:27090",
		Size:        5,
		DbName:      "wcj",
		UserName:    "admin",
		Password:    "123456",
	}
	m.InitPool()
	// log.Println(m.Save("test", map[string]interface{}{
	// 	"name": "张三",
	// 	"age":  12,
	// }))
	// log.Println(m.SaveByOriID("test", map[string]interface{}{
	// 	"name": "张三",
	// 	"age":  25,
	// }))
	log.Println(m.SaveBulkInterface("test", []interface{}{
		map[string]interface{}{
			"name": "张三1",
			"age":  1,
		},
		map[string]interface{}{
			"name": "张三2",
			"age":  2,
		},
	}...))
}
func Test_find(t *testing.T) {
	m := &MongodbSim{
		MongodbAddr: "192.168.3.128:27080",
		Size:        5,
		DbName:      "qfw",
	}
	m.InitPool()
	list, _ := m.Find("test", map[string]interface{}{
		"name": "张三",
	}, map[string]interface{}{"age": -1}, map[string]interface{}{"age": 1, "name": 1, "_id": 0}, false, -1, -1)
	log.Println(len(*list))
}
func Test_update(t *testing.T) {
	m := &MongodbSim{
		MongodbAddr: "192.168.3.128:27080",
		Size:        5,
		DbName:      "qfw",
		//UserName:    "admin",
		//Password:    "123456",
	}
	m.InitPool()
	log.Println(m.Update("user", map[string]interface{}{
		"s_phone": "15037870765",
	}, map[string]interface{}{
		"$unset": map[string]interface{}{
			"s_name": "",
		},
	}, false, false))
	return
	s := [][]map[string]interface{}{
		[]map[string]interface{}{
			map[string]interface{}{"name": "李四111"},
			map[string]interface{}{"$set": map[string]interface{}{"type": 1}},
		},
		[]map[string]interface{}{
			map[string]interface{}{"name": "张三111"},
			map[string]interface{}{"$set": map[string]interface{}{"type": 2}},
		},
	}
	one := m.UpdateBulk("test", s...)
	log.Println(one)
}
func Test_count(t *testing.T) {
	m := &MongodbSim{
		MongodbAddr: "192.168.3.128:27090",
		Size:        5,
		DbName:      "wcj",
		UserName:    "admin",
		Password:    "123456",
	}
	m.InitPool()
	one := m.Count("test", bson.M{
		"name": "张三",
	})
	log.Println(one)
}
func Test_del(t *testing.T) {
	m := &MongodbSim{
		MongodbAddr: "192.168.3.128:27090",
		Size:        5,
		DbName:      "wcj",
		UserName:    "admin",
		Password:    "123456",
	}
	m.InitPool()
	one := m.Del("test", nil)
	log.Println(one)
}
func Test_itor(t *testing.T) {
	m := &MongodbSim{
		MongodbAddr: "192.168.3.128:27080",
		Size:        5,
		DbName:      "wcj",
	}
	m.InitPool()
	sess := m.GetMgoConn()
	it := sess.DB("wcj").C("jyopenplatform_user").Find(map[string]interface{}{
		"i_status": 1,
		"i_freeze": 1,
	}).Select(map[string]interface{}{
		"a_power":          1,
		"s_m_openid":       1,
		"o_rulepro":        1,
		"s_appid":          1,
		"s_enterprisename": 1,
	}).Sort("_id").Iter()
	for user := make(map[string]interface{}); it.Next(&user); {
		log.Println(user["_id"], user["s_appid"])
	}
}
