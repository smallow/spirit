package msgbus

import (
	"crypto/rand"
	"encoding/hex"
	//	"encoding/hex"
	//"fmt"
	mr "math/rand"
)

const (
	KEY = "()*+-_=~`!@#$%^&*{}[]|',.<>?abcdefghhijklmnopqrstuvwxyz0123456789ABCDEFJHIJKLMNOPQRSTUVWXYZ"
)

//
func UUID(length int) string {
	tmp := make([]byte, length>>1)
	rand.Read(tmp)
	return hex.EncodeToString(tmp)
}

//
func OldUUID(length int) string {
	ret := ""
	for i := 0; i < length; i++ {
		pos := mr.Intn(90)
		ret += KEY[pos : pos+1]
	}
	return ret
}
