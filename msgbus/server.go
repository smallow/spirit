package msgbus

import (
	"fmt"
	"net"
)

//启动服务端
func StartServer(parseEvent func(*Packet),
	processconnection func(conn net.Conn),
	addr string) {
	netListen, err := net.Listen("tcp", addr)
	if err != nil {
		fmt.Println(err.Error())
	}
	defer netListen.Close()
	fmt.Println("等待连接，服务地址:", addr)
	//接受消息
	messageQueue := make(chan *Packet, 5000) //并发1000
	go processMsg(messageQueue, parseEvent)
	for {
		conn, err := netListen.Accept()
		if err != nil {
			continue
		} else {
			//告诉客户端ID，每次发消息都要携带自己ID
			go func() {
				processconnection(conn)
				forwardMessage(conn, messageQueue)
			}()
		}
	}
}

