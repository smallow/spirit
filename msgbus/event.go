package msgbus

//只有事件，发送类型
const (
	EVENT_RETURN_MACHINE_ID    = iota
	EVENT_REQUEST_HEARTBEAT    //心跳
	EVENT_RETURN_HEARTBEAT     //
	EVENT_PUBLISH_MYSERVICES   //发布我的服务
	EVENT_REQUEST_SPIDER_STATE //获取爬虫状态
	EVENT_RECIVE_SPIDER_STATE  //接受爬虫状态
	EVENT_RECIVE_CALLBACK      //调用返回，用于调用服务需要同步返回值
	EVENT_VIEWALL_SERVICE      //查看所有的客户端
	EVENT_REMOVE_CLIENT        //删除客户端
	EVENT_UPDATE_MYNAME        //更新客户端名称
	EVENT_SYSTEM_COMMAND       //系统控制指令，每个客户端默认都会实现
	EVENT_BYE                  //客户端主动断开
	EVENT_GET_ALLDOWNLOADER    //取所有的下载器
	//---------
	SERVICE_DOWNLOAD             = 7070 //下载服务
	SERVICE_GETPROXY             = 7071 //获取代理
	SERVICE_GETNAME              = 7072 //获取企业名录
	SERVICE_SPIDER_TAOBAO        = 7073 //淘宝网首页检索
	SERVICE_DISTINGUISH          = 7074 //人工识别验证码
	SERVICE_DOWNLOAD_DELETE_NODE = 7075 //删除节点
	SERVICE_DOWNLOAD_APPEND_NODE = 7076 //追加节点
	SERVICE_REPORT_SAVE          = 7077 //保存报表数据
	SERVICE_INVNAME_ANALYSIS     = 7078 //分析股东名录
	SERVICE_YCML_SAVE            = 7079 //异常名录保存入内存数据库
	SERVICE_YCML_NOTICE          = 7080 //异常名录下载完成通知
	SERVICE_ECPS_INC             = 7081 //公示增量数据保存
	SERVICE_OFFICE_ANALYSIS      = 7082 //word pdf excel文件解析服务 由java提供的一个服务

	SERVICE_LOG_CHECK = 7200 //检测日志记录

	//-------发送方式----------------
	SENDTO_TYPE_RAND_RECIVER = 0 //发送给任一服务接收者，默认是这种模式
	SENDTO_TYPE_ALL          = 1 //发送给所有客户端
	SENDTO_TYPE_ALL_RECIVER  = 2 //发送给所有指定服务接收者
	SENDTO_TYPE_P2P          = 3 //发送给指定客户端

	SENDTO_TYPE_P2P_BYNAME = 4 //发送给指定客户，通过指定客户的名称
)
