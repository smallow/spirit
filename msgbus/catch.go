package msgbus

import (
	"log"
	"runtime"

	//"github.com/donnie4w/go-logger/logger"
)

//出错拦截
func Catch() {
	if r := recover(); r != nil {
		log.Println("err:", r)
		for skip := 0; ; skip++ {
			_, file, line, ok := runtime.Caller(skip)
			if !ok {
				break
			}
			go log.Printf("%v,%v\n", file, line)
		}
	}
}

//出错拦截
/*func CatchLogger() {
	if r := recover(); r != nil {
		logger.Error(r)
		for skip := 0; ; skip++ {
			_, file, line, ok := runtime.Caller(skip)
			if !ok {
				break
			}
			go logger.Error(file, ", ", line)
		}
	}
}*/
