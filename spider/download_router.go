package spider

import (
	"encoding/json"
	"gitee.com/smallow/spirit/socket/siface"
	"gitee.com/smallow/spirit/socket/snet"
	"log"
	"net/http"
)

type DownloadRouter struct {
	snet.BaseRouter
	Name string
}
type HttpDownloadRequest struct {
	Url      string                 `json:"url"`
	Param    map[string]interface{} `json:"param"`
	Header   map[string]interface{} `json:"header"`
	Cookies  []*http.Cookie         `json:"cookies"`
	Method   string                 `json:"method"`
	Encoding string                 `json:"encoding"`
	IsHttps  bool                   `json:"is_https"`
	UseProxy bool                   `json:"use_proxy"`
}

type HttpDownloadResponse struct {
	Data    []byte         `json:"data"`
	Cookies []*http.Cookie `json:"cookies"`
}

func (pr *DownloadRouter) PreHandle(request siface.IRequest) {
	//request.GetConnection().GetTCPConnection().Write([]byte("before ping...\n"))
}

func (pr *DownloadRouter) Handle(request siface.IRequest) {
	log.Printf("DownloadRouter Handle method  received client msgID:[%d] data:[%s]\n", request.GetMessageID(), string(request.GetData()))
	msg := HttpDownloadRequest{}
	err := json.Unmarshal(request.GetData(), &msg)
	if err != nil {
		log.Println("下载消息格式异常")
		return
	}
	respBytes, respCookies, err := Download2(msg.Url, msg.Method, msg.Param, msg.Header, msg.Cookies, "utf8", false, false)
	if err != nil {
		log.Println("下载异常")
		return
	}
	response := &HttpDownloadResponse{
		Data:    respBytes,
		Cookies: respCookies,
	}
	bs, _ := json.Marshal(response)
	request.GetConnection().SendMsg(request.GetMessageID(), bs)
}

func (pr *DownloadRouter) PostHandle(request siface.IRequest) {
	//request.GetConnection().GetTCPConnection().Write([]byte(" after ping...\n"))
}
