package spider

import (
	"errors"
	"gitee.com/smallow/spirit/utils"
	"log"
	"net/http"
	"time"
)

var (
	timeout    = 600000
	sleep      = 200
	retryTimes = 3
	retrySleep = 500
)

func Download(downloadRequest *HttpDownloadRequest) ([]byte, []*http.Cookie, error) {
	if downloadRequest.UseProxy {
		log.Println("使用PPPOE切换IP暂未上线")
		return nil, nil, errors.New("暂不支持proxy")
	}
	return Download2(downloadRequest.Url, downloadRequest.Method, downloadRequest.Param, downloadRequest.Header, downloadRequest.Cookies, downloadRequest.Encoding, downloadRequest.UseProxy, downloadRequest.IsHttps)
}

func Download2(url, method string, requestParam, head map[string]interface{}, cookie []*http.Cookie, encoding string, useProxy, isHttps bool) ([]byte, []*http.Cookie, error) {
	defer utils.Catch()
	hc := NewHttpClient(timeout, sleep, retryTimes, retrySleep, isHttps)
	if useProxy {
		log.Println("使用PPPOE切换IP暂未上线")
	}
	requestConfig := &RequestConfig{
		UrlStr:   url,
		Method:   method,
		Param:    requestParam,
		Cookie:   cookie,
		Head:     head,
		Encoding: encoding,
	}
	resp, respCookies, err, _ := hc.Download(requestConfig)
	//log.Println("doHttp respState:", respState)
	//可以在这里统计下载量 暂时省略....
	return resp, respCookies, err
}

func ParseHttpCookie(obj interface{}) (ret []*http.Cookie) {
	if arr, ok := obj.([]interface{}); ok {
		for _, v := range arr {
			item := v.(map[string]interface{})
			maxAge := 0
			if item["MaxAge"] != nil {
				maxAge = int(item["MaxAge"].(float64))
			}
			secure := false
			if item["Secure"] != nil {
				secure = item["Secure"].(bool)
			}
			httpOnly := false
			if item["HttpOnly"] != nil {
				httpOnly = item["HttpOnly"].(bool)
			}
			exp := item["Expires"].(string)
			expt, _ := time.Parse("2006-01-02T15:04:05Z", exp)
			ret = append(ret, &http.Cookie{
				Value:   item["Value"].(string),
				Name:    item["Name"].(string),
				Domain:  item["Domain"].(string),
				Path:    item["Path"].(string),
				Expires: expt,

				MaxAge:   maxAge,
				Secure:   secure,
				HttpOnly: httpOnly,
			})
		}
	}
	return
}
