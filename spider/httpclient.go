package spider

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"gitee.com/smallow/spirit/utils"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"
	"time"
)

type (
	HttpClient struct {
		Client     *http.Client
		req        *http.Request
		jar        *cookiejar.Jar
		resp       *http.Response
		Timeout    int  //连接超时
		Sleep      int  //下载完成后等待
		RetryTimes int  //下载失败后，可以再尝试次数
		RetrySleep int  //下载失败后，尝试间隔时间
		IsHttps    bool //是否走安全协议
	}
	RequestConfig struct {
		UrlStr   string                 //
		Method   string                 //
		Param    map[string]interface{} //
		Cookie   []*http.Cookie         //
		Head     map[string]interface{} //
		Check    func([]byte) error     //
		Encoding string
	}
)

func NewHttpClient(_timeout, _sleep, _retryTimes, _retrySleep int, isHttps bool) *HttpClient {
	defer utils.Catch()
	hc := HttpClient{
		Timeout:    _timeout,
		Sleep:      _sleep,
		RetrySleep: _retrySleep,
		RetryTimes: _retryTimes,
	}
	httpTransport := &http.Transport{
		DisableKeepAlives: true,
	}
	if isHttps {
		httpTransport.TLSClientConfig = &tls.Config{RootCAs: nil, InsecureSkipVerify: true}
		httpTransport.DisableCompression = true
	}
	hc.jar, _ = cookiejar.New(nil)
	hc.Client = &http.Client{Jar: hc.jar, Transport: httpTransport, Timeout: time.Duration(_timeout) * time.Millisecond}
	return &hc
}

func (hc *HttpClient) Download(rc *RequestConfig) (resp []byte, respCookies []*http.Cookie, err error, respState int) {
	defer utils.Catch()

	method := strings.ToLower(rc.Method)
	purl, er := url.Parse(rc.UrlStr)
	if er != nil {
		log.Println(er)
	}
	if rc.Cookie != nil {
		//请求加上cookie
		hc.jar.SetCookies(purl, rc.Cookie)
	}
	if rc.Param != nil { //有参数
		values := make([]string, len(rc.Param))
		i := 0
		for k, v := range rc.Param {
			values[i] = k + "=" + fmt.Sprint(v)
			i++
		}
		queryStr := strings.Join(values, "&")
		if method == "post" {
			values := url.Values{}
			for k, v := range rc.Param {
				values[k] = []string{fmt.Sprint(v)}
			}
			if strings.Contains(fmt.Sprint(rc.Head["Content-Type"]), "application/x-www-form-urlencoded") {
				hc.req, err = http.NewRequest("POST", rc.UrlStr, strings.NewReader(values.Encode()))
			} else {
				hc.req, err = http.NewRequest("POST", rc.UrlStr, bytes.NewReader([]byte(queryStr)))
			}
		} else if method == "json" {
			pbs, _ := json.Marshal(rc.Param)
			hc.req, err = http.NewRequest("POST", rc.UrlStr, bytes.NewReader([]byte(pbs)))
			hc.req.Header.Set("Content-Type", "application/json")
		} else if method == "text" {
			hc.req, err = http.NewRequest("POST", rc.UrlStr, bytes.NewReader([]byte(fmt.Sprint(rc.Param["text"]))))
		} else {
			if strings.Contains(rc.UrlStr, "?") {
				if queryStr == "" {
					hc.req, err = http.NewRequest("GET", rc.UrlStr, nil)
				} else {
					hc.req, err = http.NewRequest("GET", rc.UrlStr+"&"+queryStr, nil)
				}
			} else {
				if queryStr == "" {
					hc.req, err = http.NewRequest("GET", rc.UrlStr, nil)
				} else {
					hc.req, err = http.NewRequest("GET", rc.UrlStr+"?"+queryStr, nil)
				}
			}
		}
	} else { //无参数，全是get请求
		hc.req, err = http.NewRequest("GET", rc.UrlStr, nil)
	}
	if err != nil {
		log.Println(err)
	}
	//加请求头参数
	if rc.Head != nil {
		for k, v := range rc.Head {
			//log.Println(k, fmt.Sprint(v))
			hc.req.Header.Add(k, fmt.Sprint(v))
		}
	}
	for i := 0; i < retryTimes; i++ {
		hc.resp, err = hc.Client.Do(hc.req)
		if err != nil {
			log.Println(err)
			time.Sleep(time.Duration(retrySleep) * time.Millisecond)
			continue
		}
		if hc.resp != nil && hc.resp.Body != nil {
			defer hc.resp.Body.Close()
			respCookies = hc.resp.Cookies()
		} else {
			continue
		}
		resp = getRespBytes(hc.resp)
		respCookies = hc.resp.Cookies()
		//对于检测失败的情况，可以单独定延时
		if rc.Check != nil {
			checkError := rc.Check(resp)
			if checkError != nil {
				time.Sleep(time.Duration(sleep) * time.Millisecond)
				continue
			}
		}
		break
	}
	if sleep > 0 {
		time.Sleep(time.Duration(sleep) * time.Millisecond)
	}
	if hc.resp == nil {
		respState = 1000
	} else {
		respState = hc.resp.StatusCode
	}
	return
}

func getRespBytes(resp *http.Response) []byte {
	defer utils.Catch()
	buffer := bytes.NewBuffer(make([]byte, 0, 65536))
	_, err := io.Copy(buffer, resp.Body)
	if err != nil {
		return []byte{}
	}
	temp := buffer.Bytes()
	length := len(temp)
	var body []byte
	//are we wasting more than 10% space?
	if cap(temp) > (length + length/10) {
		body = make([]byte, length)
		copy(body, temp)
	} else {
		body = temp
	}
	return body
}
