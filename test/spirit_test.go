package test

import (
	"fmt"
	sgorm "gitee.com/smallow/spirit/lib/gorm"
	"gitee.com/smallow/spirit/utils"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"math/rand"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

type mysqlConfig struct {
	DriverName     string `json:"driver_name" mapstructure:"driver_name"`
	DataSourceName string `json:"data_source_name" mapstructure:"data_source_name"`
	MaxOpenConn    int    `json:"max_open_conn" mapstructure:"max_open_conn"`
	MaxIdleConn    int    `json:"max_idle_conn" mapstructure:"max_idle_conn"`
}
type Customer struct {
	CustomerID int64  `json:"customer_id"`
	RealName   string `json:"real_name"`
	Sex        string `json:"sex"`
	Age        int    `json:"age"`
	Email      string `json:"email"`
	Phone      string `json:"phone"`
	Address    string `json:"address"`
}

type Customer1 struct {
	CustomerID int64  `json:"customer_id"`
	RealName   string `json:"real_name"`
	Sex        string `json:"sex"`
	Age        int    `json:"age"`
	Email      string `json:"email"`
	Phone      string `json:"phone"`
	Address    string `json:"address"`
}

func (s *Customer) TableName() string {
	return "customer2"
}

func (s *Customer1) TableName() string {
	return "customer"
}

var (
	dataSourceName = "root:Zxy@19860122@tcp(localhost:3306)/springboot_demo?charset=utf8mb4&parseTime=true&loc=Local"
	mysqlDB        *gorm.DB
)
var (
	firstName = []string{"赵", "钱", "孙", "李", "周", "武", "郑", "王", "荀", "杨", "刘"}
	midName   = []string{"国", "静", "天", "皓月", "佳", "良", "东", "西", "南", "北", "中", "林", "想", "六", "只", "擦", "的", "阿萨", "对", "九", "丈", "帐", "账"}
	lastName  = []string{"芮", "苋", "苻", "天", "藤", "酞", "谈", "东", "袒", "南", "唐", "倘", "滔", "掏", "逃", "熔", "裳", "少", "哨", "地", "为", "给", "官方", "放噶", "和", "怕"}
)

func init2() {
	mysqlDB = sgorm.NewGorm(dataSourceName, "mysql", 20, 10, &gorm.Config{
		Logger:                                   logger.Default.LogMode(logger.Error),
		DisableForeignKeyConstraintWhenMigrating: true,
	})
	log.Printf("【spirit】 mysqlDB init successful......!")
}

func Test_Mysql(t *testing.T) {
	var (
		data       = make([]*Customer, 0)
		wg         sync.WaitGroup
		totalCount = 0
		//mu         sync.RWMutex
		mu sync.Mutex
	)
	t1 := time.Now().Unix()
	for i := 1; i <= 1000000; i++ {
		sex := ""
		t := rand.Intn(100)
		if t%2 == 0 {
			sex = "男"
		} else {
			sex = "女"
		}
		data = append(data, &Customer{
			RealName: generatorName(),
			Age:      t,
			Sex:      sex,
			Phone:    utils.CreateRand(11),
			Email:    utils.MakeSimpleCaptcha(8) + "@qq.com",
			Address:  utils.MakeSimpleCaptcha(4) + "省" + utils.MakeSimpleCaptcha(2) + "市",
		})
		if i > 0 && i%8000 == 0 {
			wg.Add(1)
			go func(d []*Customer) {
				if err := mysqlDB.Create(d).Error; err != nil {
					fmt.Println("出错:", err)
				} else {
					mu.Lock()
					totalCount += 8000
					mu.Unlock()
					str := "完成[%d]条.."
					if totalCount%80000 == 0 {
						str = "完成[%d]条..\n"
					}
					fmt.Printf(str, totalCount)
				}
				wg.Done()
			}(data)
			data = make([]*Customer, 0)
		}
	}
	wg.Wait()
	t1 = time.Now().Unix() - t1
	fmt.Println("插入数据完成耗时:", t1, "秒")
}

/**
多协程插入大数据
*/
func Test_mysql2(t *testing.T) {
	var (
		totalCount = int64(0)
		wg         = &sync.WaitGroup{}
		batch      = int64(1000)
		num        = int64(20000)
	)
	ch := make(chan []*Customer)
	for i := 0; i < 3; i++ {
		go func() {
			for {
				select {
				case customers := <-ch:
					go func() {
						if err := mysqlDB.Create(customers).Error; err != nil {
							fmt.Println("出错:", err)
						} else {
							atomic.AddInt64(&totalCount, int64(len(customers)))
							str := "完成[%d]条.."
							if totalCount > 0 && totalCount%batch == 0 {
								str = "完成[%d]条..\n"
							}
							fmt.Printf(str, totalCount)
							wg.Done()
						}
					}()

				}
			}
		}()
	}
	t1 := time.Now().UnixMilli()
	customerMaker(num, ch, batch, wg)
	wg.Wait()
	t1 = time.Now().UnixMilli() - t1
	log.Printf("任务完成......,totalCount:[%d],耗时[%d]毫秒", totalCount, t1)
}

/**
mysql分区测试  根据customer_id分区11个,数据量10000000+ phone,real_name做了索引
*/
func Test_mysqlPartition(t *testing.T) {
	data := make([]*Customer, 0)
	//测试查询
	t1 := time.Now().UnixMilli()
	name := "钱皓月逃"
	mysqlDB.Find(&data, &Customer{RealName: name})
	t1 = time.Now().UnixMilli() - t1
	log.Printf("按real_name [%s]精确匹配查询,总数:[%d] 耗时:[%d]毫秒", name, len(data), t1)

	t1 = time.Now().UnixMilli()
	mysqlDB.Find(&data, &Customer{Phone: "37319331286"})
	t1 = time.Now().UnixMilli() - t1
	log.Printf("phone [%s]精确匹配查询,总数:[%d] 耗时:[%d]毫秒", "37319331286", len(data), t1)

	/*t1=time.Now().UnixMilli()
	mysqlDB.Find(&data,&Customer{Address:"0080省28市" })
	t1=time.Now().UnixMilli()-t1
	log.Printf("address [%s]精确匹配查询,总数:[%d] 耗时:[%d]毫秒","0080省28市",len(data),t1)*/

}

/**
mysql表不带分区测试 数据量20000000+ phone,real_name做了索引
*/
func Test_mysqlWithOutPartition(t *testing.T) {
	data := make([]*Customer1, 0)
	//测试查询
	t1 := time.Now().UnixMilli()
	name := "钱账"
	mysqlDB.Find(&data, &Customer1{RealName: name})
	t1 = time.Now().UnixMilli() - t1
	log.Printf("按real_name [%s]精确匹配查询,总数:[%d] 耗时:[%d]毫秒", name, len(data), t1)

	t1 = time.Now().UnixMilli()
	mysqlDB.Find(&data, &Customer1{Phone: "43688366555"})
	t1 = time.Now().UnixMilli() - t1
	log.Printf("phone [%s]精确匹配查询,总数:[%d] 耗时:[%d]毫秒", "43688366555", len(data), t1)

	/*t1=time.Now().UnixMilli()
	mysqlDB.Find(&data,&Customer1{Address:"7156省78市" })
	t1=time.Now().UnixMilli()-t1
	log.Printf("address [%s]精确匹配查询,总数:[%d] 耗时:[%d]毫秒","7156省78市",len(data),t1)*/
}

/**
通道 + 协程示例
*/
func Test_chanAndWaitGroup(t *testing.T) {
	t2 := time.Now().UnixMilli()
	a := 0
	for i := 0; i <= 100000000; i++ {
		a += i
	}
	log.Printf("同步计算结果:[%d],耗时:[%d]毫秒", a, time.Now().UnixMilli()-t2)

	var (
		totalCount = int64(0)
		wg         = &sync.WaitGroup{}
	)
	ch2 := make(chan []int, 100)
	for i := 0; i < 100; i++ {
		go func() {
			for {
				select {
				case ints := <-ch2:
					go func() {
						//fmt.Printf("开始计算[%d]-[%d]的和......", ints[0], ints[len(ints)-1])
						d := 0
						for i := 0; i < len(ints); i++ {
							d += ints[i]
						}
						//fmt.Printf("[%d]-[%d]的和计算完毕,结果[%d]\n", ints[0], ints[len(ints)-1], d)
						atomic.AddInt64(&totalCount, int64(d))
						wg.Done()
					}()
				}
			}
		}()
	}

	t1 := time.Now().UnixMilli()
	var data = make([]int, 0)
	for i := 1; i <= 1000000000; i++ {
		data = append(data, i)
		if i > 0 && i%80000 == 0 {
			wg.Add(1)
			ch2 <- data
			data = make([]int, 0)
		}
	}
	if len(data) > 0 {
		wg.Add(1)
		ch2 <- data
	}
	wg.Wait()
	t1 = time.Now().UnixMilli() - t1
	log.Printf("异步计算任务完成......,totalCount:[%d] 耗时:[%d]毫秒", totalCount, t1)
}

/**
有缓冲和无缓冲通道测试
*/
func Test_channelBuffer(t *testing.T) {
	/*ch1 := make(chan int)
	go func() {
		for i := 0; i < 3; i++ {
			fmt.Printf("子协程：i = %d\n", i)
			ch1 <- i   //子协程第一次写入无缓冲的通道,主协程这时候sleep阻塞了,所以没有打印下边的len 和cap ,等到主协程读完才行
			fmt.Printf("len(ch1)=%d,cap(ch1)=%d \n", len(ch1), cap(ch1))
		}
	}()
	time.Sleep(2 * time.Second)
	for i := 0; i < 3; i++ {
		num := <-ch1
		fmt.Println("num:", num)
	}*/
	//------------------------------------ 有缓冲通道------------------------
	fmt.Println("----------测试有缓冲的通道---------------")
	ch2 := make(chan int, 10)
	go func() {
		for i := 0; i < 3; i++ {
			fmt.Printf("子协程：i = %d\n", i)
			ch2 <- i
			fmt.Printf("len(ch2)=%d,cap(ch2)=%d \n", len(ch2), cap(ch2)) //有缓冲通道直接先把子协程执行完毕,全部写入,并且打印len和cap
		}
	}()
	time.Sleep(2 * time.Second)
	for i := 0; i < 3; i++ {
		num := <-ch2
		fmt.Println("num:", num)
	}
}

/**
生产者
*/
func customerMaker(num int64, ch chan []*Customer, batchSize int64, wg *sync.WaitGroup) {
	var data = make([]*Customer, 0)
	for i := int64(1); i <= num; i++ {
		sex := ""
		t := rand.Intn(100)
		if t%2 == 0 {
			sex = "男"
		} else {
			sex = "女"
		}
		data = append(data, &Customer{
			RealName: generatorName(),
			Age:      t,
			Sex:      sex,
			Phone:    utils.CreateRand(11),
			Email:    utils.MakeSimpleCaptcha(8) + "@qq.com",
			Address:  utils.MakeSimpleCaptcha(4) + "省" + utils.MakeSimpleCaptcha(2) + "市",
		})
		if i > 0 && i%batchSize == 0 {
			ch <- data
			wg.Add(1)
			data = make([]*Customer, 0)
		}
	}

	if len(data) > 0 {
		ch <- data
		wg.Add(1)
	}

}

/**
自动生成随机用户名数据
*/
func generatorName() string {
	name := ""
	index1 := rand.Intn(len(firstName))
	name += firstName[index1]
	index1 = rand.Intn(len(midName))
	name += midName[index1]
	if i := rand.Intn(100); i%2 == 0 {
		name += lastName[rand.Intn(len(lastName))]
	}
	return name
}

/**
Hash测试
*/
func Test_Hash(t *testing.T) {
	/*a:=5
	b:=3
	c:=a/b
	fmt.Println("商:",c)//取模c的值是向负无穷方向,取余向0方向  当a和b正负号一致时，求模运算和求余运算所得的c的值一致，因此结果一致。
	r:=a-c*b
	fmt.Println("r:",r)*/
	/*fmt.Println(154720 / 1024)
	fmt.Println(154709 / 1024)
	fmt.Println(134709 / 1024)*/
	/*b := make(chan int)
	go func() {
		for {
			select {
			case i := <-b:
				fmt.Println(i)

			}
		}
	}()
	time.Sleep(2*time.Second)
	b<-111*/

}

/**
淘宝搜索结果匹配
*/
func Test_Reg(t *testing.T) {
	str := `<script src="//g.alicdn.com/??kissy/k/1.4.15/import-style-min.js,tb/tracker/1.0.19/index.js,/tb/tsrp/1.70.26/config.js"
	            charset="utf-8"></script>
	    <script
	        src="//g.alicdn.com/searchInteraction/keyword-brand-pc/0.0.6/config.js"></script>
	    <script src="//g.alicdn.com/mm/staobao/1.6.0/index-min.js" charset="utf-8"></script>
	        <script>
	        Search.config({
	            base: '//g.alicdn.com/tb/tsrp/1.70.26/',
	            name: 'srp',
	            combine: true
	        });
	        window.givenByFE = window.givenByFE || {};
	    </script>

	    <script>

	 		g_page_config = {"pageName":"mainsrp","mods":{"shopcombotip":{"status":"hide"},"phonenav":{"status":"hide"},"debugbar":{"status":"hide"},"shopcombo":{"status":"hide"},"itemlist":{"status":"show","data":{"postFeeText":"运费","trace":"msrp_auction"}}}};
	 		g_srp_loadCss();
	    	g_srp_init();
	                window.givenByFE.tracerule= {"search_radio_all":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsearch_radio_all:1","spucombo_auction":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${data-traceclick}","srp_as_allcat":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_as_allcat:1","toolbarHistory":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dviewhistory:1","btmMybought":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpagetype:bottom;from_pos:${btmMyboughtFP[].#value}","combo_zk":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpersonal_button%3Azk","srprate":"lf_aclog\u003d${auctionNids[].#index}-${auctionNids[].#value}-${auctionReturnNum}-${sort}|${sort2}-${ifDoufuAuction[].#value}-${auctionPrices[].#value}\u0026srprate\u003d1\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}","listBtn":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:list","festivalAuction":"lf_aclog\u003d200-${data-nid}-null-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dchuizhi_tongyonglist:${data-floor}_${data-index};chuizhi_page:tongyong;sp_seller_type:${data-sp_seller_type}","v_carinfosave":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo;carinfo:1","quicklook_picture":"lf_aclog\u003d0-${auctionNids[].#value}-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_picture:1","gridBtn":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:grid","spucombo_more":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${data-traceclick}","quicklook_services":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_service:${auctionIconServices[].#value}","metis36":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dms_from:36","combo_series":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dfrom_pos%3A${page}_${combo}_0_null_link%3B${vertical}%3B${combo_type}","filterbox":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dfilterbox:${data-filterid}","shopRewrite":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dfrom_pos:${shopRewrite[].#value}","newrewrite":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${newRewriteType}","cacel_price_limit":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dcancel_price_limit:1","nav_tab_tags":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dnav_tab_tags:${data-position}","v_babysave":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:bbziliao_0","click_fastbuy":"lf_aclog\u003d${auctionNids[].#index}-${data-nid}-${auctionReturnNum}-${sort}|${sort2}-${ifDoufuAuction[].#value}-${auctionPrices[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dcharge%3Anow%3Bquery%3A${query}%3Bitem_id%3A${data-nid}","lowerbrowser_0":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dlowerbrowser:0","sortCreditDesc":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:credit-desc","detailAuction":"lf_aclog\u003d${auctionDetailNids[].#index}-${auctionDetailNids[].#value}-${auctionDetailReturnNum}-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${ws_detail}","lowerbrowser_1":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dlowerbrowser:1","lowerbrowser_2":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dlowerbrowser:2","sortPrice":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:${priceSorts[].#value}","lowerbrowser_3":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dlowerbrowser:3","findRelatedColorTab":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsuggest_fc:tab2","srp_as_radio_tmall":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_as_radio_tmall:1","navHideButton":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirectForMaidian}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026ppath\u003d${nav_properties[].#value}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dcps:yes;sn_hide_button:1","dual11banner":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${dual11banner}","imgauction":"lf_aclog\u003d${auctionNids[].#index}-${auctionNids[].#value}-0-all-0-${auctionPrices[].#value}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dauc_from:same","msrp_auction":"lf_aclog\u003d${auctionNids[].#index}-${auctionNids[].#value}-${auctionReturnNum}-${sort}|${sort2}-${ifDoufuAuction[].#value}-${auctionPrices[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%3Bsp_seller_type%3A${sp_seller_types[].#value}%3Bisp4p%3A${isp4p[].#value}%3Bnewitem%3A${activityClick[].#value}%3Bauction_pid%3A${data-pid}${exec-auctionClick}\u0026s_query\u003d${query}\u0026vers\u003dj\u0026list_model\u003d${list_model}\u0026recommend_nav\u003d${data-recommend-nav}\u0026risk\u003d${data-risk}","sortRenqiDesc":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:renqi-desc","shirinkBtn":"lf_aclog\u003dnull-null-40-coefp|-0-null\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click%3Aminijj","pingce":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%3Beval%3A1","sc":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${iframe_show};sc_spm:${sc_spm};sc_type:${sc_type}","relatedSearch":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${rsPositions[].#value}%3Bhotrelate%3A${relateHotTrace[].#value}\u0026s_query\u003d${query}","search_radio_globalbuy":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsearch_radio_globalbuy:1","layerrelation":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dlayer_relation:${data-floor}","item_tag":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003ditem_tag%3A1","tab_viewbuy":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${tab_viewbuy[].#value}","sortLast":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:double11-last","btmMycollect":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpagetype:bottom;from_pos:${btmMycollectFP[].#value}","quicklook_detail":"lf_aclog\u003d0-${auctionNids[].#value}-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_detail:1","noresultmetis_buybuy":"lf_aclog\u003d${noresult_metis_ids[].#index}-${noresult_metis_ids[].#value}-null-null-0-null\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dnorst_gxhtype:viewbuy","toolbarPageup":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpageup:1","spuNavCategory":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dcps:yes;${spu_nav_cateclick[].#value}\u0026s_query\u003d${query}","quicklook_next":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_right:1","customUGC_on":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dcfg_button%3Asz","rw-mod-birthday":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbirthday_icon:1","etaoPowerBtn":"lf_aclog\u003dnull-null-null-null-0-null\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpower_etao%3A1","items_nickname":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrpwwnick%3A${auctionNicks[].#value}","shopPromotion":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dol_prop:1","zkServiceTabClick":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpromotion_type:${data-tabName}","noresultmetis_collect":"lf_aclog\u003d${noresult_metis_ids[].#index}-${noresult_metis_ids[].#value}-null-null-0-null\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dnorst_gxhtype:collect","from_srp_logo":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dfrom_srp_logo:1","srp_bottom_page5":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_bottom_page5:1","srp_bottom_pageup":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_bottom_pageup:1","from_srp_tonglan_taobaolink":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dfrom_srp_tonglan_taobaolink:1","sameseller_item":"lf_aclog\u003d${sameSellerItemNids[].#index}-${sameSellerItemNids[].#value}-0-${sort}-0-${sameSellerItemPrices[].#value}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick};${sameSellerItemClick[].#value}","combo_collect":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpersonalprompt%3A1_4","knowledgeMap":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${srppage};${rsPositions[].#value}","quicklook_close":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_close:${quicklookCloseType[].#value}","srp_as_radio_globalbuy":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_as_radio_globalbuy:1","srpshop_ipv":"lf_aclog\u003d0-${inshopAuctionNids[].#value}-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrpshop_ipv:1;iframe_show:srpshop_1","auction_customized":"lf_aclog\u003d100${data-index}-${data-nid}-${data-num}-${sort}|${sort2}-0-${data-price}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%3Bsp_seller_type%3A0%3Bauction_pid%3A${data-pid}${exec-auctionClick}%3Butoi%3A1\u0026s_query\u003d${query}\u0026recommend_nav\u003d${data-recommend-nav}\u0026risk\u003d${data-risk}","srp_as_exbutton":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_as_exbutton:1","quicklook_btn":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_hover:1","commonTipClick":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dtip_click:${data-tracevalue}","search_radio_tmall":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsearch_radio_tmall:1","tab_buybuy":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${tab_buybuy[].#value}","spuNavProperty":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dcps:yes\u0026multivariate\u003d${multivariate}\u0026s_query\u003d${query}","sortDefault":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:default","tabTmall":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:mall","double11_auction":"lf_aclog\u003d${auctionNids[].#index}-${auctionNids[].#value}-${auctionReturnNum}-${sort}|${sort2}-${auctionPrices[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%3Bsp_seller_type%3A${sp_seller_types[].#value}%3Bauction_pid%3A${data-pid}${exec-auctionClick}\u0026s_query\u003d${query}\u0026vers\u003dj\u0026list_model\u003d${list_model}","sameSellerMore":"lf_aclog\u003dnull-null-${knowledgeBarReturnNum}-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsingleshop:2;from_pos:shopview_null_null_null_more","customUGC_enable":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dcfg_button%3Asave","vertical_spu":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dfrom_pos:${vertical_from_pos[].#value};${from_type};spucombo\u003d${spucombo}","srp_select_pageup":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_select_pageup:1","srp_as_tdbutton":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_as_tdbutton:1","tabAll":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:all","tabClickCommon":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:${data-tabName}","combo_product":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${product_combo_click}","combo_xp":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpersonal_button%3Anew","auction":"lf_aclog\u003d${auctionNids[].#index}-${auctionNids[].#value}-${auctionReturnNum}-${sort}|${sort2}-${ifDoufuAuction[].#value}-${auctionPrices[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%3Bsp_seller_type%3A${sp_seller_types[].#value}%3Bauction_pid%3A${data-pid}${exec-auctionClick}\u0026s_query\u003d${query}\u0026vers\u003dj\u0026list_model\u003d${list_model}","imgsearch_cat":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dps_cat:${data-imgcat}","ps_detail_comment":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dps_detail_comment:${reviewPosition}","more_market":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dmore_market:1","tabShopping":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:shopping","littleAuction":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dfew_words:${fewWords[].#index}","theme_auction":"lf_aclog\u003d${auctionNids[].#index}-${auctionNids[].#value}-${auctionReturnNum}-all|${sort2}-${ifDoufuAuction[].#value}-${auctionPrices[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%3Bsp_seller_type%3A${sp_seller_types[].#value}%3Bauction_pid%3A${data-pid}${exec-auctionClick}\u0026s_query\u003d${query}\u0026vers\u003dj","big_pic":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbig_pic:1","customUGC_sex_male":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dcfg_button%3Asex_man","user_rate":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003duser_rate:1","sku_auction":"${exec-skuClick}","layermore":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dlayer_more:${data-floor}","findRelatedBtn":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsuggest_fc:button","knowldegeAuction":"lf_aclog\u003dnull-null-${knowledgeBarReturnNum}-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${ws_qa}","spu_price":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${tab_viewbuy[].#value}\",\":\"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${spu_price[].#value}","btmSharealbum":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpagetype:bottom;from_pos:${btmSharealbumFP[].#value}","v_page2_auction":"lf_aclog\u003d${auctionNids[].#index}-${auctionNids[].#value}-${auctionReturnNum}-${sort}|${sort2}-null-${auctionPrices[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}","srp_bottom_pagedown":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_bottom_pagedown:1","ps_detail_wktag":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dps_detail_wktag:1","relatedBSearch":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${rsPositions[].#value}","location":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:loc","srpwwnick":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026srpwwnick\u003d${auctionNicks[].#value}","huodong_sc":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dhuodong_click:${huodong_sc_alias}","CommonStatsClick":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${data-statsKey}:${data-statsValue}","toolbarPagedown":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpagedown:1","noresultmetis_top":"lf_aclog\u003d${noresult_metis_ids[].#index}-${noresult_metis_ids[].#value}-null-null-0-null\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dnorst_gxhtype:top","v_page2_nav":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dcps:yes","srp_hebingtongkuan_cancel":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dhebingtk_status:2","imgrecauction":"lf_aclog\u003d${recommendNids[].#index}-${recommendNids[].#value}-0-all-0-${recommendPrices[].#value}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dauc_from:recom","quicklook_service":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_service:${auctionIconServices[].#value}","knowledgeAuctionMore":"lf_aclog\u003dnull-null-${knowledgeBarReturnNum}-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dws_qa_more:${ws_qa_position}","ps_detail_emotional":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dps_detail_emotional:1","lessgraph":"lf_aclog\u003d${lessgraphNids[].#index}-${lessgraphNids[].#value}-${lessgraphReturnNum}-${sort}|${sort2}-0-${lessgraphPrices[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}","quicklook_title":"lf_aclog\u003d0-${auctionNids[].#value}-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_title:1","sortOldStarts":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:old_starts","noresultrec_tip":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dnorescw:${norescwTip[].#value}","theme_activity":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%3Bnewitem%3A${activityClick[].#value}","tabOld":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:old","metis2":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dms_from:2","metis1":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dms_from:1","srpshop_spv":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrpshop_spv%3A1%3Biframe_show%3Asrpshop_1%3Bsrpshop_sellerid%3A${shopComboShopId}","mysearchU2iAuction":"lf_aclog\u003d${u2iAuctionNids[].#index}-${u2iAuctionNids[].#value}-${u2iAuctionReturnNum}-${sort}|${sort2}-null-${u2iAuctionPrices[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%recom_type%3Au2i${exec-auctionClick}","v_btn7":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_btn7}","v_btn8":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_btn8}","gobuy":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dgobuy:1","navPropertyNew":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirectForMaidian}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026ppath\u003d${exec-navPPath}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dcps:yes;${data-click}\u0026s_query\u003d${query}","v_btn3":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_btn3}","v_btn4":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_btn4}","v_btn5":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_btn5}","v_btn6":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_btn6}","combo_bought":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpersonalprompt%3A1_3","btnclick":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirectForMaidian}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026ppath\u003d${exec-navPPath}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${data-trace}\u0026s_query\u003d${query}","selection_guide":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dselection_guide:1","v_nav4":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_nav4[].#value}","personalization":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpersonal_fc:${personalization}","v_nav5":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_nav5[].#value}","v_nav2":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_nav2[].#value}","items_rate":"lf_aclog\u003d${auctionNids[].#value}-${auctionNids[].#value}-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrprate:1","v_nav3":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_nav3[].#value}","v_nav8":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_nav8[].#value}","v_nav6":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_nav6[].#value}","v_nav7":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_nav7[].#value}","auctionColorRelatedShopping":"lf_aclog\u003d${relColorAuctionNids[].#index}-${relColorAuctionNids[].#value}-${relColorAuctionReturnNum}-${sort}|${sort2}-${relColorIfDoufuAuction[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%3bsuggest_fc%3atab2","v_nav0":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_nav0[].#value}","v_nav1":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_nav1[].#value}","vertical_seller":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dfrom_pos:${vertical_from_pos[].#value};ps_detail:seller","anticheat":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003danticheat:1","newrewriteitem":"lf_aclog\u003d${newRewriteNids[].#index}-${newRewriteNids[].#value}-null-${sort}|${sort2}-null\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}","lsrp_auction":"lf_aclog\u003d${auctionNids[].#index}-${auctionNids[].#value}-${auctionReturnNum}-${sort}|${sort2}-${ifDoufuAuction[].#value}-${auctionPrices[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%3Bsp_seller_type%3A${sp_seller_types[].#value}%3Bnewitem%3A${activityClick[].#value}%3Bauction_pid%3A${data-pid}${exec-auctionClick}\u0026s_query\u003d${query}\u0026vers\u003dj","vertical_activity":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dfrom_pos:${vertical_from_pos[].#value};ps_detail:sales","i2i_btn_click":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003di2i_srppv:1","customUGC_off":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dnotcfg%3A1","shopNewest":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dol_new:1","navTextTag":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirectForMaidian}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dcps:yes;${data-click}\u0026s_query\u003d${query}","btmHandpick":"lf_aclog\u003d${btmAucIdxes[].#value}-${btmAucNids[].#value}-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpagetype:bottom;from_pos:${btmHandpickFP[].#value}","toolbarSearchhelp":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsearchhelp:1","navCategoryNew":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirectForMaidian}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026cat\u003d${nav_categories[].#value}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dcps:yes;${data-trace-click}\u0026s_query\u003d${query}","auction_u2i":"lf_aclog\u003d100${data-index}-${data-nid}-${data-num}-${sort}|${sort2}-0-${data-price}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%3Bsp_seller_type%3A0%3Bauction_pid%3A${data-pid}${exec-auctionClick}%3Butoi%3A1\u0026s_query\u003d${query}","toobarFeedback":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dfeedback:1","ps_article":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dps_article:1","srp_as_radio_all":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_as_radio_all:1","auctionRelatedShopping":"lf_aclog\u003d${relAuctionNids[].#index}-${relAuctionNids[].#value}-${relAuctionReturnNum}-${sort}|${sort2}-${relIfDoufuAuction[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%3bsuggest_fc%3atab1","customUGC_purchase_3":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dcfg_button%3Aprice_higher","sameSeller":"lf_aclog\u003dnull-null-0-0-0-0\u0026stats_click\u003dbutton_click:relatedAuction","quicklook_star":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_star:${auctionNicks[].#value}","quicklook_hover":"lf_aclog\u003dnull-null-0-0-0-0\u0026cat\u003d${catLevelOne}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_hover:1","wanke":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dwanke:${link-order}","customUGC_purchase_1":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dcfg_button%3Aprice_lower","srp_select_pagedown":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_select_pagedown:1","customUGC_purchase_2":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dcfg_button%3Aprice_middle","srp_bottom_page2":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_bottom_page2:1","srp_bottom_page1":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_bottom_page1:1","srp_bottom_page4":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_bottom_page4:1","search_radio_old":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsearch_radio_old:1","mysearchSupplyAuction":"lf_aclog\u003d${supplyAuctionNids[].#index}-${supplyAuctionNids[].#value}-${supplyAuctionReturnNum}-${sort}|${sort2}-null-${supplyAuctionPrices[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}${supplyTypes[].#value}${exec-auctionClick}","srp_bottom_page3":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_bottom_page3:1","quicklook_ww":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_ww:${auctionNicks[].#value}","srp_as_close":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_as_close:1","relatedShop":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003drsshop:1","imgsearchbtn":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dps_upload:1","items_services":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrpwwnick%3A${nicknames[].#value}","seller_location":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dseller_shipping_location:${data-tracetype}","srpservice":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrpservice:${auctionIconServices[].#value}","mysearch_shop_oper":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dms_indact:${data-indact}","fashion_fast_search":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003d${srppage};fashion_fast_search:${data-position}\u0026s_query\u003d${query}","quicklook_prev":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026cat\u003d${catLevelOne}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_left:1","v_auction8":"lf_aclog\u003d${v_auction4[].#index}-${v_auction4[].#value}-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick};${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_auction_chuizhicombo8};${tab_type}","v_auction6":"lf_aclog\u003d${v_auction4[].#index}-${v_auction4[].#value}-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick};${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_auction_chuizhicombo6};${tab_type}","v_auction7":"lf_aclog\u003d${v_auction4[].#index}-${v_auction4[].#value}-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick};${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_auction_chuizhicombo7};${tab_type}","btmSpecialshop":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpagetype:bottom;from_pos:${btmSpecialshopFP[].#value}","v_auction5":"lf_aclog\u003d${v_auction4[].#index}-${v_auction4[].#value}-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick};${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_auction_chuizhicombo5};${tab_type}","v_auction2":"lf_aclog\u003d${v_auction2[].#index}-${v_auction2[].#value}-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick};${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_auction_chuizhicombo2};${tab_type}","v_auction3":"lf_aclog\u003d${v_auction3[].#index}-${v_auction3[].#value}-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick};${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_auction_chuizhicombo3};${tab_type}","birthday_sc_btn_2":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${iframe_show};birthday_fc:2","v_auction0":"lf_aclog\u003d${v_auction0[].#index}-${v_auction0[].#value}-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick};${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_auction_chuizhicombo0};${tab_type}","v_auction1":"lf_aclog\u003d${v_auction1[].#index}-${v_auction1[].#value}-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick};${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_auction_chuizhicombo1};${tab_type}","v_btn0":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_btn0}","birthday_sc_btn_3":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${iframe_show};birthday_fc:3","v_btn1":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_btn1}","birthday_sc_btn_1":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${iframe_show};birthday_fc:1","v_btn2":"lf_aclog\u003dnull-null-null-|-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${chuizhi_page};${chuizhi_list};chuizhi_combo:${v_btn2}","toolBarTohead":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dtohead:1","i2i_item":"lf_aclog\u003d${i2iItemNids[].#index}-${i2iItemNids[].#value}-0-${sort}-0-${i2iItemPrices[].#value}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick};${i2iItemClick[].#value}","srp_as_button":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_as_button:1","quicklook_commentNumber":"lf_aclog\u003d0-${auctionNids[].#value}-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_rate:1","quicklook_comment":"lf_aclog\u003d0-${auctionNids[].#value}-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dquick_look_comment:1","srp_as_radio_old":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrp_as_radio_old:1","customUGC_sex_female":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dcfg_button%3Asex_woman","lessresearch":"lf_aclog\u003d${lessresearchNids[].#index}-${lessresearchNids[].#value}-${lessresearchReturnNum}-${sort}|${sort2}-0-${lessresearchPrices[].#value}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${lessresearchtype}","tab_viewview":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${tab_viewview[].#value}","sortSaleDesc":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:sale-desc","questionAuction":"lf_aclog\u003dnull-null-${questionNum}-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${ws_baike}","parameter":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dparameter:1","srp_hebingtongkuan":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dhebingtk_status:1","rw-mod-weather-query":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dweather_fc:1","combo_sns":"lf_aclog\u003dnull-null-${auctionReturnNum}-all|-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dpersonalprompt%3A1_2","scene_auction":"lf_aclog\u003d${data-index}-${data-nid}-${data-num}-all|-0-${data-price}\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${statsClick}%3Bsp_seller_type%3A0%3Bauction_pid%3A${data-pid}${exec-auctionClick}\u0026s_query\u003d${query}\u0026vers\u003dj","ps_detail_property":"lf_aclog\u003dnull-null-null-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dps_detail_property:${ps_detail_property[].#value}","imgsearch_gender":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026stats_click\u003dps_gender:${data-imggender}","combo_link":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dfrom_pos%3A${page}_${combo}_0_null_link%3B${vertical}","knowldegelist":"lf_aclog\u003dnull-null-${knowledgeListReturnNum}-${sort}|${sort2}-0-null\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003d${ws_gwzs}","findRelatedSeeSeeTab":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsuggest_fc:tab1","metis44":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dms_from:44","srpshop_gotolook":"lf_aclog\u003dnull-null-0-0-0-0\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dsrpshop_gotolook:1;iframe_show:srpshop_2","sortAgain":"lf_aclog\u003dnull-null-${auctionReturnNum}-${sort}|${sort2}-null\u0026direct_cat\u003d${catdirect}\u0026lf_acfrom\u003d${if_tank}\u0026at_alitrackid\u003d${alitrackid}\u0026at_bucketid\u003d${bucketId}\u0026multi_bucket\u003d${multi_bucket}\u0026srppage\u003d${srppage}\u0026nick\u003d${nick}\u0026rn\u003d${rn}\u0026multivariate\u003d${multivariate}\u0026stats_click\u003dbutton_click:double11-again"};
	</script>

	    <link rel="search" type="application/opensearchdescription+xml" title="淘宝搜索" href="/opensearch.xml">
	</head>
	`
	reg2 := regexp.MustCompile(`g_page_config\s*=\s*(\{.+\})`)
	matchedData := reg2.FindStringSubmatch(str)
	if len(matchedData) == 2 {
		fmt.Println(matchedData[1])
	} else {
		fmt.Println("匹配失败")
	}
	/*str2:=`g_page_config={name:"张三",age:20,data:{"a":"33",b:["1",2,"rest"]}};
			g_srp_loadCss();
				g_srp_init();`
	reg2:= regexp.MustCompile(`g_page_config=(\{.+\})`)
	fmt.Println(reg2.FindStringSubmatch(str2)[1])*/
	//str := "Hello World"
	//reg, _ := regexp.Compile(`\w\w+`)
	//fmt.Printf("%q\n", reg.FindString(str))

	/*reg = regexp.MustCompile(`(\w)(\w+)`)
	fmt.Println(reg.FindStringSubmatch(str))
	reg = regexp.MustCompile(`(\w)(\w)+`)
	fmt.Println(reg.FindStringSubmatch(str))*/
}

/**
淘宝分页搜索结果匹配
*/
func Test_TbPaginationReg(t *testing.T) {
	//str := `jsonp1384({'page':1,'name':'王会东'});`
	str := `



jsonp951(
{
    "pageName": "mainsrp",
    "mods": {
        "tab": {
            "status": "show",
            "data": {
                "tabs": [
                    {
                        "type": "all",
                        "weight": 10,
                        "name": "all",
                        "id": "tabFilterAll",
                        "trace": "",
                        "href": "//s.taobao.com/search?ie\u003dutf8\u0026js\u003d1\u0026imgfile\u003d\u0026q\u003d大白菜\u0026suggest\u003dhistory_2\u0026_input_charset\u003dutf-8\u0026wq\u003dABCDE\u0026suggest_query\u003dABCDE\u0026source\u003dsuggest\u0026tab\u003dall",
                        "text": "所有宝贝",
                        "isActive": true,
                        "spmId": ""
                    },
                    {
                        "type": "all",
                        "weight": 20,
                        "name": "mall",
                        "id": "tabFilterMall",
                        "trace": "",
                        "href": "//s.taobao.com/search?ie\u003dutf8\u0026js\u003d1\u0026imgfile\u003d\u0026q\u003d大白菜\u0026suggest\u003dhistory_2\u0026_input_charset\u003dutf-8\u0026wq\u003dABCDE\u0026suggest_query\u003dABCDE\u0026source\u003dsuggest\u0026tab\u003dmall",
                        "text": "天猫",
                        "isActive": false,
                        "spmId": "d4919860"
                    },
                    {
                        "type": "all",
                        "weight": 30,
                        "name": "old",
                        "id": "tabFilterOld",
                        "trace": "",
                        "href": "//s.taobao.com/search?ie\u003dutf8\u0026js\u003d1\u0026imgfile\u003d\u0026q\u003d大白菜\u0026suggest\u003dhistory_2\u0026_input_charset\u003dutf-8\u0026wq\u003dABCDE\u0026suggest_query\u003dABCDE\u0026source\u003dsuggest\u0026tab\u003dold",
                        "text": "二手",
                        "isActive": false,
                        "spmId": ""
                    }
                ],
                "spmModId": "1998181369"
            },
            "export": false
        },
        "feedback": {
            "status": "show",
            "data": {
                "render": true,
                "useOld": true,
                "showType": ""
            },
            "export": false
        },
        "p4p": {
            "status": "show",
            "data": {
                "baobeiExtraClass": "",
                "etaoAds": true,
                "p4pconfig": {
                    "keyword": "%E5%A4%A7%E7%99%BD%E8%8F%9C",
                    "keywordGBK": "%B4%F3%B0%D7%B2%CB",
                    "catid": "",
                    "propertyid": "",
                    "ip": "39.105.16.22",
                    "loc": "",
                    "gprice": "",
                    "sort": "",
                    "sbid": "",
                    "q2cused": 0,
                    "pageNum": 1,
                    "p4pbottom_up": false,
                    "b2b_show": false,
                    "etao_wanke": false,
                    "etao_effect": false,
                    "offset": 0,
                    "refpid": "",
                    "source": "",
                    "xmatch": 0,
                    "rn": "e27697f057c001b2371bba3c4c58db21",
                    "ismall": "",
                    "srp": "mainsrp",
                    "tags": "",
                    "p4p_btsinfo": "",
                    "auction_tag": "",
                    "has_sku_pic": false,
                    "auction_num": {
                        "search": 0
                    },
                    "doufu": 0,
                    "fp_pushleft": 0,
                    "time": "1654756836"
                }
            },
            "export": false
        },
        "noresult": {
            "status": "show",
            "data": {
                "noResultCode": 0,
                "queryShow": "大白菜",
                "render": true
            },
            "export": false
        },
        "header": {
            "status": "show",
            "data": {
                "q": "大白菜",
                "tabParams": {
                    "js": "1",
                    "stats_click": "search_radio_all:1",
                    "initiative_id": "staobaoz_block",
                    "ie": "utf8"
                },
                "dropdown": [
                    {
                        "url": "/search",
                        "text": "宝贝",
                        "type": "item",
                        "isActive": true
                    },
                    {
                        "url": "//shopsearch.taobao.com/search",
                        "text": "店铺",
                        "type": "shop",
                        "isActive": false
                    }
                ]
            },
            "export": false
        },
        "itemlist": {
            "status": "hide",
            "export": false
        },
        "sortbar": {
            "status": "show",
            "data": {
                "sortList": [
                    {
                        "name": "综合",
                        "tip": "综合排序",
                        "trace": "",
                        "isActive": true,
                        "value": "default",
                        "key": "sort"
                    },
                    {
                        "name": "销量",
                        "tip": "销量从高到低",
                        "trace": "",
                        "isActive": false,
                        "value": "sale-desc",
                        "key": "sort"
                    },
                    {
                        "name": "信用",
                        "tip": "信用从高到低",
                        "trace": "",
                        "isActive": false,
                        "value": "credit-desc",
                        "key": "sort"
                    },
                    {
                        "name": "价格",
                        "tip": "价格从低到高",
                        "trace": "",
                        "isActive": false,
                        "value": "price-asc",
                        "key": "sort",
                        "dropdownList": [
                            {
                                "name": "价格",
                                "tip": "价格从低到高",
                                "value": "price-asc",
                                "trace": ""
                            },
                            {
                                "name": "价格",
                                "tip": "价格从高到低",
                                "value": "price-desc",
                                "trace": ""
                            },
                            {
                                "name": "总价",
                                "tip": "总价从低到高",
                                "value": "total-asc",
                                "trace": ""
                            },
                            {
                                "name": "总价",
                                "tip": "总价从高到低",
                                "value": "total-desc",
                                "trace": ""
                            }
                        ]
                    }
                ],
                "pager": {
                    "pageSize": 44,
                    "totalPage": 1,
                    "currentPage": 1,
                    "totalCount": 0
                },
                "filter": [
                    {
                        "isActive": false,
                        "value": "1",
                        "title": "包邮",
                        "key": "baoyou",
                        "trace": "",
                        "traceData": {
                            "filterid": "filter_baoyou"
                        },
                        "isHighlight": false,
                        "pos": 0,
                        "dom_id": "filter_baoyou"
                    },
                    {
                        "isActive": false,
                        "value": "385",
                        "title": "赠送 退货运费险",
                        "key": "auction_tag[]",
                        "trace": "",
                        "traceData": {
                            "filterid": "filterYunFeiXian"
                        },
                        "isHighlight": false,
                        "pos": 0,
                        "dom_id": "filterYunFeiXian"
                    },
                    {
                        "isActive": false,
                        "value": "1",
                        "title": "货到付款",
                        "key": "support_cod",
                        "trace": "",
                        "traceData": {
                            "filterid": "filterServiceCOD"
                        },
                        "isHighlight": false,
                        "pos": 0,
                        "dom_id": "filterServiceCOD"
                    },
                    {
                        "isActive": false,
                        "value": "1",
                        "title": "海外商品",
                        "key": "globalbuy",
                        "trace": "",
                        "traceData": {
                            "filterid": "filterServiceOversea"
                        },
                        "isHighlight": false,
                        "pos": 0,
                        "dom_id": "filterServiceOversea"
                    },
                    {
                        "isActive": false,
                        "value": "1",
                        "title": "二手",
                        "key": "filterFineness",
                        "trace": "",
                        "traceData": {
                            "filterid": "filter_ershou"
                        },
                        "isHighlight": false,
                        "pos": 0,
                        "dom_id": "filter_ershou"
                    },
                    {
                        "isActive": false,
                        "value": "tmall",
                        "title": "天猫",
                        "key": "filter_tianmao",
                        "trace": "",
                        "traceData": {
                            "filterid": "filter_tianmao"
                        },
                        "isHighlight": false,
                        "pos": 0,
                        "dom_id": "filter_tianmao"
                    },
                    {
                        "isActive": false,
                        "value": "1",
                        "title": "正品保障",
                        "key": "user_type",
                        "trace": "",
                        "traceData": {
                            "filterid": "filterProtectionQuality"
                        },
                        "isHighlight": false,
                        "pos": 0,
                        "dom_id": "filterProtectionQuality"
                    },
                    {
                        "isActive": false,
                        "value": "1",
                        "title": "24小时内发货",
                        "key": "consign_date",
                        "trace": "",
                        "traceData": {
                            "filterid": "fahuobaozhang"
                        },
                        "isHighlight": false,
                        "pos": 0,
                        "dom_id": "fahuobaozhang"
                    },
                    {
                        "isActive": false,
                        "value": "4806",
                        "title": "7+天内退货",
                        "key": "auction_tag[]",
                        "trace": "",
                        "traceData": {
                            "filterid": "tuihuochengnuo"
                        },
                        "isHighlight": false,
                        "pos": 0,
                        "dom_id": "tuihuochengnuo"
                    },
                    {
                        "isActive": false,
                        "value": "yes",
                        "title": "旺旺在线",
                        "key": "olu",
                        "trace": "",
                        "traceData": {
                            "filterid": "filterServiceWWOnline"
                        },
                        "isHighlight": false,
                        "pos": 0,
                        "dom_id": "filterServiceWWOnline"
                    }
                ],
                "location": {
                    "active": "",
                    "guessLoc": "",
                    "usualLoc": []
                },
                "style": "grid"
            },
            "export": false
        },
        "bottomsearch": {
            "status": "show",
            "data": {
                "query": "大白菜",
                "showSearchBox": true
            },
            "export": false
        }
    },
    "mainInfo": {
        "currentUrl": "//s.taobao.com/search?q=%E5%A4%A7%E7%99%BD%E8%8F%9C&commend=all&ssid=s5-e&search_type=item&sourceId=tb.index&spm=a21bo.jianhua.201856-taobao-item.2&ie=utf8&initiative_id=tbindexz_20170306&bcoffset=1&ntoffset=3&p4ppushleft=2%25!C(MISSING)48&s=88",
        "modLinks": {
            "default": "//s.taobao.com/search?q=%E5%A4%A7%E7%99%BD%E8%8F%9C&commend=all&ssid=s5-e&search_type=item&sourceId=tb.index&spm=a21bo.jianhua.201856-taobao-item.2&ie=utf8&initiative_id=tbindexz_20170306&bcoffset=1&ntoffset=3&p4ppushleft=2%25!C(MISSING)48&s=88"
        },
        "srpGlobal": {
            "q": "大白菜",
            "encode_q": "%E5%A4%A7%E7%99%BD%E8%8F%9C",
            "utf8_q": "大白菜",
            "cat": "",
            "s": 1,
            "tnk": "",
            "bucketid": 0,
            "multi_bucket": "",
            "style": "grid",
            "initiative_id": "",
            "machine": "",
            "buckets": "",
            "sp_url": "",
            "srpName": "mainsrp"
        },
        "traceInfo": {
            "pvStat": "vers\u003dj\u0026list_model\u003dgrid\u0026searchurl\u003d1\u0026cat\u003d\u0026direct_cat\u003d\u0026at_lflog\u003d0-0-0-0-2-0-0-all|\u0026at_bucketid\u003d14\u0026multi_bucket\u003d16_14_11_0_0_0_0_0\u0026at_colo\u003det2\u0026at_host\u003dsearchappintern010176144159.et2\u0026alitrackid\u003dwww.atatech.org\u0026at_alitrackid\u003dwww.atatech.org\u0026last_alitrackid\u003dwww.taobao.com\u0026stats_show\u003duser_group%3AClusterMergeInfo%3Aexcellent%3A0.21%3Blunbo%3A0.79%3Bmd_QueryIntentionType%3A%3Bhas_p4p%3A0%3Bs%3Amainsearch%3Bqinfo%3A1%2C2%2C4%2C10%2C23%2C34%2C40%2C50%2C55%2C61%2C88%2C95%2C100%2C117%2C146%2C153%2C160%2C171%2C301%2C307%2C604%2C704%2C721%2C1100%2C1106%2C1498%2C10800016%2C10800105%3Bapass%3A0%3Bhas_sku_pic%3A0%3Bsearch_radio_all%3A1%3Bsn_hide%3A0%3Bsuggest%3A5_2%3Btab_type%3Aall%3B\u0026rn\u003d098c4f2a91ac0fe4e4179461399351ec\u0026nick\u003dprincehaku\u0026multivariate\u003dmain_alg%3A276%3Bmain_fe%3A283%3Bmain_fe_extend%3A4534%3Bpersonal%3A5174%3Bpfourp_mbox%3A4754%3Bpfourp_test%3A565%3Bsp_bts%3A311%3Btoufang_taobao%3A5196\u0026srppage\u003d1\u0026s_query\u003dBLOCK_SRP",
            "traceData": {
                "lastAlitrackid": "www.taobao.com",
                "bucketId": "",
                "qinfo": "",
                "apass": "",
                "qp_bury": "",
                "remoteip": "42.120.74.158",
                "catpredict_bury": "",
                "from_pos": "",
                "auctionReturnNum": "0",
                "catdirectForMaidian": "",
                "noResultCode": "-3",
                "colo": "",
                "newRewriteType": "rewrite:0",
                "at_colo": "",
                "querytype_bury": "",
                "hostname": "",
                "list_model": "grid",
                "catLevelOne": "",
                "doufuAuctionNum": "0",
                "suggest": "",
                "priceSorts": [
                    "price-asc",
                    "price-desc",
                    "total-asc",
                    "total-desc"
                ],
                "nick": "",
                "ifDoufuAuction": [],
                "catdirect": "",
                "srppage": "1",
                "auctionNicks": [],
                "cat": "",
                "at_bucketid": "",
                "sort": "all",
                "rn": "",
                "query": "",
                "at_lflog": "0-0-0-0-2-0-0-all|",
                "if_tank": "0",
                "at_host": "",
                "sp_seller_types": [],
                "alitrackid": "",
                "navType": "0",
                "spu_combo": "",
                "is_rs": "0",
                "multivariate": "",
                "has_sku_pic": "has_sku_pic:0",
                "show_compass": "0",
                "omit_rewrite": "omit_rewrite:1",
                "statsClick": "",
                "statsClickInUrl": "search_radio_all:1",
                "auctionPrices": [],
                "tabType": "tab_type:all",
                "navStatus": "0",
                "noresult": "0",
                "if_rs": "0",
                "multi_bucket": "",
                "rewriteQuery": "",
                "rewrite_bury": "",
                "auctionIconServices": [],
                "rewriteStatus": "0",
                "price_rank": "0",
                "sort2": "",
                "isp4p": [],
                "activityClick": [],
                "auctionNids": []
            }
        },
        "remainMods": []
    },
    "feature": {
        "retinaOff": false,
        "shopcardOff": false,
        "webpOff": false
    },
    "map": {}
}
    )`
	str=strings.Replace(str," ","",-1)
	str=strings.Replace(str,"\n","",-1)
	reg2 := regexp.MustCompile(`jsonp\d+\(\n*(.+)\n*\)`)
	matchedData := reg2.FindStringSubmatch(str)
	if matchedData !=nil && len(matchedData)==2{
		log.Println(matchedData[1])
	}
	/*for _, val := range matchedData {
		log.Println(val + "\n")
	}*/
}

func Test_UrlParse(t *testing.T) {
	escape := url.QueryEscape("东北大米")
	fmt.Println("escape:", escape)
}
