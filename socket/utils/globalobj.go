package utils

import (
	"encoding/json"
	"gitee.com/smallow/spirit/socket/siface"
	"io/ioutil"
	"log"
)

/**
框架在单独使用不依赖webManage时候的全局配置参数
*/
type GlobalObj struct {
	/**
	Server
	*/
	TcpServer siface.IServer `json:"-"`
	Host      string         `json:"host"`
	TcpPort   int            `json:"tcp_port"`
	Name      string         `json:"name"`

	/**
	STCP相关
	*/
	Version        string `json:"version"`          //当前TCP2消息框架版本号
	MaxConn        int    `json:"max_conn"`         //最大连接数
	MaxPackageSize uint32 `json:"max_package_size"` //数据包最大值

	WorkerPoolSize    uint32 `json:"worker_pool_size"`     //当前业务工作worker go routine的数量
	MaxWorkerTaskSize uint32 `json:"max_worker_task_size"` //最大worker数量
}

/**
定义一个全局的对外GlobalObj
*/
var TCP2GlobalObject *GlobalObj

func (g *GlobalObj) Reload() {
	bs, err := ioutil.ReadFile("conf/socket.json")
	if err != nil {
		log.Println("GlobalObj configuration Reload error:", err)
		return
	}
	err = json.Unmarshal(bs, &TCP2GlobalObject)
	if err != nil {
		log.Println("GlobalObj configuration Unmarshal error:", err)
	}
}

func init() {
	TCP2GlobalObject = &GlobalObj{
		TcpServer:         nil,
		Host:              "127.0.0.1",
		TcpPort:           7070,
		Name:              "TCP2Server",
		Version:           "V0.7",
		MaxConn:           1000,
		MaxPackageSize:    4096,
		WorkerPoolSize:    10,
		MaxWorkerTaskSize: 1024,
	}
	TCP2GlobalObject.Reload()
}
