package utils

import (
	"bytes"
	"crypto/rand"
	"encoding/binary"
	"encoding/hex"
)

func Byte2Int(src []byte) int32 {
	var ret int32
	binary.Read(bytes.NewReader(src), binary.BigEndian, &ret)
	return ret
}


func Int2Byte(src int32) []byte {
	buf := bytes.NewBuffer([]byte{})
	binary.Write(buf, binary.BigEndian, src)
	return buf.Bytes()
}


func Uint322Byte(src uint32) []byte {
	buf := bytes.NewBuffer([]byte{})
	binary.Write(buf, binary.BigEndian, src)
	return buf.Bytes()
}

const (
	KEY = "()*+-_=~`!@#$%^&*{}[]|',.<>?abcdefghhijklmnopqrstuvwxyz0123456789ABCDEFJHIJKLMNOPQRSTUVWXYZ"
)

//保证系统ID唯一
func UUID(length int) string {
	tmp := make([]byte, length>>1)
	rand.Read(tmp)
	return hex.EncodeToString(tmp)
}
