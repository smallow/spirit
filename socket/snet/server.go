package snet

import (
	"fmt"
	"gitee.com/smallow/spirit/socket/siface"
	"gitee.com/smallow/spirit/socket/utils"
	"log"
	"net"
)

//iServer的接口实现
type Server struct {
	Name        string
	IPVersion   string //服务器绑定的ip版本
	IP          string //服务器监听的ip
	Port        int    //服务器监听的端口
	MsgHandle   siface.IMsgHandle
	ConnMgr     siface.IConnManager
	OnConnStart func(conn siface.IConnection)
	OnConnStop  func(conn siface.IConnection)
}

/*func callBackToClient(conn *net.TCPConn, data []byte, cnt int, cid uint32) error {
	log.Println("[Conn Handle] CallBackTo clientID :", cid)
	conn.Write(data[:cnt])
	return nil
}*/

func (s *Server) Start() {
	log.Printf("[TCP2] Server Name:\"%s\",Starting......", utils.TCP2GlobalObject.Name)
	log.Printf("[TCP2] Version:%s,MaxConn:%d", utils.TCP2GlobalObject.Version, utils.TCP2GlobalObject.MaxConn)
	log.Printf("[TCP2] Start Server Listening at IP:%s,Port:%d \n", s.IP, s.Port)

	go func() {
		//0 开启消息队列工作池
		s.MsgHandle.StartWorkerPool()

		//1 获取一个tcp的addr
		addr, err := net.ResolveTCPAddr(s.IPVersion, fmt.Sprintf("%s:%d", s.IP, s.Port))
		if err != nil {
			fmt.Println("resolve tcp addr error:", err)
			return
		}
		//2 监听服务器地址
		listener, err := net.ListenTCP(s.IPVersion, addr)
		if err != nil {
			fmt.Printf("listen %s  error:%v", s.IPVersion, err)
			return
		}
		log.Printf("[TCP2] start spirit tcp server %s success,listening......\n", s.Name)
		//3 阻塞的等待客户端连接
		var cid uint32
		for {
			//如果有客户端连接过来,阻塞就会返回
			conn, err := listener.AcceptTCP()
			if err != nil {
				fmt.Println("Accept err:", err)
				continue
			}
			//log.Println("******************utils.TCP2GlobalObject.MaxConn:*************", utils.TCP2GlobalObject.MaxConn)
			//log.Println("******************s.ConnMgr.Len():*************", s.ConnMgr.Len())
			//判断最大连接数
			if s.ConnMgr.Len() >= utils.TCP2GlobalObject.MaxConn {
				log.Println("Too Many Connections......Refuse Conn join request")
				conn.Close()
				continue
			}

			dealConn := NewConnection(s, conn, cid, s.MsgHandle)
			cid++
			//启动当前的链接处理业务
			go dealConn.Start()
		}
	}()

}

func (s *Server) Stop() {

	s.ConnMgr.ClearConn()
}

func (s *Server) Serve() {
	//启动服务功能
	s.Start()

	//TODO 做一些启动服务器之后的额外业务

	//阻塞
	select {}
}

/*func (s *Server) AddRouter(router siface.IRouter) {
	s.Router = router
	log.Println("Add Router Success!!")
}*/

func (s *Server) AddRouter(msgID uint32, router siface.IRouter) {
	s.MsgHandle.AddRouter(msgID, router)
}

func (s *Server) GetConnMgr() siface.IConnManager {
	return s.ConnMgr
}

/**
初始化Server
*/
func NewServer() siface.IServer {
	s := &Server{
		Name:      utils.TCP2GlobalObject.Name,
		IPVersion: "tcp4",
		IP:        utils.TCP2GlobalObject.Host,
		Port:      utils.TCP2GlobalObject.TcpPort,
		MsgHandle: NewMsgHandle(),
		ConnMgr:   NewConnManager(),
	}
	return s
}

func (s *Server) SetOnConnStart(hookFunc func(connection siface.IConnection)) {
	s.OnConnStart = hookFunc
}
func (s *Server) SetOnConnStop(hookFunc func(connection siface.IConnection)) {
	s.OnConnStop = hookFunc
}

func (s *Server) CallOnConnStart(connection siface.IConnection) {
	if s.OnConnStart != nil {
		log.Println("call connID=", connection.GetConnID(), " onConn start!")
		s.OnConnStart(connection)
	}
}
func (s *Server) CallOnConnStop(connection siface.IConnection) {
	if s.OnConnStop != nil {
		log.Println("call connID=", connection.GetConnID(), " onConn stop!")
		s.OnConnStop(connection)
	}
}
