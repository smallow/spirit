package snet

import "gitee.com/smallow/spirit/socket/siface"

/**
实现router时候先继承这个BaseRouter基类
*/
type BaseRouter struct {
}

/**
baseRouter的实现方法都是空 是因为有的router不希望有PreHandle、PostHandle这两个业务
*/
func (br *BaseRouter) PreHandle(request siface.IRequest)  {}
func (br *BaseRouter) Handle(request siface.IRequest)     {}
func (br *BaseRouter) PostHandle(request siface.IRequest) {}
