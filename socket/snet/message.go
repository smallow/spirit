package snet

type Message struct {
	ID      uint32
	DataLen uint32
	Data    []byte
}

func (m *Message) GetMessageID() uint32 {
	return m.ID
}
func (m *Message) GetMsgLen() uint32 {
	return m.DataLen
}

func (m *Message) GetData() []byte {
	return m.Data
}

func (m *Message) SetData(data []byte) {
	m.Data = data
}
