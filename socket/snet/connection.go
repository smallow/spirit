package snet

import (
	"gitee.com/smallow/spirit/socket/siface"
	"gitee.com/smallow/spirit/socket/utils"
	"github.com/pkg/errors"
	"io"
	"log"
	"net"
	"sync"
)

type Connection struct {
	//当前Conn属于哪个server
	TcpServer siface.IServer
	//当前连接的socket tcp套接字
	Conn *net.TCPConn
	//链接ID
	ConnID uint32
	//当前链接状态
	isClosed bool
	//当前链接所绑定的的业务处理方法API
	//handleAPI siface.HandleFunc  //有了router之后这个handleAPI已失效

	//告知当前链接已经退出|停止的channel
	ExitChan chan bool

	//用于读、写协程之间消息通信的chan
	msgChan chan []byte

	MsgHandle siface.IMsgHandle

	//连接中的自定义属性
	properties map[string]interface{}

	propertiesLock sync.RWMutex
}

func NewConnection(tcpServer siface.IServer, conn *net.TCPConn, connID uint32, msgHandle siface.IMsgHandle) *Connection {
	c := &Connection{
		TcpServer:  tcpServer,
		Conn:       conn,
		ConnID:     connID,
		isClosed:   false,
		MsgHandle:  msgHandle,
		ExitChan:   make(chan bool, 1),
		msgChan:    make(chan []byte),
		properties: make(map[string]interface{}, 0),
	}
	c.TcpServer.GetConnMgr().Add(c)
	return c
}

func (c *Connection) StartReader() {
	log.Println("ConnID=", c.ConnID, "Reader Go routine is running......")
	defer log.Println("ConnID=", c.ConnID, " Reader is exit,RemoteAddr is:", c.RemoteAddr().String())
	defer c.Stop()
	var tryTimes = 0
	for {
		tryTimes++
		//创建一个拆包解包对象
		dp := &Package{}
		//修改原始每次读取一个最大包长度字节流的方式 为 固定协议([DataLen(4字节)+MsgID(4字节)]=HEAD)+Body方式 TLV协议(type+len+value)
		//分两次读,第一次从conn读head
		headData := make([]byte, dp.GetHeadLen())
		_, err := io.ReadFull(c.Conn, headData)
		if err != nil {
			log.Printf("ConnID=%d read head error:[%v]", c.ConnID, err)
			break
		}
		//拆包得到msgID和msgDataLen的message对象,没有data
		msg, err := dp.Unpack(headData)
		if err != nil {
			log.Printf("ConnID=%d Unpack head binary data error:[%v]", c.ConnID, err)
			break
		} else {
			//log.Printf("ConnID=%d read head success,msgID:[%d],msgLen:[%d]", c.ConnID, msg.GetMessageID(), msg.GetMsgLen())
		}
		//>0说明msg是有body数据的
		var data []byte
		msgID := msg.GetMessageID()
		if msg.GetMsgLen() > 0 {
			data = make([]byte, msg.GetMsgLen())
			_, err = io.ReadFull(c.Conn, data)
			if err != nil {
				log.Printf("ConnID=%d msgID=[%d] read body data error:[%v]", c.ConnID, msgID, err)
				break
			}
		}
		msg.SetData(data)
		log.Printf("---------------->ConnID=%d msgID=[%d] received msg data:[%s]>----------------------", c.ConnID, msgID, string(msg.GetData()))
		req := &Request{conn: c, msg: msg}
		//执行注册的路由方法
		/*go func(request siface.IRequest) {
			c.Router.PreHandle(request)
			c.Router.Handle(request)
			c.Router.PostHandle(request)
		}(req)*/
		//go c.MsgHandle.DoMsgHandler(req)
		if utils.TCP2GlobalObject.WorkerPoolSize > 0 {
			c.MsgHandle.SendRequestToTaskQueue(req)
		} else {
			go c.MsgHandle.DoMsgHandler(req)
		}

	}
}

/**
写消息goroutine,专门发给客户端消息的模块
*/
func (c *Connection) StartWriter() {
	log.Println("ConnID=", c.ConnID, "Writer Go routine is running......")
	defer log.Println("ConnID=", c.ConnID, c.RemoteAddr().String(), "[conn Writer Exit!]")
	for {
		select {
		case bs := <-c.msgChan:
			if _, err := c.Conn.Write(bs); err != nil {
				log.Println("ConnID=", c.ConnID, c.RemoteAddr().String(), " writer writes error:", err)
				return
			}
		case <-c.ExitChan:
			//说明reader已经退出,此时writer也需要退出
			return
		}
	}
}

func (c *Connection) Start() {
	log.Println("Conn Start ..ConnID=", c.ConnID)
	//启动从当前的链接读取数据业务
	go c.StartReader()
	//启动从当前连接写数据业务
	go c.StartWriter()

	c.TcpServer.CallOnConnStart(c)

}

func (c *Connection) Stop() {
	log.Println("Conn Stop ..ConnID=", c.ConnID)
	if c.isClosed {
		return
	}
	c.isClosed = true
	c.TcpServer.CallOnConnStop(c)
	c.Conn.Close()
	//告知writer goroutine 关闭
	c.ExitChan <- true
	//从ConnMgr中去除
	c.TcpServer.GetConnMgr().Remove(c)
	//回收资源
	close(c.ExitChan)
	close(c.msgChan)
}

func (c *Connection) GetTCPConnection() *net.TCPConn {
	return c.Conn
}

func (c *Connection) GetConnID() uint32 {
	return c.ConnID
}

func (c *Connection) RemoteAddr() net.Addr {
	return c.Conn.RemoteAddr()
}

func (c *Connection) SendMsg(msgID uint32, data []byte) error {
	if c.isClosed {
		return errors.New("Connection is closed when SenMsg")
	}
	//将data 进行封包
	dp := &Package{}
	bs, err := dp.Pack(&Message{ID: msgID, DataLen: uint32(len(data)), Data: data})
	if err != nil {
		return err
	}
	//log.Println("SendMsg data:", bs)
	//将数据发送给客户端
	/*_, err = c.Conn.Write(bs)
	if err != nil {
		return err
	}*/
	c.msgChan <- bs
	return nil
}

//设置连接属性
func (c *Connection) SetProperty(key string, val interface{}) {
	c.propertiesLock.Lock()
	defer c.propertiesLock.Unlock()
	c.properties[key] = val
}

//获取连接属性
func (c *Connection) GetProperty(key string) (interface{}, error) {
	c.propertiesLock.Lock()
	defer c.propertiesLock.Unlock()
	if value, ok := c.properties[key]; ok {
		return value, nil
	}
	return nil, errors.New("找不到属性")
}

//移除连接属性
func (c *Connection) RemoveProperty(key string) {
	c.propertiesLock.Lock()
	defer c.propertiesLock.Unlock()
	delete(c.properties, key)
}
