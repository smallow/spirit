package snet

import (
	"gitee.com/smallow/spirit/socket/siface"
	"gitee.com/smallow/spirit/socket/utils"
	"log"
	"strconv"
)

type MsgHandle struct {
	Apis map[uint32]siface.IRouter
	//负责worker取任务的消息队列
	TaskQueue []chan siface.IRequest
	//worker池的数量
	WorkerPoolSize uint32
}

func NewMsgHandle() *MsgHandle {
	return &MsgHandle{Apis: make(map[uint32]siface.IRouter),
		WorkerPoolSize: utils.TCP2GlobalObject.WorkerPoolSize,
		TaskQueue:      make([]chan siface.IRequest, utils.TCP2GlobalObject.WorkerPoolSize)}
}

func (m *MsgHandle) DoMsgHandler(request siface.IRequest) {
	handler, ok := m.Apis[request.GetMessageID()]
	if !ok {
		log.Println("api msgID="+strconv.Itoa(int(request.GetMessageID())), " is NOT FOUND! Need Register !")
		return
	}
	handler.PreHandle(request)
	handler.Handle(request)
	handler.PostHandle(request)

}
func (m *MsgHandle) AddRouter(msgID uint32, router siface.IRouter) {
	if _, ok := m.Apis[msgID]; ok {
		panic("repeat msgID=" + strconv.Itoa(int(msgID)))
	}
	m.Apis[msgID] = router
}

func (m *MsgHandle) StartWorkerPool() {
	for i := 0; i < int(m.WorkerPoolSize); i++ {
		//启动一个worker
		m.TaskQueue[i] = make(chan siface.IRequest, utils.TCP2GlobalObject.MaxWorkerTaskSize)
		go m.startOneWorker(i, m.TaskQueue[i])
	}
}

func (m *MsgHandle) startOneWorker(workerID int, worker chan siface.IRequest) {
	log.Println("[WorkerID=", workerID, " chan starting to waiting for request ......]")
	for {
		select {
		case request := <-worker:
			m.DoMsgHandler(request)
		}
	}
}

func (m *MsgHandle) SendRequestToTaskQueue(request siface.IRequest) {
	workerID := request.GetConnection().GetConnID() % m.WorkerPoolSize
	log.Println("Add ConnID=", request.GetConnection().GetConnID(), "  Request MsgID= ", request.GetMessageID(), " to WorkerID=", workerID)
	m.TaskQueue[workerID] <- request
}
