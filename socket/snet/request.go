package snet

import "gitee.com/smallow/spirit/socket/siface"

type Request struct {
	//已经和客户端建立好的链接
	conn siface.IConnection
	//客户端请求的数据
	//data []byte
	msg siface.IMessage
}

func (r *Request) GetConnection() siface.IConnection {
	return r.conn
}

/*func (r *Request) GetData() []byte {
	return r.data
}*/

func (r *Request) GetData() []byte {
	return r.msg.GetData()
}

func (r *Request) GetMessageID() uint32 {
	return r.msg.GetMessageID()
}
