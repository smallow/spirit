package snet

import (
	"bytes"
	"encoding/binary"
	"gitee.com/smallow/spirit/socket/siface"
	"gitee.com/smallow/spirit/socket/utils"
	"github.com/pkg/errors"
)

const (
	Tcp2Head = "STCP2"
)

type Package struct {
}

func (m *Package) GetHeadLen() uint32 {
	//DataLen uint32 4字节+MsgID uint32 4字节=8字节
	return 8
}

func (m *Package) Pack(message siface.IMessage) ([]byte, error) {
	//创建一个存在bytes字节流的缓冲
	dataBuf := bytes.NewBuffer([]byte{})
	//将dataLen写入dataBuf中
	if err := binary.Write(dataBuf, binary.LittleEndian, message.GetMsgLen()); err != nil {
		return nil, err
	}
	//将msgID写入dataBuf
	if err := binary.Write(dataBuf, binary.LittleEndian, message.GetMessageID()); err != nil {
		return nil, err
	}
	//将data写入dataBuf
	if err := binary.Write(dataBuf, binary.LittleEndian, message.GetData()); err != nil {
		return nil, err
	}
	return dataBuf.Bytes(), nil
}

func (m *Package) Unpack(data []byte) (siface.IMessage, error) {
	//创建一个ioReader
	dataBuf := bytes.NewReader(data)
	message := &Message{}
	//读dataLen
	if err := binary.Read(dataBuf, binary.LittleEndian, &message.DataLen); err != nil {
		return nil, err
	}
	//读msgID
	if err := binary.Read(dataBuf, binary.LittleEndian, &message.ID); err != nil {
		return nil, err
	}
	//判断dataLen是否超出MaxPackageSize
	if utils.TCP2GlobalObject.MaxPackageSize > 0 && utils.TCP2GlobalObject.MaxPackageSize < message.DataLen {
		return nil, errors.New("too large msg data recv!")
	}
	return message, nil
}
