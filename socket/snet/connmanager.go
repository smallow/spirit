package snet

import (
	"gitee.com/smallow/spirit/socket/siface"
	"github.com/pkg/errors"
	"log"
	"strconv"
	"sync"
)

type ConnManager struct {
	connections map[uint32]siface.IConnection //管理连接的集合
	connLock    sync.RWMutex                  //读写保护锁
}

//创建连接
func NewConnManager() *ConnManager {
	return &ConnManager{
		connections: make(map[uint32]siface.IConnection),
	}
}

func (connMgr *ConnManager) Add(conn siface.IConnection) {
	connMgr.connLock.Lock()
	defer connMgr.connLock.Unlock()
	connMgr.connections[conn.GetConnID()] = conn
	log.Println("connection ID=", conn.GetConnID(), " add to ConnManager successfully :conn num= ", connMgr.Len())
}

func (connMgr *ConnManager) Remove(conn siface.IConnection) {
	connMgr.connLock.Lock()
	defer connMgr.connLock.Unlock()
	delete(connMgr.connections, conn.GetConnID())
	log.Println("connection ID=", conn.GetConnID(), " delete ConnManager successfully :conn num= ", connMgr.Len())
}

func (connMgr *ConnManager) Get(connID uint32) (siface.IConnection, error) {
	connMgr.connLock.RLock()
	defer connMgr.connLock.RUnlock()
	if conn, ok := connMgr.connections[connID]; ok {
		return conn, nil
	}
	return nil, errors.New("connection ID=" + strconv.Itoa(int(connID)) + " not FOUND!")
}

func (connMgr *ConnManager) Len() int {
	return len(connMgr.connections)
}

func (connMgr *ConnManager) ClearConn() {
	connMgr.connLock.Lock()
	defer connMgr.connLock.Unlock()

	for connID, conn := range connMgr.connections {
		conn.Stop()
		delete(connMgr.connections, connID)
	}
	log.Println("clear all connections success!")
}
