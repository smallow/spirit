package siface

/**
封包、拆包模块
直接面向TCP连接中的数据流
*/

type IPackage interface {
	GetHeadLen() uint32
	//封包
	Pack(msg IMessage) ([]byte, error)
	//拆包
	Unpack([]byte) (IMessage, error)
}
