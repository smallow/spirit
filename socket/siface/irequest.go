package siface

/**
把客户端请求的链接信息和数据包封装
*/

type IRequest interface {

	//获取当前连接
	GetConnection() IConnection

	//得到请求数据
	GetData() []byte

	GetMessageID() uint32
}
