package siface

/**
消息管理抽象层
*/

type IMsgHandle interface {
	//调度执行对应的router
	DoMsgHandler(request IRequest)
	//添加router
	AddRouter(msgID uint32, router IRouter)

	//启动一个worker工作池
	StartWorkerPool()

	SendRequestToTaskQueue(request IRequest)
}
