package siface

//定义一个服务接口

type IServer interface {
	//启动服务器
	Start()
	//停止服务器
	Stop()
	//运行服务器
	Serve()
	//给当前服务注册路由功能 供客户端连接处理使用
	//AddRouter(router IRouter)
	AddRouter(msgID uint32, router IRouter)

	GetConnMgr() IConnManager

	SetOnConnStart(func(connection IConnection))
	SetOnConnStop(func(connection IConnection))

	CallOnConnStart(connection IConnection)
	CallOnConnStop(connection IConnection)
}
