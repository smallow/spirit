package siface

type IMessage interface {
	GetMessageID() uint32
	GetMsgLen() uint32
	GetData() []byte
	SetData(data []byte)
}
