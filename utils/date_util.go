package utils

import (
	"fmt"
	"time"
)

const (
	Date_Full_Layout    = "2006-01-02 15:04:05"
	Date_Short_Layout   = "2006-01-02"
	Date_Small_Layout   = "01-02"
	Date_Time_Layout    = "15:04"
	Date_yyyyMMdd       = "20060102"
	Date_yyyyMMdd_Point = "2006.01.02"
)

//日期格式化
func FormatDate(src *time.Time, layout string) string {
	return (*src).Local().Format(layout)
}

//兼容从Java转换过来的数据，java生成的时间戳是按微秒算的，Go中只能按毫秒或纳秒算
func formatDateWithInt64(src *int64, layout string) string {
	var tmp int64
	if *src > 0 {
		if len(fmt.Sprint(*src)) >= 12 {
			tmp = (*src) / 1000
		} else {
			tmp = (*src)
		}
	} else {
		if len(fmt.Sprint(*src)) >= 13 {
			tmp = (*src) / 1000
		} else {
			tmp = (*src)
		}
	}
	date := time.Unix(tmp, 0)
	return FormatDate(&date, layout)
}

func FormatDateByInt64(src *int64, layout string) string {
	var tmp int64
	if *src > 0 {
		if len(fmt.Sprint(*src)) >= 12 {
			tmp = (*src) / 1000
		} else {
			tmp = (*src)
		}
	} else {
		if len(fmt.Sprint(*src)) >= 13 {
			tmp = (*src) / 1000
		} else {
			tmp = (*src)
		}
	}
	date := time.Unix(tmp, 0)
	return FormatDate(&date, layout)
}

//支持源端多种格式
func FormatDateWithObj(src *interface{}, layout string) string {
	if tmp, ok := (*src).(int64); ok {
		return formatDateWithInt64(&tmp, layout)
	} else if tmp2, ok2 := (*src).(float64); ok2 {
		tmpe := int64(tmp2)
		return formatDateWithInt64(&tmpe, layout)
	} else if tmp3, ok3 := (*src).(*time.Time); ok3 {
		return tmp3.Format(layout)
	} else {
		return ""
	}
}
