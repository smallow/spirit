package utils

import (
	"crypto/md5"
	cryptoRand "crypto/rand"
	"encoding/hex"
	"fmt"
	"github.com/dchest/captcha"
	"io"
	"math/rand"
	"net/url"
	"strings"
	"time"
)

func FilterXSS(str string) string {
	str = strings.Replace(str, "<", "&#60;", -1)
	str = strings.Replace(str, ">", "&#62;", -1)
	str = strings.Replace(str, "%3C", "&#60;", -1)
	str = strings.Replace(str, "%3E", "&#62;", -1)
	str = strings.Replace(str, "expression", "ｅｘｐｒｅｓｓｉｏｎ", -1)
	str = strings.Replace(str, "javascript", "ｊａｖａｓｃｒｉｐｔ", -1)
	return str
}

func MakeSimpleCaptcha(n int) string {
	var idChars = []byte("0123456789abcdefghijklmnopqrstuvwxyz")
	b := captcha.RandomDigits(n)
	for i, c := range b {
		b[i] = idChars[c]
	}
	return string(b)
}

/*
	获取当前时间的毫秒值
*/
func Now4Millis() int64 {
	return time.Now().UnixNano() / 1e6
}

/**
生成指定位数的随机数
*/
func CreateRand(_len int) string {
	var numbers = []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}
	var result = ""
	//r:=rand.New(rand.NewSource(time.Now().UnixNano()))
	//return fmt.Sprintf("%11v",rand.Int63n(100000000000))
	for i := 0; i < _len; i++ {
		result = result + numbers[rand.Intn(len(numbers))]
	}
	return result
}

/*产生n以内的随机数*/
func MakeIntRand(n int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(n)
}

//生成32位md5字串
func GetMd5String(s string) string {
	h := md5.New()
	h.Write([]byte(s))
	return hex.EncodeToString(h.Sum(nil))
}

func GenerateSimpleToken() string {
	rand.Seed(time.Now().UnixNano())
	str := fmt.Sprintf("%d%d", time.Now().UnixNano(), rand.Intn(999999))
	h := md5.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}

//获取复杂的随机数
func GetLetterRandom(length int, flag ...bool) string {
	var idChars = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
	var mod byte = 52
	if len(flag) > 0 && flag[0] {
		idChars = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
		mod = 26
	}
	b := make([]byte, length)
	maxrb := byte(256 - (256 % int(mod)))
	i := 0
EXIT:
	for {
		r := make([]byte, length+(length/4))
		if _, err := io.ReadFull(cryptoRand.Reader, r); err != nil {
			panic("GetLetterRandom: error reading random source: " + err.Error())
		}
		for _, c := range r {
			if c > maxrb {
				continue
			}
			b[i] = c % mod
			i++
			if i == length {
				break EXIT
			}
		}
	}
	for i, c := range b {
		b[i] = idChars[c]
	}
	return string(b)
}


/*获取复杂的随机数,数字和字母的组合
 * c > 2 数字的个数和字母的个数随机分配
 * n 数字的个数
 * l 字母的个数
 */
func GetComplexRandom(c, n, l int) string {
	if c < 2 && (n < 1 || l < 1) {
		return "--"
	}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	myCommonMethod := func(flag bool) int {
		if flag {
			return r.Intn(c-1) + 1
		} else {
			return r.Intn(c)
		}
	}
	if c >= 2 {
		n = myCommonMethod(true)
		l = c - n
	} else {
		c = l + n
	}
	value := MakeSimpleCaptcha(n) + GetLetterRandom(l)
	var array = strings.Split(value, "")
	for i := 0; i < c/2; i++ {
		r1 := myCommonMethod(false)
		r2 := myCommonMethod(false)
		o := array[r1]
		array[r1] = array[r2]
		array[r2] = o
	}
	return strings.Join(array, "")
}


//通用加密
func CommonEncodeArticle(stype string, keys ...string) (id string) {
	//正文
	var SE = &SimpleEncrypt{Key: "seland2022seland2022"}
	var SE2 = &SimpleEncrypt{Key: "2022asset"}
	if stype == "content" {
		id = BEncodeArticleId2ByCheck("A", SE, SE2, keys...)
	}
	return
}

//短地址加密,二次加密带校验和
func BEncodeArticleId2ByCheck(h string, s1, s2 *SimpleEncrypt, keys ...string) string {
	kstr := strings.Join(keys, ",")
	kstr = s1.EncodeStringByCheck(kstr)
	return url.QueryEscape(h + GetLetterRandom(2) + s2.EncodeStringByCheck(kstr))
}

/**
产生一般订单编号方法
*/
func CreateOrderCode() string {
	return fmt.Sprint(time.Now().Nanosecond()) + fmt.Sprint(GetSimpleRandom(6))
}

func GetSimpleRandom(n int) string {
	var idChars = []byte("0123456789")
	b := captcha.RandomDigits(n)
	for i, c := range b {
		b[i] = idChars[c]
	}
	return string(b)
}