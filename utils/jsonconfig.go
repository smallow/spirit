package utils

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

func ReadConfig(config ...interface{}) {
	var r *os.File
	if len(config) > 1 {
		filepath, _ := config[0].(string)
		r, _ = os.Open(filepath)
		defer r.Close()
		bs, _ := ioutil.ReadAll(r)
		json.Unmarshal(bs, config[1])
	} else {
		r, _ = os.Open("./config.json")
		defer r.Close()
		bs, _ := ioutil.ReadAll(r)
		json.Unmarshal(bs, config[0])
	}
}
