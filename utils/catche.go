package utils

import (
	"log"
	"runtime"
)

//出错拦截
func Catch() {
	if r := recover(); r != nil {
		log.Println("err:", r)
		for skip := 0; ; skip++ {
			_, file, line, ok := runtime.Caller(skip)
			if !ok {
				break
			}
			go log.Printf("%v,%v\n", file, line)
		}
	}
}
